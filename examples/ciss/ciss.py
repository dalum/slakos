import sys
sys.path.append("../../")

import numpy as np
import sympy as sym
import matplotlib.pyplot as plt

import slakos
from slakos import *
from slakos.simplify import costrig, identity

I = sym.S('I')

from slakos.representations.abstractvector import AbstractVectorSource, represent, span_in, normalize
from slakos.representations.parameters import ParameterSource, spanning
from slakos.representations.transformations import translation, rotation_plane, slater_koster, spherical_decomposition, angular_momentum, diag

A = AbstractVectorSource('A')
B = AbstractVectorSource('B')

S = AbstractVectorSource('S')
V = ParameterSource('V')

a, b = sym.symbols('a, b', real=True, positive=True)
#b = tan(b)
#a = sym.pi / 3

basis = (A.w, A._, A.x, A.y, A.z)
T = translation(b * A.z, A.w, basis)
P = rotation_plane(A.x, A.y, a, basis)
O = P @ T

atom_0 = A.x + A.w
v = O @ atom_0 - atom_0
r, theta, phi, subs = spherical_decomposition(v, basis[-3:])
phi = 0

R, E = slater_koster(1, theta, phi, A, V)

I_S = S.up/S.up + S.down/S.down
I_E = diag((A._, A.x, A.y, A.z))

V_ = sum(symbol * vector for symbol, vector in zip(
    sym.symbols('V_lambda, V_sss, V_sps, V_pps, V_ppp', real=True),
    (V['lambda'], V.sss, normalize(V.spp - V.spm), V.pp0, V.pp1)))

A[0] = A.z
A[1] = normalize(A.x + I * A.y)
A[-1] = normalize(A.x - I * A.y)

angmom = angular_momentum((A[1], A[0], A[-1]), basis[-3:])
sigma = angular_momentum((S.up, S.down), basis[-3:])
SOC = V['lambda'] / (angmom % sigma)

H_1 = ((I_S * R)).dag @ (I_S * E)[..., V_] @ ((I_S * O) @ (I_S * R))
H_1_ = ((I_S * O) @ (I_S * R)).dag @ (I_S * E)[..., V_] @ ((I_S * R))
xi, kappa, alpha = sym.symbols('xi, kappa, alpha', real=True)
subs_ = {sym.sqrt(r ** 2 - b ** 2).conjugate(): sym.sqrt(r ** 2 - b ** 2)}
H_1 = H_1 * sym.exp(I * xi) + H_1_ * sym.exp(-I * xi)
H = (H_1).simplify().subs(subs_).simplify().subs(subs_).simplify() + SOC[..., V_]

P = diag(span_in((A.z) * (S.up + S.down)))
Q = diag(span_in((A.x + A.y) * (S.up + S.down)))

omega = sym.Symbol('omega')

full_span = (A.x + A.y + A.z) * (S.up + S.down)
#H_eff = P @ H @ P + P @ (H @ (Q @ (diag(full_span) / omega + (H / (omega ** 2)) @ (Q @ (H @ P)))))

# model = sym.Matrix(represent(H_eff, span_in((A.z) * (S.up + S.down))))#.subs(subs)
model = sym.Matrix(represent(H, span_in((A._ + A.x + A.y + A.z) * (S.up + S.down))))#.subs(subs)
symbols = sorted(model.free_symbols, key=lambda a: a.name)
import numpy as np
fn = sym.lambdify(symbols, model, 'numpy')
params = [-2, 2, 1, 1, 0.5, np.pi]
a_, b_ = np.pi / 3, 0.5
r_ = np.sqrt(b_**2 + (np.cos(a_) - 1)**2 + np.sin(a_)**2)
params = lambda kappa, omega: [0.03, -2, 2, a_, b_, omega, r_, kappa]
params = lambda kappa, omega: [0.03, -2, 2, a_, b_, r_, kappa]
from numpy.linalg import eigvalsh
import matplotlib.pyplot as plt

xi2 = sym.Symbol('xi2')
parameters = Parameters({},
                        ['xi', xi2],
                        [a, b],
                        [],
                        expr=model)
parameters['a'] = sym.pi / 3
parameters['b'] = 0.2
parameters['r'] = sym.sqrt(b**2 + (sym.cos(a) - 1)**2 + sym.sin(a)**2)

hamiltonian = Hamiltonian(model, parameters)

from slakos.collections.symbolic import PointList
path = Path([r'$-\pi$', r'$0$', r'$\pi$'],
            PointList([ sym.Point(-sym.pi, 0),
                        sym.Point(0, 0),
                        sym.Point(sym.pi, 0) ]),
            parameters)
band = BandStructure(hamiltonian, path)
