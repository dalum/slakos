from sympy import (S, var, srepr, latex,
                   Matrix, SparseMatrix, BlockMatrix, MatrixSymbol, zeros,
                   sqrt, exp, cos, sin, tan,
                   simplify, trigsimp, collect, expand, factor,
                   lambdify)
from sympy.physics.quantum.dagger import Dagger

from itertools import (product, combinations, permutations, repeat, accumulate,
                       combinations_with_replacement)
from functools import reduce

import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import matplotlib.widgets as wdg
import operator as op

from multiprocessing import Pool

var("a,b,c", real=True, positive=True)
k = Matrix(var("k_x,k_y,k_z", real=True))

def eprint(env, expr, prefix="", suffix=",",
           conv=lambda expr: latex(S(expr)), fold_frac_powers=True):
    print("\\begin{{{env}}}\n{prefix}{expr}{suffix}\n\\end{{{env}}}".format(
          env=env, expr=conv(expr), prefix=prefix, suffix=suffix))
def sprint(expr):
    print(srepr(expr))

def vector_magnitude(v):
    return sqrt((v.T * v)[0])

def unify(expr):
    return expr.subs({'a': a, 'b': b, 'c': c})

def atomic_overlap(vector, orbitals, labels, interchangable=False):
    r = unify(Matrix(vector))
    d = vector_magnitude(r)
    V = (*var(("V_ss_sigma__{0}{1}," +
               "V_pp_sigma__{0}{1}," +
               "V_pp_pi__{0}{1}")
              .format(*sorted(labels)),
              real=True),
         *var(("V_sp_sigma__{0}{1}," * (2 if interchangable else 1) +
               "V_sp_sigma__{1}{0}," if not interchangable else "")
              .format(*(sorted(labels) if interchangable else labels)),
              real=True))

    epsilon = var(("epsilon_s__{0}," +
                   "epsilon_p__{0}").format(*labels), real=True)

    E = {}

    E['s'] = epsilon[0]
    E['p'] = epsilon[1]

    if d == 0:
        return E[orbitals[0][0]] * int(orbitals[0] == orbitals[1])

    E['s,s'] = V[0]

    for i, l in zip(["x", "y", "z"], r / d):
        E['s,p_{i}'.format(i=i)] = l * V[3]
        E['p_{i},s'.format(i=i)] = - l * V[4]
        E['p_{i},p_{i}'.format(i=i)] = (
            (l ** 2) * V[1] + (1 - l ** 2) * V[2])

    for (i, l), (j, m) in combinations(zip(["x", "y", "z"], r / d), 2):
        E['p_{i},p_{j}'.format(i=i, j=j)] = l * m * V[1] - m * l * V[2]
        E['p_{j},p_{i}'.format(i=i, j=j)] = l * m * V[1] - m * l * V[2]

    return exp(S("I") * (r.T * k)[0]) * E[','.join(orbitals)]

def atomic_matrix_element(vectors, orbitals, labels, interchangable=False):
    vectors = unify(Matrix(vectors))
    return sum(atomic_overlap(vectors[:,i], orbitals, labels,
                              interchangable=interchangable)
                     for i in range(vectors.cols))

def translation_vectors(vectors, size=1):
    vectors = unify(Matrix(vectors))
    return row_join(reduce(op.add, [n[i] * vectors[:,i]
                                    for i in range(vectors.cols)])
                    for n in product(range(-size, size+1),
                                     repeat=vectors.cols))

def atomic_hopping_vectors(m, n, atoms, vectors):
    atoms = unify(Matrix(atoms))
    vectors = unify(Matrix(vectors))
    return row_join(atoms[:,n] + vectors[:,i] - atoms[:,m]
                    for i in range(vectors.cols))

def vector_length_filter(vectors, length):
    vectors = unify(Matrix(vectors))
    vectors = [vectors[:,i] for i in range(vectors.cols)
               if simplify(vector_magnitude(vectors[:,i]) - length) == 0]
    return row_join(vectors) if len(vectors) else Matrix([])

def vector_lengths(vectors, subs=None):
    vectors = unify(Matrix(vectors))
    lengths = {simplify(vector_magnitude(vectors[:,i]))
               for i in range(vectors.cols)}

    return (sorted(lengths, key=lambda expr: expr.subs(subs)) if subs
            else [*lengths])

def neighbor_hopping_lengths(atoms, vectors, subs):
    atoms = unify(Matrix(atoms))
    vectors = unify(Matrix(vectors))

    hvecs = [[atomic_hopping_vectors(m, n, atoms, translation_vectors(vectors))
              for m in range(atoms.cols)]
             for n in range(atoms.cols)]

    lengths = {elem for n in range(atoms.cols) for m in range(atoms.cols)
               for elem in vector_lengths(hvecs[n][m])}

    return sorted(lengths, key=lambda expr: expr.subs(subs))

def atomic_matrix_block(m, n, atoms, tvecs, length, orbitals, labels,
                        interchangable, simplifier):
    from tight_binding import (vector_length_filter, atomic_hopping_vectors,
                               translation_vectors, atomic_matrix_element)
    from sympy import MatrixExpr
    hvecs = vector_length_filter(atomic_hopping_vectors(m, n, atoms, tvecs),
                                 length)
    return Matrix([[simplifier(
        atomic_matrix_element(hvecs, (o1, o2), (labels[m], labels[n]),
                              interchangable=interchangable))
                    for o1 in orbitals[labels[m]]]
                   for o2 in orbitals[labels[n]]])

def simp_none(expr):
    return expr

def simp_factor(expr):
    from sympy import factor
    return factor(expr)

def simp_2dtrig(expr):
    from sympy import trigsimp, cos, Expr
    return trigsimp(expr.subs({k_z: 0}).rewrite(cos)) if isinstance(expr, Expr) else expr

def tight_binding_hamiltonian(neighnums, subs, atoms, orbitals, labels, vectors,
                              interchangable=False, processes=None,
                              simplifier=simp_none):
    atoms = unify(Matrix(atoms))
    vectors = unify(Matrix(vectors))

    with Pool(processes=processes) as pool:
        tvecs = translation_vectors(vectors)
        for neighnum in neighnums:
            length = neighbor_hopping_lengths(atoms, vectors, subs)[neighnum]
            arglist = ((m, n, atoms, tvecs, length, orbitals, labels,
                        interchangable, simplifier)
                       for m, n in combinations_with_replacement(
                               range(atoms.cols), 2))
            blocks = [reduce(op.add, a) for a in zip(
                pool.starmap(atomic_matrix_block, arglist),
                *([blocks] if 'blocks' in locals() else []))]

    ndiag = lambda i, j: j + i * atoms.cols - (i * i + i) // 2
    mfunc = lambda i, j: (blocks[ndiag(i,j)] if j >= i
                          else blocks[ndiag(j,i)].H)

    return BlockMatrix([[mfunc(i, j) for i in range(atoms.cols)]
                        for j in range(atoms.cols)])

def enumerate_labels(labels):
    from collections import Counter
    counter = Counter()
    enum_labels = []
    for label in labels:
        counter.update(label)
        enum_labels.append("{0}{1}".format(label, counter[label]))
    return enum_labels

def hamiltonian_as_symbolic(block_matrix, name, labels, interchangable=False, simplifier=simp_factor):
    enum_labels = enumerate_labels(labels)
    blocks = block_matrix.blocks
    symbols = {}
    unique = []

    for i, j in combinations_with_replacement(range(len(labels)), 2):
        if not any(simplifier(blocks[i, j])[:,:]):
            symbols[(i, j)] = S(0)
            symbols[(j, i)] = S(0)
            continue

        symbol_name = "{0}_{1},{2}".format(name, enum_labels[i], enum_labels[j])

        for i_, j_ in symbols.keys():
            if simplifier(blocks[i_, j_])[:,:] == simplifier(blocks[i, j])[:,:]:
                symbols[(i, j)] = symbols[(i_, j_)]
                break
        else:
            symbols[(i, j)] = MatrixSymbol(symbol_name, *blocks[i,j].shape)
            unique.append((symbols[(i, j)], blocks[(i, j)]))

        if i != j:
            symbols[(j, i)] = Dagger(symbols[(i, j)])

    return Matrix(*blocks.shape, lambda i, j: symbols[(i, j)]), unique

def globalize_symbols(expr):
    for A in filter(lambda A: A.is_Symbol, expr.atoms()):
        globals()[str(A)] = A

def eigenvalues_at_point(func, params):
    return (lambda k_, *, params=params: np.linalg.eigvalsh(func(*k_, *params)))

def values_along_path(func, points, *, params=None, steps=100):
    points = unify(Matrix(points))
    dist = np.array([float(vector_magnitude(points[:,i+1] - points[:,i]))
                     for i in range(points.cols - 1)])
    dist /= sum(dist)
    path = [(points[:,i] * (1 - d) + points[:,i + 1] * d).T.tolist()[0]
              for i in range(points.cols - 1)
              for d in np.linspace(0, 1, steps * dist[i], i == points.cols - 2)]

    Y = np.array([func(list(map(float, k)), **({"params": params} if params else {}))
                  for k in path], ndmin=2).swapaxes(0, 1)
    X = np.tile(np.linspace(0, 1, Y.shape[1], True), (Y.shape[0], 1))
    datasets = np.dstack((X, Y))
    symmetry_points = [0, *accumulate(dist)]

    def update_func(params=params):
        return values_along_path(func, points, params=params, steps=steps)

    return datasets, symmetry_points, update_func

def plot_path_data(datasets, *, fmt='k-', ax=None, fig=None, steps=100, dim=2):
    if ax is None:
        fig = plt.figure()
        ax = fig.add_subplot(111)

    return (ax,
            [ax.plot(dataset[:, 0], dataset[:, 1], fmt)[0]
             for i, dataset in enumerate(datasets)],
            fig)

def plot_setup(ax, symmetry_points, labels, *,
               xlim=(0, 1), ylim=(-5, 5), ylabel="eV", vlinecol='k'):
    for point in symmetry_points:
        ax.axvline(point, color=vlinecol)
    ax.set_xlim(0, 1)
    plt.xticks(symmetry_points, labels)
    ax.set_ylim(*ylim)
    ax.set_ylabel(ylabel)

    return ax

def plot_interactive_sliders(ax, lines, update_func, params, adjustable):
    sliders = {}

    def update(val):
        for num, slider in sliders.items():
            params[num] = slider.val

        datasets, *_ = update_func(params)
        for line, dataset in zip(lines, datasets):
            line.set_ydata(dataset[:,1])

    for i, (num, rng, name) in enumerate(adjustable):
        sliders[num] = wdg.Slider(
            plt.axes([0.15 + (0.45 * (i % 2)),
                      0.01 + (0.025 * (i // 2)),
                      0.30,
                      0.02]),
            latex(name),
            float(params[num]) - rng,
            float(params[num]) + rng,
            valinit=params[num])
        sliders[num].on_changed(update)

    return sliders

def default_measure(fit_data, model_data, params, **kwargs):
    fit_slice = kwargs.get('fit_slice', np.s_[:])
    return (fit_data[fit_slice] - model_data[fit_slice]).flatten().tolist()

def build_fit_function(func, points, fit_data, guess, param_nums, *,
                       measure=default_measure, **kwargs):
    def _fn(vals):
        nonlocal guess

        for param, val in zip(param_nums, vals):
            guess[param] = val
        model_data, *_ = values_along_path(func, points, params=guess,
                                           steps=fit_data.shape[1])
        return measure(fit_data, model_data, params, **kwargs)
    return _fn

def build_optimize_plot_function_leastsq(func, points, fit_data, guess,
                                         param_names, *,
                                         measure=default_measure, **kwargs):

    def _opt_fn(param_nums, maxfev=100, sequentially=True, measure=measure,
                **_kwargs):
        from scipy.optimize import leastsq

        if sequentially:
            for num in param_nums:
                leastsq(build_fit_function(
                    func, points, fit_data, guess, [num], measure=measure,
                    **{**kwargs, **_kwargs}),
                        [guess[num]],
                        maxfev=maxfev)
        else:
            leastsq(build_fit_function(
                func, points, fit_data, guess, param_nums, measure=measure,
                **{**kwargs, **_kwargs}),
                    [guess[num] for num in param_nums],
                    maxfev=maxfev)
        return guess

    def _plot_fn(steps=100, variation=2, model_data_fmt='k-',
                 fit_data_fmt='r--', interactive=True):
        datasets, symmetry_points, update_func = (
            values_along_path(func, points, steps=steps))
        ax, lines, fig = plot_path_data(datasets, fmt=model_data_fmt)
        plot_setup(ax, symmetry_points, labels)
        if interactive:
            sliders = plot_interactive_sliders(ax, lines, update_func, guess,
                                               [*zip(range(len(guess)),
                                                     [variation] * len(guess),
                                                     param_names)])
        plot_path_data(fit_data, ax=ax, fmt=fit_data_fmt)
        return fig, ax, lines, update_func

    return _opt_fn, _plot_fn

def load_numpy_printed_list(s):
    return [*map(float, s.replace("[", "").replace("]", "").split())]

def print_parameters(params, param_names, units):
    print(", ".join("${name}={val}\\text{{ {unit}}}$".format(name=latex(S(name)), val=val, unit=unit)
                    for name, val, unit in zip(param_names, params, units)))

def measure_bandgap(datasets, vband, cband):
    direct_gap = min(datasets[cband,:,1] - datasets[vband,:,1])
    indirect_gap = min(datasets[cband,:,1]) - max(datasets[vband,:,1])
    return direct_gap, indirect_gap

def banddiff_measure(fit_data, model_data, params, **kwargs):
    fit_slice = kwargs.get('fit_slice', np.s_[:,:,:])
    offset = kwargs.get('offset', 0.01)
    power = kwargs.get('power', 4)
    banddiff_weight = kwargs.get('banddiff_weight', 1)
    bandgap = kwargs.get('bandgap', 1.4)
    bandgap_weight = kwargs.get('bandgap_weight', 1000)
    vband = kwargs.get('vband', 9)
    cband = kwargs.get('cband', 10)

    shiftu_slice = (slice(fit_slice[0].start+1,
                          fit_slice[0].stop,
                          fit_slice[0].step),
                    *fit_slice[1:])
    shiftd_slice = (slice(fit_slice[0].start,
                          fit_slice[0].stop-1,
                          fit_slice[0].step),
                    *fit_slice[1:])

    diff = (fit_data[fit_slice] - model_data[fit_slice]).flatten().tolist()
    band_diff = (
        1 / ((fit_data[shiftu_slice] - fit_data[shiftd_slice]) ** power + offset)
        - 1 / ((model_data[shiftu_slice] - model_data[shiftd_slice]) ** power + offset)
    ).flatten().tolist()
    return [*diff, banddiff_weight * sum(band_diff),
            bandgap_weight * (bandgap - min(measure_bandgap(fit_data,
                                                        vband, cband)))]
