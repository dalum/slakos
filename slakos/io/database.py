import sqlite3
import sympy as sym

from typing import Callable, Mapping, Iterable
from concurrent.futures import ProcessPoolExecutor

from slakos.collections.utilities import matrix_from_sparse_mapping
from slakos.simplify import identity
from slakos.representations import Lattice, Basis
from slakos.representations.tables import MatrixElementTable
from slakos.modelling.hopping import matrix_element_neighbor_sum

class Database:
    def __init__(self, dbfile: str) -> None:
        self.__dict__.update(locals())

        with sqlite3.connect(self.dbfile) as con:
            con.execute("CREATE TABLE IF NOT EXISTS _properties ("
                        "tblname VARCHAR, "
                        "propname VARCHAR, "
                        "propval VARCHAR, "
                        "UNIQUE (tblname, propname) ON CONFLICT REPLACE)")

            con.execute("CREATE TABLE IF NOT EXISTS _parameters ("
                        "id integer primary key autoincrement, "
                        "name VARCHAR, "
                        "hash VARCHAR, "
                        "parameters VARCHAR)")

    def query(self, query: str, values: tuple = ()) -> sqlite3.Cursor:
        with sqlite3.connect(self.dbfile) as con:
            result = con.execute(query, values).fetchall()
        return result

    def set_property(self, tblname: str, name: str, value: str) -> None:
        with sqlite3.connect(self.dbfile) as con:
            con.execute("INSERT INTO _properties VALUES (?, ?, ?)",
                        (tblname, name, value))

    def setup(self, tblname: str, append=False, ignore=False,
              clear=False) -> None:
        if clear:
            self.clear(tblname)

        with sqlite3.connect(self.dbfile) as con:
            try:
                con.execute("CREATE TABLE {} ("
                            "row VARCHAR, "
                            "column VARCHAR, "
                            "index_ INT, "
                            "expr VARCHAR)".format(tblname))
                return True
            except sqlite3.OperationalError:
                if append:
                    return True
                elif ignore:
                    return False
                else:
                    raise NameError("table {} already exists.".format(tblname))

    def clear(self, tblname: str) -> None:
        with sqlite3.connect(self.dbfile) as con:
            con.execute("DROP TABLE IF EXISTS {}".format(tblname))

    def populate(self, tblname: str,
                 basis: Basis,
                 lattice: Lattice,
                 table: MatrixElementTable,
                 indices: Iterable,
                 k: sym.Point,
                 *,
                 simplifier: Callable = identity,
                 derivs: list = [],
                 hermitian=True,
                 **kwargs) -> bool:
        if not self.setup(tblname, **kwargs):
            return False

        self.set_property(tblname, "hermitian", str(hermitian))

        from itertools import combinations_with_replacement, product

        blocks = tuple(basis.blocks.items())

        if hermitian:
            block_combs = list(combinations_with_replacement(blocks, 2))
        else:
            block_combs = list(product(basis.blocks.items(), repeat=2))

        hvecs_indices = list(zip(
            (lattice.neighbor_hopping_vectors(index) for index in indices),
            indices))

        with ProcessPoolExecutor() as e:
            futures = [e.submit(_db_write_matrix_block,
                                self.dbfile, tblname,
                                block1, block2, k,
                                hvecs = hvecs[block1[0][0], block2[0][0]],
                                table = table,
                                index = index,
                                subs = lattice.equilibrium_subs,
                                simplifier=simplifier,
                                derivs=derivs)
                       for hvecs, index in hvecs_indices
                       for block1, block2 in block_combs]

        return all(future.result() for future in futures)

    def _block(self, tblname: str, indices: list,
               basis_row: Basis, basis_col: Basis, prefix: str = '') -> sym.Matrix:
        with sqlite3.connect(self.dbfile) as con:
            hermitian = eval(con.execute("SELECT propval FROM _properties "
                                         "WHERE tblname=? AND propname=?",
                                         (tblname, 'hermitian')).fetchone()[0])

        with ProcessPoolExecutor() as e:
            futures = {(i, j): e.submit(_db_fetch_matrix_element,
                                        self.dbfile, tblname,
                                        hermitian, b1, b2, indices, prefix)
                       for i, b1 in enumerate(basis_row)
                       for j, b2 in enumerate(basis_col)}
        results = {key: future.result() for key, future in futures.items()}

        return matrix_from_sparse_mapping((len(basis_row), len(basis_col)),
                                           results)

    def matrix(self, tblname: str, indices: list, basis: 'Basis', **kwargs) -> sym.Matrix:
        return self._block(tblname, indices, basis, basis, **kwargs)

    def block_matrix(self, tblname: str, indices: list,
                     basis: 'Basis', **kwargs) -> sym.BlockMatrix:
        """Return a list of blocks and their names."""
        return (sym.BlockMatrix([[self._block(tblname, indices, b1, b2, **kwargs)
                                  for _, b2 in basis.blocks.items()]
                                 for _, b1 in basis.blocks.items()]).blocks,
                [(elem, num) for (elem, num), basis in basis.blocks.items()])

    def tensor_matrix(self, tblname: str, indices: list,
                      basis: 'Basis', d: int = 2, **kwargs) -> sym.Matrix:
        """Return a matrix of the form M x I(d)."""
        from sympy.physics.quantum import TensorProduct
        return TensorProduct(self.matrix(tblname, indices, basis, **kwargs), sym.eye(2))

    # Parameters

    def list_parameters(self) -> None:
        with sqlite3.connect(self.dbfile) as con:
            query = con.execute("SELECT name FROM _parameters")
            results_set = {result[0] for result in query.fetchall()}
        return results_set

    def save_parameters(self, name: str, parameters: 'Parameters') -> None:
        from pickle import dumps

        with sqlite3.connect(self.dbfile) as con:
            con.execute("INSERT INTO _parameters (name, hash, parameters) "
                        "VALUES (?, ?, ?)",
                        (name, parameters.hash(), dumps(parameters)))

    def load_parameters(self, name: str, n: int = 1, hash_=None) -> 'Parameters':
        from pickle import loads

        with sqlite3.connect(self.dbfile) as con:
            if hash_ is None:
                query = con.execute("SELECT parameters FROM _parameters "
                                    "WHERE name=? ORDER BY id DESC",
                                    (name,))
            else:
                query = con.execute("SELECT parameters FROM _parameters "
                                    "WHERE name=? AND hash=? ORDER BY id DESC",
                                    (name, hash_))

        results_gen = (loads(result[0]) for result in query.fetchall())

        from itertools import islice, chain, repeat
        return [*islice(chain(results_gen, repeat(None)), n)][n - 1]


def _db_write_matrix_block(dbfile: str, tblname: str,
                           block1: Basis, block2: Basis, k: sym.Point,
                           hvecs: set,
                           table: MatrixElementTable,
                           index: str,
                           subs: dict,
                           simplifier: Callable = identity,
                           derivs: list = [],
                           timeout: float = 60.0) -> None:
    from itertools import product, chain, repeat

    values = []
    (elem1, _), basis1 = block1
    (elem2, _), basis2 = block2
    orbitals1 = [basis_elem.orbital for basis_elem in basis1]
    orbitals2 = [basis_elem.orbital for basis_elem in basis2]

    for o1, o2 in product(orbitals1, orbitals2):
        base_expr = simplifier(matrix_element_neighbor_sum(
            hvecs, k, table, (index, elem1, elem2), (o1, o2), subs))

        def deriv_str(xs):
            return ','.join(sym.latex(x) for x in xs)
        derivs_ = [(sym.diff(base_expr, *xs), deriv_str(xs)) for xs in derivs]

        def append_value(prefix, expr):
            values.append(("{}.{}.{}".format(prefix, elem1, o1),
                           "{}.{}.{}".format(prefix, elem2, o2),
                           str(index),
                           sym.srepr(expr)))

        for expr, prefix in [(base_expr, ''), *derivs_]:
            append_value(prefix, expr)

    with sqlite3.connect(dbfile, timeout=timeout) as con:
        con.execute(''.join(("INSERT INTO {} VALUES ".format(tblname),
                             ", ".join(repeat("(?, ?, ?, ?)",
                                              len(values))))),
                    (*chain(*values),))

    return True

def _db_fetch_matrix_element(dbfile: str, tblname: str, hermitian: bool,
                             b1: tuple, b2: tuple, indices: Iterable,
                             prefix: str = '') -> sym.Expr:
    elem1, _, o1 = b1
    b1 = "{}.{}.{}".format(prefix, elem1, o1)
    elem2, _, o2 = b2
    b2 = "{}.{}.{}".format(prefix, elem2, o2)

    with sqlite3.connect(dbfile) as con:
        hermitian = eval(con.execute("SELECT propval FROM _properties "
                                     "WHERE tblname=? AND propname=?",
                                     (tblname, 'hermitian')).fetchone()[0])

        def fetch_element(b1, b2, index):
            try:
                result = con.execute("SELECT expr FROM {} WHERE "
                                     "row=? AND column=? AND "
                                     "index_=?".format(tblname),
                                     (b1, b2, str(index))).fetchone()[0]
                return sym.sympify(result)
            except TypeError as e:
                error_string = ("no entry for row={}, column={}, index={}"
                                "".format(b1, b2, str(index)))
                raise ValueError(error_string) from e

        def fetch_element_protected(b1, b2, index):
            try:
                return fetch_element(b1, b2, index)
            except ValueError as e:
                if hermitian:
                    return sym.conjugate(fetch_element(b2, b1, index))
                else:
                    raise e

        from functools import reduce
        from operator import add

        return reduce(add, (fetch_element_protected(b1, b2, index)
                            for index in indices))
