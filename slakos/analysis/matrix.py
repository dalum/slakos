import sympy as sym

from typing import Callable, Iterable, Mapping

from slakos.simplify import identity

ANALYSIS_UNIQUE = 1
ANALYSIS_IDENTITY = 2
ANALYSIS_CONJUGATE = 4
ANALYSIS_TRANSPOSE = 8
ANALYSIS_ADJOINT = ANALYSIS_CONJUGATE | ANALYSIS_TRANSPOSE

def analyse_backward(row, col, matrix: sym.Matrix,
                     simplifier: Callable = identity,
                     variables: dict = {}, hermitian=False) -> dict:
    result = dict(flags = 0,
                  entry = (None, None),
                  variables = [])

    block = matrix[row, col]
    if isinstance(block, sym.Matrix):
        zero = sym.zeros(block.shape)
    else:
        zero = 0

    if hermitian and row > col:
        rows_and_cols = ((col, row),)
    else:
        rows_and_cols = ((row, col) for row in range(matrix.rows)
                                    for col in range(matrix.cols))

    for row_, col_ in rows_and_cols:
        if row_ == row and col == col_:
            result['flags'] = ANALYSIS_UNIQUE
            break

        block_ = matrix[row_, col_]

        if block_ == block or (block.shape == block_.shape and
                               simplifier(block - block_) == zero):
            result['flags'] = ANALYSIS_IDENTITY
            result['entry'] = (row_, col_)
            break

        elif (block_ == sym.conjugate(block) or
              (block.shape == block_.shape and
               simplifier(block_ - sym.conjugate(block)) == zero)):
            result['flags'] = ANALYSIS_CONJUGATE
            result['entry'] = (row_, col_)
            break

        elif (block_ == sym.transpose(block) or
              (block.shape == sym.transpose(block_).shape and
               simplifier(block_ - sym.transpose(block)) == zero)):
            result['flags'] = ANALYSIS_TRANSPOSE
            result['entry'] = (row_, col_)
            break

        elif (block_ == sym.adjoint(block) or
              (block.shape == sym.adjoint(block_).shape and
               simplifier(block_ - sym.adjoint(block)) == zero)):
            result['flags'] = ANALYSIS_ADJOINT
            result['entry'] = (row_, col_)
            break
    else:
        if hermitian and row > col:
            result['flags'] = ANALYSIS_ADJOINT
            result['entry'] = (col, row)
        else:
            result['flags'] = ANALYSIS_UNIQUE

    result['variables'] = [variable for variable in variables
                           if variable in block.atoms(sym.Symbol)]

    return result

def analyse_matrix(matrix: sym.Matrix, block_basis: list = None,
                   uname: str = None,
                   simplifier: Callable = identity,
                   variables: dict = {},
                   hermitian=False) -> sym.Matrix:
    symbols = {}
    unique = {}

    from concurrent.futures import ProcessPoolExecutor
    with ProcessPoolExecutor() as e:
        from numpy import array
        from itertools import product, repeat

        # prod = [*product(*map(range, matrix.shape))]

        # simils = array([*e.map(analyse_backward,
        #                        (a[0] for a in prod),
        #                        (a[1] for a in prod),
        #                        repeat(matrix))]).reshape(*matrix.shape)

        futures = { (row, col): e.submit(analyse_backward, row, col, matrix,
                                         simplifier=simplifier,
                                         variables=variables,
                                         hermitian=hermitian)
                    for row in range(matrix.rows)
                    for col in range(matrix.cols) }
        simils = {key: value.result() for key, value in futures.items()}

    for row, col in product(*map(range, matrix.shape)):
        if (row, col) in symbols:
            continue

        if not (simils[row, col]['flags'] ^ ANALYSIS_IDENTITY):
            symbols[row, col] = symbols[simils[row, col]['entry']]
            continue
        elif not (simils[row, col]['flags'] ^ ANALYSIS_CONJUGATE):
            symbols[row, col] = sym.conjugate(symbols[simils[row, col]['entry']])
            continue
        elif not (simils[row, col]['flags'] ^ ANALYSIS_TRANSPOSE):
            symbols[row, col] = sym.Transpose(symbols[simils[row, col]['entry']])
            continue
        elif not (simils[row, col]['flags'] ^ ANALYSIS_ADJOINT):
            symbols[row, col] = sym.Adjoint(symbols[simils[row, col]['entry']])
            continue

        # If none of the above, we are dealing with a unique element.
        block = simplifier(matrix[row, col])

        if isinstance(block, sym.MatrixBase):
            if not any(block):
                symbols[row, col] = sym.S(0)
                symbols[col, row] = sym.S(0)
                continue
            if uname is None:
                uname = 'h'
        elif isinstance(block, sym.Expr):
            if not bool(block):
                symbols[row, col] = sym.S(0)
                symbols[col, row] = sym.S(0)
                continue
            if uname is None:
                uname = 't'
        else:
            raise TypeError("{} is not a matrix or expression".format(block))

        def format_element(elem, num):
            return "{}{}".format(elem.symbol, num)

        if block_basis is not None:
            name = "{}_{}{}".format(uname,
                                    format_element(*block_basis[row]),
                                    format_element(*block_basis[col]))
        else:
            name = "{}_{}{}".format(uname, row + 1, col + 1)

        symbol = sym.Symbol(name)

        if len(simils[row, col]['variables']) > 0:
            symbols[row, col] = symbol(*simils[row, col]['variables'])
        else:
            symbols[row, col] = symbol

        unique.update({symbols[row, col]: simplifier(block)})

    return (sym.Matrix(*matrix.shape, lambda row, col: symbols[row, col]), unique)
