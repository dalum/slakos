import numpy as np
import sympy as sym

from inspect import signature
from sympy.printing.theanocode import theano_function

from .unify import qualified_name

def slambdify(symbols, expr, dtypes=None):
    dtypes = dtypes or {}
    wrapped = theano_function(symbols, [expr], dtypes=dtypes)

    wrapped_args = [symbol.name for symbol in symbols]
    qualified_symbols = [qualified_name(expr.name) for expr in symbols]

    args_map = dict(zip(qualified_symbols, wrapped_args))
    args_str = ', '.join(qualified_symbols)
    print(args_map)

    definition = r'''def slambdified_function({args}):
    """Slambdified function.

    Parameters
    ----------
    {args}

    """
    kwargs = {{args_map[key]: value for key, value in locals().items()}}
    return wrapped(**kwargs)

    '''.format(args=args_str)

    partial_function_code = compile(
        definition, "<slambdified function source>", "exec")

    partial_globals = {}
    eval(partial_function_code, {'args_map': args_map, 'wrapped': wrapped},
         partial_globals)

    slambdified_function = partial_globals['slambdified_function']
    slambdified_function.wrapped = wrapped

    return slambdified_function
