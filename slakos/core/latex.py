import sympy as sym

from sympy.printing.printer import Printer
from sympy.printing.latex import LatexPrinter
from sympy.printing.latex import translate as sympy_translate

tex_extra_dictionary = {
    'up': r'\uparrow',
    'down': r'\downarrow',
}

class SlakosLatexPrinter(LatexPrinter):
    _default_settings = {
        "order": None,
        "mode": "plain",
        "itex": False,
        "fold_frac_powers": True,
        "fold_func_brackets": False,
        "fold_short_frac": None,
        "long_frac_ratio": 2,
        "mul_symbol": None,
        "inv_trig_style": "abbreviated",
        "mat_str": None,
        "mat_delim": "[",
        "symbol_names": {},
        "substitutions": {},
        "prefix": "",
        "suffix": "",
        "columns": 3,
    }

    printmethod = "_latex"

    def __init__(self, settings=None):
        Printer.__init__(self, settings)

        if self._settings['fold_short_frac'] is None and \
                self._settings['mode'] == 'inline':
            self._settings['fold_short_frac'] = True

        mul_symbol_table = {
            None: r" ",
            "ldot": r" \,.\, ",
            "dot": r" \cdot ",
            "times": r" \times "
        }

        self._settings['mul_symbol_latex'] = \
            mul_symbol_table[self._settings['mul_symbol']]

        self._settings['mul_symbol_latex_numbers'] = \
            mul_symbol_table[self._settings['mul_symbol'] or 'dot']

        self._delim_dict = {'(': ')', '[': ']'}

    def _print_Transpose(self, expr):
        mat = expr.arg
        if isinstance(mat, sym.Symbol):
            return r"%s^T" % self._print(mat)
        elif isinstance(mat, sym.Function):
            fn, args = self._print(mat).split(r'{\left')
            return r"%s^T{\left%s" % (fn, args)
        else:
            return r"\left(%s\right)^T" % self._print(mat)

    def _print_Adjoint(self, expr):
        mat = expr.arg
        if isinstance(mat, sym.Symbol):
            return r"%s^\dag" % self._print(mat)
        elif isinstance(mat, sym.Function):
            fn, args = self._print(mat).split(r'{\left')
            return r"%s^\dag{\left%s" % (fn, args)
        else:
            return r"\left(%s\right)^\dag" % self._print(mat)

    def _print_conjugate(self, expr, exp=None):
        if isinstance(expr.args[0], sym.Symbol):
            tex = r"%s^{*}" % self._print(expr.args[0])
        elif isinstance(expr.args[0], sym.Function):
            fn, args = self._print(expr.args[0]).split(r'{\left')
            tex = r"%s^{*}{\left%s" % (fn, args)
        else:
            tex = r"\overline{%s}" % self._print(expr.args[0])

        if exp is not None:
            return r"%s^{%s}" % (tex, exp)
        else:
            return tex

    def _hprint_Function(self, func):
        from sympy.printing.latex import accepted_latex_functions

        func = self._deal_with_super_sub(func)

        if func in accepted_latex_functions:
            name = r"\%s" % func
        else:
            name = func

        return name


    def _deal_with_super_sub(self, string):
        # Unlike SymPy's LaTeX printer, we put commas between super- and
        # subscripts in names.
        from sympy.printing.conventions import split_super_sub

        name, supers, subs = split_super_sub(string)

        name = translate(name)
        supers = [translate(sup) for sup in supers]
        subs = [translate(sub) for sub in subs]

        # glue all items together:
        if len(supers) > 0:
            name += "^{%s}" % " ".join(supers)
        if len(subs) > 0:
            name += "_{%s}" % " ".join(subs)

        return name

    def doprint(self, expr):
        if (self._settings['mode'] == 'inline' and
            hasattr(expr, '_latex_inline')):
            tex = expr._latex_inline(self)
            self._settings['mode'] = 'plain'
        elif (self._settings['mode'] == 'table' and
            hasattr(expr, '_latex_table')):
            tex = expr._latex_table(self)
            self._settings['mode'] = 'plain'
        else:
            tex = Printer.doprint(self, expr)

        tex = self._final_substitutions(tex)

        prefix, suffix = self._settings['prefix'], self._settings['suffix']
        tex = prefix + tex + suffix

        if self._settings['mode'] == 'plain':
            return tex
        elif self._settings['mode'] == 'inline':
            return r"$%s$" % tex
        elif self._settings['itex']:
            return r"$$%s$$" % tex
        else:
            env_str = self._settings['mode']
            return r"\begin{%s}%s\end{%s}" % (env_str, tex, env_str)

    def _final_substitutions(self, tex):
        import re
        for pattern, repl in self._settings['substitutions'].items():
            tex = re.sub(pattern, repl, tex)
        return tex

def translate(s):
    return tex_extra_dictionary.get(s) or sympy_translate(s)

def latex(expr, **settings):
    return SlakosLatexPrinter(settings).doprint(expr)

def eprint(mode: str, expr: sym.Expr, **settings) -> None:
    settings['mode'] = mode
    print(latex(expr, **settings))
