import sympy as sym

class LabelledPoint(sym.Point):
    __slots__ = ['label', 'point']

    def __init__(self, label, *coordinates):
        self.label = label
        sym.Point.__init__(*coordinates)
