# This file is part of Slakos.
# 
# Slakos is free software: you can redistribute it and/or modify it under the terms
# of the GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
# 
# Slakos is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE.  See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with
# Slakos.  If not, see <http://www.gnu.org/licenses/>.

import logging

import numpy as np
import sympy as sym

from typing import Callable, Iterable

class Vector:
    def __init__(self, coordinates: Iterable,
                 label: str = None, identifier: str = None, *,
                 convert=True) -> None:
        # Flatten column iterables
        coordinates = np.array([*coordinates]).flatten().tolist()

        if convert:
            super().__setattr__('coordinates', tuple(map(slakify, coordinates)))
        else:
            super().__setattr__('coordinates', tuple(coordinates))

        label = label or repr(coordinates)
        super().__setattr__('label', label)
        super().__setattr__('identifier', identifier)

    @property
    def idlabel(self):
        return '{}{}'.format(self.label, self.identifier)

    def atoms(self, *args):
        return {atom for x in self.coordinates for atom in x.atoms(*args)}

    def _unify(self, **kwargs):
        cls = self.__class__
        new_coordinates = (unify(x, **kwargs) for x in self.coordinates)
        return cls(new_coordinates, self.label, self.identifier)

    def with_label(self, label):
        cls = self.__class__
        return cls(self.coordinates, label, self.identifier)

    def evalb(self):
        new_coordinates = (*map(float, self.coordinates),)

        cls = self.__class__
        return cls(new_coordinates, self.label, self.identifier, convert=False)

    def __iter__(self):
        return iter(self.coordinates)

    def __getitem__(self, name):
        return self.coordinates[name]

    def __getattr__(self, name):
        def get(*args, **kwargs):
            new_coordinates = (
                getattr(x, name, lambda *args, **kwargs: x)(*args, **kwargs)
                for x in self.coordinates)

            cls = self.__class__
            return cls(new_coordinates, self.label, self.identifier)
        return get

    def __setattr__(self, name, value):
        raise AttributeError("cannot modify attributes of Vector.")

    def __delattr__(self, name):
        raise AttributeError("cannot delete attributes of Vector.")

    def __getstate__(self):
        return (tuple(map(sym.srepr, self.coordinates)),
                self.label, self.identifier)

    def __setstate__(self, state):
        self.__init__(*state)

    def __abs__(self):
        return sym.sqrt(sum(x ** 2 for x in self.coordinates))

    def __len__(self):
        return len(self.coordinates)

    def __repr__(self):
        return "Vector({}, {}, {})".format(self.coordinates,
                                           self.label, self.identifier)

    def __hash__(self):
        return hash(self.coordinates) + hash(self.label)

    def __eq__(self, other):
        if isinstance(other, type(self)):
            return (self.label == other.label and
                    self.coordinates == other.coordinates)

    def __neg__(self):
        cls = self.__class__
        new_coordinates = tuple(-x for x in self.coordinates)
        new_label = '(-{})'.format(self.label)
        return cls(new_coordinates, new_label, self.identifier)

    def __sub__(self, other):
        cls = self.__class__
        if isinstance(other, cls):
            if len(self) != len(other):
                raise ValueError("vector dimensions {}, {} do not match"
                                 "".format(len(self), len(other)))

            new_coordinates = tuple(self.coordinates[i] - other.coordinates[i]
                                    for i in range(len(self)))
            new_label = '({} - {})'.format(self.label, other.label)
            return cls(new_coordinates, new_label, None)

        return NotImplemented

    def __add__(self, other):
        cls = self.__class__
        if isinstance(other, cls):
            if len(self) != len(other):
                raise ValueError("vector dimensions {}, {} do not match"
                                 "".format(len(self), len(other)))

            new_coordinates = tuple(self.coordinates[i] + other.coordinates[i]
                                    for i in range(len(self)))
            new_label = '(' + self.label + ' + ' + other.label + ')'
            return cls(new_coordinates, new_label, None)

        return NotImplemented

    def __mul__(self, other):
        try:
            cls = self.__class__
            new_coordinates = tuple(self.coordinates[i] * other
                                    for i in range(len(self)))
            new_label = '({} * {})'.format(other, self.label)
            return cls(new_coordinates, new_label, None)
        except:
            raise ValueError("cannot multiply vector with instance of type {}"
                             "".format(type(other)))

        return NotImplemented
    __rmul__ = __mul__

    def __matmul__(self, other):
        try:
            from functools import reduce
            from operator import add
            return reduce(add, (self.coordinates[i] * other.coordinates[i]
                                for i in range(len(self))))
        except:
            raise ValueError("cannot matrix multiply vector with instance of "
                             "type {}".format(type(other)))

        return NotImplemented

class VectorCollection:
    @classmethod
    def from_sequences(cls, coordinates: Iterable[Iterable],
                       labels: Iterable = None,
                       axis: int = 0):
        """Create a VectorCollection from sequences of labels and coordinates.

        Parameters
        ----------
        coordinates : two-dimensional sequence
            Sequence of coordinates to be assigned to the vectors.
        labels : sequence
            Sequence of labels to be applied to the vectors. If None (default)
            the vectors will be labelled by their index.
        axis : int, optional
            Axis along which to separate vectors and dimensions. If `axis` is 0
            (default), the coordinates are assumed to be aligned along axis 1
            and vice versa. See notes for more information.

        Usage Notes
        -----------
        If coordinates are arranged in columns, specify `axis=1`.

        """
        coordinates = np.array(coordinates)
        if axis == 1:
            coordinates = coordinates.swapaxes(0, 1)

        if labels is None:
            labels = [*map(str, range(len(coordinates)))]
            identifiers = [None for _ in range(len(labels))]
        else:
            identifiers = []

            from collections import Counter
            counter = Counter()

            for label in labels:
                if labels.count(label) > 1:
                    counter.update([label])
                    identifiers.append(counter[label])
                else:
                    identifiers.append(None)

        self = cls(Vector(coordinate, label, identifier)
                   for coordinate, label, identifier in
                   zip(coordinates, labels, identifiers))

        return self

    @property
    def labels(self):
        return [vector.label for vector in self]

    @property
    def idlabels(self):
        return [vector.idlabel for vector in self]

    @property
    def coordinates(self):
        return [vector.coordinates for vector in self]

    def _unify(self, **kwargs):
        cls = self.__class__
        return cls(unify_map(self, **kwargs))

    def atoms(self, *args):
        return {atom for vector in self
                for atom in vector.atoms(*args)}

    def __getattr__(self, name):
        def get(*args, **kwargs):
            cls = self.__class__
            return cls(getattr(vector, name)(*args, **kwargs)
                       for vector in self)
        return get

    def __getstate__(self):
        return self.__dict__

    def __setstate__(self, state):
        self.__dict__ = state

    def sorted(self):
        def key_function(vector):
            return (vector.identifier, vector.label)
        return VectorList(sorted(self, key=key_function))

class VectorSet(set, VectorCollection):
    pass

class VectorList(list, VectorCollection):
    pass

class DiscreteForwardSpace(dict):
    @classmethod
    def ndspan(cls, *iterables, is_open=False) -> 'DiscreteForwardSpace':
        from itertools import product, repeat
        indices = [*map(lambda it: [*range(len(it))], iterables)]

        def point_from_indices(i_s):
            return Vector((*map(lambda it, i_: it[i_], iterables, i_s),))

        def neighbor_indices(i_s, is_open=False):
            return (i for j in range(len(iterables))
                      for i in (shifted_index(i_s, j, is_open=is_open),)
                      if i is not None)

        def shifted_index(i_s, i, is_open=False):
            list_i_s = [*i_s]
            list_i_s[i] = list_i_s[i] + 1

            if is_open:
                if list_i_s[i] >= len(iterables[i]):
                    return None
            else:
                list_i_s[i] %= len(iterables[i])

            return (*list_i_s,)

        return cls({point_from_indices(i_s):
                    {*map(point_from_indices,
                          neighbor_indices(i_s, is_open=is_open)),}
                    for i_s in product(*indices)})

    @classmethod
    def from_path(cls, path: Callable, steps: int,
                  is_open=True) -> 'DiscreteForwardSpace':
        points = path(steps)

        def next_point(i):
            i_ = i + 1
            if is_open:
                if i_ >= len(points):
                    return None
            else:
                i_ %= len(points)
            return points[i_]

        self = cls()

        for i in range(len(points)):
            next_point_ = next_point(i)
            next_point_set = {next_point_} if next_point_ is not None else set()
            if points[i] in self:
                self[points[i]].update(next_point_set)
            else:
                self[points[i]] = next_point_set

        return self

    def evalb(self):
        cls = self.__class__
        return cls({key.evalb(): {point.evalb() for point in value}
                    for key, value in self.items()})

    @property
    def coordinates(self):
        cls = self.__class__
        return cls({key.coordinates: {point.coordinates for point in value}
                    for key, value in self.items()})

    def inverse(self):
        cls = self.__class__
        return cls({point: {*(point_ for point_ in self
                          if point in self[point_])}
                    for point in self})


def numerify(expr: sym.Expr):
    """Convert all numerical symbols to their numerical values."""

    for A in filter(lambda A: A.is_real and A.is_constant(), expr.atoms()):
        expr = expr.subs({A: float(A)})

    for A in filter(lambda A: A.is_complex and A.is_constant(), expr.atoms()):
        expr = expr.subs({A: complex(A)})

    return expr
