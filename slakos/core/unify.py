import logging

import sympy as sym

def qualified_name(name: str) -> str:
    """Return `name` with unqualified symbols stripped"""
    from unicodedata import category
    id_start = ['Lu', 'Ll', 'Lt', 'Lm', 'Lo', 'Nl']

    if category(name[0]) not in id_start and name[0] != '_':
        name = name[1:]

    id_continue = id_start + ['Mn', 'Mc', 'Nd', 'Pc']

    name = ''.join(char for char in name
                   if category(char) in id_continue)

    return name

def fuzzy_subs(expr: sym.Expr, subs: dict) -> sym.Expr:
    """Replace occurences of symbols with names found in `subs`."""
    import re
    subs = {atom: value for key, value in subs.items()
            for atom in expr.atoms(sym.Symbol)
            if str(atom) == str(key)
            or (str(key).startswith('/')
                and re.match(str(key[1:]), str(atom)))}
    if subs:
        expr = expr.subs(subs)
    return expr

def unifys(string: str, **kwargs) -> sym.Symbol:
    """Returns the global symbol derived from sympified `string`.

    If the symbol doesn't exist, create it first and inject it into the global
    scope, then return it.

    """
    from inspect import currentframe
    kwargs['_frame'] = kwargs.get('_frame', currentframe().f_back)

    try:
        symbol = slakify(string)
        if hasattr(symbol, 'name'):
            symbol = unify_symbol(symbol, **kwargs)
    finally:
        del kwargs['_frame']

    return symbol

def unify_symbol(symbol: sym.Symbol, force_override=False,
                 **kwargs) -> sym.Symbol:
    """Push `symbol` to the global scope."""
    logger = logging.getLogger('slakos.base.unify_symbol')

    from inspect import currentframe
    frame = kwargs.get('_frame', currentframe().f_back)

    try:
        name = qualified_name(symbol.name)

        if name not in frame.f_globals or force_override:
            frame.f_globals[name] = symbol
            logger.debug("defining global qualified symbol name: {} "
                         "from name {}".format(name, symbol.name))
        else:
            logger.debug("symbol {} already defined".format(name))
            symbol = frame.f_globals[name]
    finally:
        del frame

    return symbol

def unify(obj, **kwargs):
    """Return `obj` with symbols pushed to or replaced from the global scope."""
    # Object that implements the
    logger = logging.getLogger('slakos.base.unify')

    from inspect import currentframe
    kwargs['_frame'] = kwargs.get('_frame', currentframe().f_back)

    try:
        if hasattr(obj, '_unify'):
            return obj._unify(**kwargs)

        # SymPy expression or something that behaves like it.
        elif hasattr(obj, 'atoms') and hasattr(obj, 'replace'):
            for symbol in obj.atoms(sym.Symbol):
                obj = obj.replace(symbol, unify_symbol(symbol, **kwargs))
        else:
            logger.debug("could not unify instance {}".format(obj))
    finally:
        del kwargs['_frame']

    return obj

def unify_map(obj, **kwargs):
    from inspect import currentframe
    kwargs['_frame'] = kwargs.get('_frame', currentframe().f_back)

    try:
        obj = map(lambda seq: unify(seq, **kwargs), obj)
    finally:
        del kwargs['_frame']

    return obj

def unifys_map(obj, **kwargs):
    from inspect import currentframe
    kwargs['_frame'] = kwargs.get('_frame', currentframe().f_back)

    from inspect import currentframe
    kwargs['_frame'] = kwargs.get('_frame', currentframe().f_back)

    try:
        obj = map(lambda seq: unifys(seq, **kwargs), obj)
    finally:
        del kwargs['_frame']

    return obj

def slakify(expr):
    """Sympify, but parse special LaTeX magic commands too."""
    if isinstance(expr, sym.Expr):
        return expr

    expr = str(expr)

    import re
    # inline
    expr = re.sub(r"\$", r"", expr)
    # \cdot
    expr = re.sub(r"\\cdot", r"*", expr)
    # \sqrt
    expr = re.sub(r"\\sqrt\{(.*?)}", r"sqrt(\1)", expr)
    # \frac
    expr = re.sub(r"\\frac\{ (.*?) \}\{ (.*?) \}", r"(\1) / (\2)", expr)
    # strip any remaining backslashes in front of symbols
    expr = re.sub(r"\\(\S*)", r"\1", expr)
    # Finally, sympify
    expr = sym.sympify(expr)

    return expr
