# Put reduce and mul into the function's local scope for slight speed-ups
from functools import reduce
from operator import add, mul, matmul, eq

def prod(iterable, *, reduce=reduce, mul=mul):
    return reduce(mul, iterable, 1)
