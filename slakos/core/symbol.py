import sympy as sym

global_registered_symbols = {}

def registered_symbol(symbol, override=False):
    if override or str(symbol) not in global_registered_symbols:
        global_registered_symbols[str(symbol)] = symbol
    return global_registered_symbols[str(symbol)]

def symbols(*args, override=False, **kwargs):
    return [registered_symbol(symbol)
            for symbol in sym.symbols(*args, override=override, **kwargs)]
