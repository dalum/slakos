# This file is part of Slakos.
#
# Slakos is free software: you can redistribute it and/or modify it under the terms
# of the GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# Slakos is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# Slakos.  If not, see <http://www.gnu.org/licenses/>.

def identity(expr):
    return expr

def costrig(expr):
    from sympy import trigsimp, cos, Expr, MatrixExpr
    return trigsimp(expr.rewrite(cos)) if isinstance(expr, (Expr, MatrixExpr)) else expr

def powfact(expr):
    from sympy import factor, cancel, powsimp
    return factor(cancel(powsimp(expr)))
