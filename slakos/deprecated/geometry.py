class LineSegment:
    def __init__(self, start: sym.Point, end: sym.Point) -> None:
        self.__dict__.update(locals())

from numpy import empty, ndarray

class PointMesh(set):
    def __init__(self, *coordinates):
        from itertools import product
        self.shape = tuple(len(xs) for xs in coordinates)
        self._array_mapping = {}
        for xs in product(*(enumerate(xs) for xs in coordinates)):
            self._array_mapping[tuple(x[0] for x in xs)] = tuple(x[1] for x in xs)
        self.update(self._array_mapping.values())

    def map(self, fn) -> ndarray:
        arr = empty(self.shape, dtype=object)
        for key, coord in self._array_mapping.items():
            arr[key] = fn(coord)
        return arr

    def select(self, mapping) -> ndarray:
        arr = empty(self.shape, dtype=object)
        for key, coord in self._array_mapping.items():
            arr[key] = mapping[coord]
        return arr

class PointSequence(list):
    def map(self, fn) -> list:
        return [fn(coord) for coord in self]

    def mapm(self, mapping) -> list:
        return [mapping[coord] for coord in self]
