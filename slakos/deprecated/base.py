# This file is part of Slakos.
# 
# Slakos is free software: you can redistribute it and/or modify it under the terms
# of the GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
# 
# Slakos is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE.  See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with
# Slakos.  If not, see <http://www.gnu.org/licenses/>.

import logging

import numpy as np
import sympy as sym

from typing import Callable, Iterable

class SortedDict(dict):
    def __init__(self, *args, key_function=lambda key: key, **kwargs):
        super().__init__(*args, **kwargs)
        self.key_function = key_function

    def __repr__(self):
        return '{{{}}}'.format(', '.join('{}: {}'.format(key, item)
                                         for key, item in self.items()))

    def __iter__(self):
        return iter(sorted(self.keys(), key=self.key_function))

    def items(self):
        return ((key, self[key]) for key in self)

class ConservativeDict(dict):
    def __setitem__(self, key, value):
        if key not in self:
            super().__setitem__(key, value)

class Vector:
    def __init__(self, coordinates: Iterable,
                 label: str = None, identifier: str = None, *,
                 convert=True) -> None:
        # Flatten column iterables
        coordinates = np.array([*coordinates]).flatten().tolist()

        if convert:
            super().__setattr__('coordinates', tuple(map(slakify, coordinates)))
        else:
            super().__setattr__('coordinates', tuple(coordinates))

        super().__setattr__('label', label)
        super().__setattr__('identifier', identifier)

    @property
    def idlabel(self):
        return '{}{}'.format(self.label, self.identifier)

    def atoms(self, *args):
        return {atom for x in self.coordinates for atom in x.atoms(*args)}

    def _unify(self, **kwargs):
        cls = self.__class__
        new_coordinates = (unify(x, **kwargs) for x in self.coordinates)
        return cls(new_coordinates, self.label, self.identifier)

    def with_label(self, label):
        cls = self.__class__
        return cls(self.coordinates, label, self.identifier)

    def evalb(self):
        new_coordinates = (*map(float, self.coordinates),)

        cls = self.__class__
        return cls(new_coordinates, self.label, self.identifier, convert=False)

    def __iter__(self):
        return iter(self.coordinates)

    def __getitem__(self, name):
        return self.coordinates[name]

    def __getattr__(self, name):
        def get(*args, **kwargs):
            new_coordinates = (
                getattr(x, name, lambda *args, **kwargs: x)(*args, **kwargs)
                for x in self.coordinates)

            cls = self.__class__
            return cls(new_coordinates, self.label, self.identifier)
        return get

    def __setattr__(self, name, value):
        raise AttributeError("cannot modify attributes of Vector.")

    def __delattr__(self, name):
        raise AttributeError("cannot delete attributes of Vector.")

    def __getstate__(self):
        return (tuple(map(sym.srepr, self.coordinates)),
                self.label, self.identifier)

    def __setstate__(self, state):
        self.__init__(*state)

    def __abs__(self):
        return sym.sqrt(sum(x ** 2 for x in self.coordinates))

    def __len__(self):
        return len(self.coordinates)

    def __repr__(self):
        return "Vector({}, {}, {})".format(self.coordinates,
                                           self.label, self.identifier)

    def __hash__(self):
        return hash(self.coordinates) + hash(self.label)

    def __eq__(self, other):
        if isinstance(other, type(self)):
            return (self.label == other.label and
                    self.coordinates == other.coordinates)

    def __neg__(self):
        cls = self.__class__
        new_coordinates = tuple(-x for x in self.coordinates)
        new_label = '(-{})'.format(self.label)
        return cls(new_coordinates, new_label, self.identifier)

    def __sub__(self, other):
        cls = self.__class__
        if isinstance(other, cls):
            if len(self) != len(other):
                raise ValueError("vector dimensions {}, {} do not match"
                                 "".format(len(self), len(other)))

            new_coordinates = tuple(self.coordinates[i] - other.coordinates[i]
                                    for i in range(len(self)))
            new_label = '({} - {})'.format(self.label, other.label)
            return cls(new_coordinates, new_label, None)

        return NotImplemented

    def __add__(self, other):
        cls = self.__class__
        if isinstance(other, cls):
            if len(self) != len(other):
                raise ValueError("vector dimensions {}, {} do not match"
                                 "".format(len(self), len(other)))

            new_coordinates = tuple(self.coordinates[i] + other.coordinates[i]
                                    for i in range(len(self)))
            new_label = '(' + self.label + ' + ' + other.label + ')'
            return cls(new_coordinates, new_label, None)

        return NotImplemented

    def __mul__(self, other):
        try:
            cls = self.__class__
            new_coordinates = tuple(self.coordinates[i] * other
                                    for i in range(len(self)))
            new_label = '({} * {})'.format(other, self.label)
            return cls(new_coordinates, new_label, None)
        except:
            raise ValueError("cannot multiply vector with instance of type {}"
                             "".format(type(other)))

        return NotImplemented
    __rmul__ = __mul__

    def __matmul__(self, other):
        try:
            from functools import reduce
            from operator import add
            return reduce(add, (self.coordinates[i] * other.coordinates[i]
                                for i in range(len(self))))
        except:
            raise ValueError("cannot matrix multiply vector with instance of "
                             "type {}".format(type(other)))

        return NotImplemented

class VectorCollection:
    @classmethod
    def from_sequences(cls, coordinates: Iterable[Iterable],
                       labels: Iterable = None,
                       axis: int = 0):
        """Create a VectorCollection from sequences of labels and coordinates.

        Parameters
        ----------
        coordinates : two-dimensional sequence
            Sequence of coordinates to be assigned to the vectors.
        labels : sequence
            Sequence of labels to be applied to the vectors. If None (default)
            the vectors will be labelled by their index.
        axis : int, optional
            Axis along which to separate vectors and dimensions. If `axis` is 0
            (default), the coordinates are assumed to be aligned along axis 1
            and vice versa. See notes for more information.

        Usage Notes
        -----------
        If coordinates are arranged in columns, specify `axis=1`.

        """
        coordinates = np.array(coordinates)
        if axis == 1:
            coordinates = coordinates.swapaxes(0, 1)

        if labels is None:
            labels = [*map(str, range(len(coordinates)))]
            identifiers = [None for _ in range(len(labels))]
        else:
            identifiers = []

            from collections import Counter
            counter = Counter()

            for label in labels:
                if labels.count(label) > 1:
                    counter.update([label])
                    identifiers.append(counter[label])
                else:
                    identifiers.append(None)

        self = cls(Vector(coordinate, label, identifier)
                   for coordinate, label, identifier in
                   zip(coordinates, labels, identifiers))

        return self

    @property
    def labels(self):
        return [vector.label for vector in self]

    @property
    def idlabels(self):
        return [vector.idlabel for vector in self]

    @property
    def coordinates(self):
        return [vector.coordinates for vector in self]

    def _unify(self, **kwargs):
        cls = self.__class__
        return cls(unify_map(self, **kwargs))

    def atoms(self, *args):
        return {atom for vector in self
                for atom in vector.atoms(*args)}

    def __getattr__(self, name):
        def get(*args, **kwargs):
            cls = self.__class__
            return cls(getattr(vector, name)(*args, **kwargs)
                       for vector in self)
        return get

    def __getstate__(self):
        return self.__dict__

    def __setstate__(self, state):
        self.__dict__ = state

    def sorted(self):
        def key_function(vector):
            return (vector.identifier, vector.label)
        return VectorList(sorted(self, key=key_function))

class VectorSet(set, VectorCollection):
    pass

class VectorList(list, VectorCollection):
    pass

class DiscreteForwardSpace(dict):
    @classmethod
    def ndspan(cls, *iterables, is_open=False) -> 'DiscreteForwardSpace':
        from itertools import product, repeat
        indices = [*map(lambda it: [*range(len(it))], iterables)]

        def point_from_indices(i_s):
            return Vector((*map(lambda it, i_: it[i_], iterables, i_s),))

        def neighbor_indices(i_s, is_open=False):
            return (i for j in range(len(iterables))
                      for i in (shifted_index(i_s, j, is_open=is_open),)
                      if i is not None)

        def shifted_index(i_s, i, is_open=False):
            list_i_s = [*i_s]
            list_i_s[i] = list_i_s[i] + 1

            if is_open:
                if list_i_s[i] >= len(iterables[i]):
                    return None
            else:
                list_i_s[i] %= len(iterables[i])

            return (*list_i_s,)

        return cls({point_from_indices(i_s):
                    {*map(point_from_indices,
                          neighbor_indices(i_s, is_open=is_open)),}
                    for i_s in product(*indices)})

    @classmethod
    def from_path(cls, path: Callable, steps: int,
                  is_open=True) -> 'DiscreteForwardSpace':
        points = path(steps)

        def next_point(i):
            i_ = i + 1
            if is_open:
                if i_ >= len(points):
                    return None
            else:
                i_ %= len(points)
            return points[i_]

        self = cls()

        for i in range(len(points)):
            next_point_ = next_point(i)
            next_point_set = {next_point_} if next_point_ is not None else set()
            if points[i] in self:
                self[points[i]].update(next_point_set)
            else:
                self[points[i]] = next_point_set

        return self

    def evalb(self):
        cls = self.__class__
        return cls({key.evalb(): {point.evalb() for point in value}
                    for key, value in self.items()})

    @property
    def coordinates(self):
        cls = self.__class__
        return cls({key.coordinates: {point.coordinates for point in value}
                    for key, value in self.items()})

    def inverse(self):
        cls = self.__class__
        return cls({point: {*(point_ for point_ in self
                          if point in self[point_])}
                    for point in self})

def eprint(mode: str, expr: sym.Expr, **settings) -> None:
    settings['mode'] = mode
    print(SlakosLatexPrinter(settings).doprint(expr))

from sympy.printing.latex import LatexPrinter

class SlakosLatexPrinter(LatexPrinter):
    _default_settings = {
        "order": None,
        "mode": "plain",
        "itex": False,
        "fold_frac_powers": True,
        "fold_func_brackets": False,
        "fold_short_frac": None,
        "long_frac_ratio": 2,
        "mul_symbol": None,
        "inv_trig_style": "abbreviated",
        "mat_str": None,
        "mat_delim": "[",
        "symbol_names": {},
        "substitutions": {},
        "prefix": "",
        "suffix": "",
        "columns": 3,
    }

    printmethod = "_latex"

    def __init__(self, settings=None):
        from sympy.printing.printer import Printer
        Printer.__init__(self, settings)

        if self._settings['fold_short_frac'] is None and \
                self._settings['mode'] == 'inline':
            self._settings['fold_short_frac'] = True

        mul_symbol_table = {
            None: r" ",
            "ldot": r" \,.\, ",
            "dot": r" \cdot ",
            "times": r" \times "
        }

        self._settings['mul_symbol_latex'] = \
            mul_symbol_table[self._settings['mul_symbol']]

        self._settings['mul_symbol_latex_numbers'] = \
            mul_symbol_table[self._settings['mul_symbol'] or 'dot']

        self._delim_dict = {'(': ')', '[': ']'}

    def _print_Transpose(self, expr):
        mat = expr.arg
        if isinstance(mat, sym.Symbol):
            return r"%s^T" % self._print(mat)
        elif isinstance(mat, sym.Function):
            fn, args = self._print(mat).split(r'{\left')
            return r"%s^T{\left%s" % (fn, args)
        else:
            return r"\left(%s\right)^T" % self._print(mat)

    def _print_Adjoint(self, expr):
        mat = expr.arg
        if isinstance(mat, sym.Symbol):
            return r"%s^\dag" % self._print(mat)
        elif isinstance(mat, sym.Function):
            fn, args = self._print(mat).split(r'{\left')
            return r"%s^\dag{\left%s" % (fn, args)
        else:
            return r"\left(%s\right)^\dag" % self._print(mat)

    def _print_conjugate(self, expr, exp=None):
        if isinstance(expr.args[0], sym.Symbol):
            tex = r"%s^{*}" % self._print(expr.args[0])
        elif isinstance(expr.args[0], sym.Function):
            fn, args = self._print(expr.args[0]).split(r'{\left')
            tex = r"%s^{*}{\left%s" % (fn, args)
        else:
            tex = r"\overline{%s}" % self._print(expr.args[0])

        if exp is not None:
            return r"%s^{%s}" % (tex, exp)
        else:
            return tex

    def _hprint_Function(self, func):
        from sympy.printing.latex import accepted_latex_functions

        func = self._deal_with_super_sub(func)

        if func in accepted_latex_functions:
            name = r"\%s" % func
        else:
            name = func

        return name


    def _deal_with_super_sub(self, string):
        # Unlike SymPy's LaTeX printer, we put commas between super- and
        # subscripts in names.
        from sympy.printing.conventions import split_super_sub
        from sympy.printing.latex import translate

        name, supers, subs = split_super_sub(string)

        name = translate(name)
        supers = [translate(sup) for sup in supers]
        subs = [translate(sub) for sub in subs]

        # glue all items together:
        if len(supers) > 0:
            name += "^{%s}" % " ".join(supers)
        if len(subs) > 0:
            name += "_{%s}" % " ".join(subs)

        return name

    def doprint(self, expr):
        from sympy.printing.printer import Printer
        if (self._settings['mode'] == 'inline' and
            hasattr(expr, '_latex_inline')):
            tex = expr._latex_inline(self)
            self._settings['mode'] = 'plain'
        elif (self._settings['mode'] == 'table' and
            hasattr(expr, '_latex_table')):
            tex = expr._latex_table(self)
            self._settings['mode'] = 'plain'
        else:
            tex = Printer.doprint(self, expr)

        tex = self._final_substitutions(tex)

        prefix, suffix = self._settings['prefix'], self._settings['suffix']
        tex = prefix + tex + suffix

        if self._settings['mode'] == 'plain':
            return tex
        elif self._settings['mode'] == 'inline':
            return r"$%s$" % tex
        elif self._settings['itex']:
            return r"$$%s$$" % tex
        else:
            env_str = self._settings['mode']
            return r"\begin{%s}%s\end{%s}" % (env_str, tex, env_str)

    def _final_substitutions(self, tex):
        import re
        for pattern, repl in self._settings['substitutions'].items():
            tex = re.sub(pattern, repl, tex)
        return tex

def slatex(expr, **settings):
    return SlakosLatexPrinter(settings).doprint(expr)

def slakify(expr):
    """Sympify, but parse special LaTeX magic commands too."""
    if isinstance(expr, sym.Expr):
        return expr

    expr = str(expr)

    import re
    # inline
    expr = re.sub(r"\$", r"", expr)
    # \cdot
    expr = re.sub(r"\\cdot", r"*", expr)
    # \sqrt
    expr = re.sub(r"\\sqrt\{(.*?)}", r"sqrt(\1)", expr)
    # \frac
    expr = re.sub(r"\\frac\{ (.*?) \}\{ (.*?) \}", r"(\1) / (\2)", expr)
    # strip any remaining backslashes in front of symbols
    expr = re.sub(r"\\(\S*)", r"\1", expr)
    # Finally, sympify
    expr = sym.sympify(expr)

    return expr

def qualified_name(name: str) -> str:
    """Return `name` with unqualified symbols stripped"""
    from unicodedata import category
    id_start = ['Lu', 'Ll', 'Lt', 'Lm', 'Lo', 'Nl']

    if category(name[0]) not in id_start and name[0] != '_':
        name = name[1:]

    id_continue = id_start + ['Mn', 'Mc', 'Nd', 'Pc']

    name = ''.join(char for char in name
                   if category(char) in id_continue)

    return name

def force_subs(expr: sym.Expr, subs: dict) -> sym.Expr:
    """Replace occurences of symbols with names found in `subs`."""
    subs = {atom: value for key, value in subs.items()
            for atom in expr.atoms(sym.Symbol)
            if str(atom) == str(key)}
    expr = expr.subs(subs)
    return expr

def unifys(string: str, **kwargs) -> sym.Symbol:
    """Returns the global symbol derived from sympified `string`.

    If the symbol doesn't exist, create it first and inject it into the global
    scope, then return it.

    """
    from inspect import currentframe
    kwargs['_frame'] = kwargs.get('_frame', currentframe().f_back)

    try:
        symbol = slakify(string)
        if hasattr(symbol, 'name'):
            symbol = unify_symbol(symbol, **kwargs)
    finally:
        del kwargs['_frame']

    return symbol

def unify_symbol(symbol: sym.Symbol, force_override=False,
                 **kwargs) -> sym.Symbol:
    """Push `symbol` to the global scope."""
    logger = logging.getLogger('slakos.base.unify_symbol')

    from inspect import currentframe
    frame = kwargs.get('_frame', currentframe().f_back)

    try:
        name = qualified_name(symbol.name)

        if name not in frame.f_globals or force_override:
            frame.f_globals[name] = symbol
            logger.debug("defining global qualified symbol name: {} "
                         "from name {}".format(name, symbol.name))
        else:
            logger.debug("symbol {} already defined".format(name))
            symbol = frame.f_globals[name]
    finally:
        del frame

    return symbol

def unify(obj, **kwargs):
    """Return `obj` with symbols pushed to or replaced from the global scope."""
    # Object that implements the
    logger = logging.getLogger('slakos.base.unify')

    from inspect import currentframe
    kwargs['_frame'] = kwargs.get('_frame', currentframe().f_back)

    try:
        if hasattr(obj, '_unify'):
            return obj._unify(**kwargs)

        # SymPy expression or something that behaves like it.
        elif hasattr(obj, 'atoms') and hasattr(obj, 'replace'):
            for symbol in obj.atoms(sym.Symbol):
                obj = obj.replace(symbol, unify_symbol(symbol, **kwargs))
        else:
            logger.debug("could not unify instance {}".format(obj))
    finally:
        del kwargs['_frame']

    return obj

def unify_map(obj, **kwargs):
    from inspect import currentframe
    kwargs['_frame'] = kwargs.get('_frame', currentframe().f_back)

    try:
        obj = map(lambda seq: unify(seq, **kwargs), obj)
    finally:
        del kwargs['_frame']

    return obj

def unifys_map(obj, **kwargs):
    from inspect import currentframe
    kwargs['_frame'] = kwargs.get('_frame', currentframe().f_back)

    from inspect import currentframe
    kwargs['_frame'] = kwargs.get('_frame', currentframe().f_back)

    try:
        obj = map(lambda seq: unifys(seq, **kwargs), obj)
    finally:
        del kwargs['_frame']

    return obj

def numerify(expr: sym.Expr):
    """Convert all numerical symbols to their numerical values."""

    for A in filter(lambda A: A.is_real and A.is_constant(), expr.atoms()):
        expr = expr.subs({A: float(A)})

    for A in filter(lambda A: A.is_complex and A.is_constant(), expr.atoms()):
        expr = expr.subs({A: complex(A)})

    return expr

def slambdify(symbols, expr):
    dummy_fn = sym.lambdify(symbols, expr, dummify=True, modules='numpy')

    from inspect import signature
    dummy_fn_args = [*signature(dummy_fn).parameters.keys()]
    qualified_symbols = [*map(lambda expr: qualified_name(expr.name), symbols)]

    args_map = dict(zip(qualified_symbols, dummy_fn_args))
    args_str = ', '.join(qualified_symbols)

    definition = r'''def slambdified_function({args}):
    """Slambdified function.

    Parameters
    ----------
    {args}

    """
    kwargs = {{args_map[key]: value for key, value in locals().items()}}
    return fn(**kwargs)

    '''.format(args=args_str)

    partial_function_code = compile(
        definition, "<slambdified function source>", "exec")

    partial_globals = {}
    eval(partial_function_code, {'args_map': args_map, 'fn': dummy_fn},
         partial_globals)

    slambdified_function = partial_globals['slambdified_function']

    return slambdified_function
