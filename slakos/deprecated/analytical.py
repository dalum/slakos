# This file is part of Slakos.
#
# Slakos is free software: you can redistribute it and/or modify it under the terms
# of the GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# Slakos is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# Slakos.  If not, see <http://www.gnu.org/licenses/>.

import logging

import numpy as np
import sympy as sym

from typing import Callable, Mapping, Iterable

from .base import (Vector, VectorCollection, VectorSet, VectorList,
                   force_subs, unify, unify_map, unifys, unifys_map,
                   unify_symbol, numerify,
                   qualified_name,
                   slakify, eprint, slatex,
                   slambdify)
from .simplify import identity, powfact

from sympy import sqrt, exp, cos, sin

class InteratomicMatrixTable(dict):
    def __init__(self, labels: list, indices: list,
                 equivalences: dict = {},
                 ext_string: str = "_{a}{b}_{bond}__{l1}{l2},__({index})",
                 ext_string_inverse: str = None) -> None:
        from itertools import product
        for l1, l2, index in product(labels, labels, indices):
            self[l1, l2, index] = self.generate(
                (l1, l2), index, ext_string, ext_string_inverse, equivalences)

    def generate(self, labels: list, index,
                 ext_string: str, ext_string_inverse: str,
                 equivalences: dict = {}) -> dict:
        labels = (equivalences.get(labels[0], labels[0]),
                  equivalences.get(labels[1], labels[1]))
        interchangable = labels[0] == labels[1]

        if ext_string_inverse is None:
            ext_string_inverse = ext_string

        from itertools import (combinations_with_replacement, combinations,
                               permutations, product)
        V = ConservativeDict()

        if index is None:
            V.update({**{(a,): sym.Symbol("epsilon_{a}__{0}".format(labels[0], a=a),
                                          real=True) for a in 'spd'}})
            E = ConservativeDict({a: V[a,] for a in 'spd'})
            return dict(E)

        r_0, r = sym.symbols("r_0, r", real=True, positive=True)

        for bond, ab in zip(['sigma', 'pi', 'delta'], ['spd', 'pd', 'd']):
            for a, b in combinations_with_replacement(ab, 2):
                equal = (a == b or interchangable)

                ordered_labels = (sorted(labels) if equal else labels)
                format_dict = dict(l1=ordered_labels[0], l2=ordered_labels[1],
                                   a=a, b=b, bond=bond, index=index)

                ext = ext_string.format(**format_dict)
                V_0 = sym.Symbol('V' + ext, real=True)
                alpha = sym.Symbol('alpha' + ext, real=True)

                V[a, b, bond] = V_0 + alpha * (r_0 - r) / r_0

                if not equal:
                    format_dict["l1"], format_dict["l2"] = (
                        format_dict["l2"], format_dict["l1"])
                    ext = ext_string_inverse.format(**format_dict)
                    V_0 = sym.Symbol('V' + ext, real=True)
                    alpha = sym.Symbol('alpha' + ext, real=True)

                V[b, a, bond] = V_0 + alpha * (r_0 - r) / r_0
        E = ConservativeDict()

        (s, p, d), (sigma, pi, delta) = ('spd', ('sigma', 'pi', 'delta'))
        l_, m_, n_ = sym.symbols("l,m,n")

        for (l, m, n), (x, y, z) in [((l_, m_, n_), ("x", "y", "z")),
                                     ((m_, n_, l_), ("y", "z", "x")),
                                     ((n_, l_, m_), ("z", "x", "y"))]:
            s = 's'
            p_x, p_y, p_z = 'p_{'+x+'}', 'p_{'+y+'}', 'p_{'+z+'}'
            d_xy, d_yz, d_zx = 'd_{'+x+y+'}', 'd_{'+y+z+'}', 'd_{'+z+x+'}'
            d_x2y2, d_z2 = 'd_{x^2-y^2}', 'd_{z^2}'

            E[s,s] = V[s,s,sigma]
        
            E[s,p_x] = l * V[s,p,sigma]
            E[p_x,s] = -l * V[p,s,sigma]
        
            E[p_x, p_x] = ((l ** 2) * V[p,p,sigma] + (1 - l ** 2) * V[p,p,pi])
            E[p_x, p_y] = l * m * V[p,p,sigma] - m * l * V[p,p,pi]
            E[p_x, p_z] = l * n * V[p,p,sigma] - n * l * V[p,p,pi]
        
            E[s, d_xy] = sqrt(3) * l * m * V[s,d,sigma]
            E[d_xy, s] = sqrt(3) * l * m * V[d,s,sigma]
        
            E[s, d_x2y2] = sqrt(3) / 2 * (l ** 2 - m ** 2) * V[s,d,sigma]
            E[d_x2y2, s] = sqrt(3) / 2 * (l ** 2 - m ** 2) * V[d,s,sigma]
        
            E[s, d_z2] = (n ** 2 - (l ** 2 + m ** 2) / 2) * V[s,d,sigma]
            E[d_z2, s] = (n ** 2 - (l ** 2 + m ** 2) / 2) * V[d,s,sigma]
        
            E[p_x, d_xy] = (sqrt(3) * l ** 2 * m * V[p,d,sigma] +
                               m * (1 - 2 * l ** 2) * V[p,d,pi])
            E[d_xy, p_x] = -(sqrt(3) * l ** 2 * m * V[d,p,sigma] +
                             m * (1 - 2 * l ** 2) * V[d,p,pi])
        
            E[p_x, d_yz] = (sqrt(3) * l * m * n * V[p,d,sigma] -
                            2 * l * m * n * V[p,d,pi])
            E[d_yz, p_x] = -(sqrt(3) * l * m * n * V[d,p,sigma] -
                             2 * l * m * n * V[d,p,pi])
        
            E[p_x, d_zx] = (sqrt(3) * l ** 2 * n * V[p,d,sigma] +
                            n * (1 - 2 * l ** 2) * V[p,d,pi])
            E[d_zx, p_x] = -(sqrt(3) * l ** 2 * m * V[d,p,sigma] +
                             m * (1 - 2 * l ** 2) * V[d,p,pi])
        
            E[p_x, d_x2y2] = (sqrt(3)/2 * l * (l ** 2 - m ** 2) * V[p,d,sigma] +
                              l * (1 - l ** 2 + m ** 2) * V[p,d,pi])
            E[d_x2y2, p_x] = -(sqrt(3)/2 * l * (l ** 2 - m ** 2) * V[d,p,sigma] +
                               l * (1 - l ** 2 + m ** 2) * V[d,p,pi])
        
            E[p_y, d_x2y2] = (sqrt(3)/2 * m * (l ** 2 - m ** 2) * V[p,d,sigma] -
                              m * (1 + l ** 2 - m ** 2) * V[p,d,pi])
            E[d_x2y2, p_y] = -(sqrt(3)/2 * m * (l ** 2 - m ** 2) * V[d,p,sigma] -
                               m * (1 + l ** 2 - m ** 2) * V[d,p,pi])
        
            E[p_z, d_x2y2] = (sqrt(3)/2 * n * (l ** 2 - m ** 2) * V[p,d,sigma] -
                              n * (l ** 2 - m ** 2) * V[p,d,pi])
            E[d_x2y2, p_z] = -(sqrt(3)/2 * n * (l ** 2 - m ** 2) * V[d,p,sigma] -
                               n * (l ** 2 - m ** 2) * V[d,p,pi])
        
            E[p_x, d_z2] = (l * (n ** 2 - (l ** 2 + m ** 2) / 2) * V[p,d,sigma] -
                            sqrt(3) * l * n ** 2 * V[p,d,pi])
            E[d_z2, p_x] = -(l * (n ** 2 - (l ** 2 + m ** 2) / 2) * V[d,p,sigma] -
                             sqrt(3) * l * n ** 2 * V[d,p,pi])
        
            E[p_y, d_z2] = (m * (n ** 2 - (l ** 2 + m ** 2) / 2) * V[p,d,sigma] -
                            sqrt(3) * m * n ** 2 * V[p,d,pi])
            E[d_z2, p_y] = -(m * (n ** 2 - (l ** 2 + m ** 2) / 2) * V[d,p,sigma] -
                             sqrt(3) * m * n ** 2 * V[d,p,pi])
        
            E[p_z, d_z2] = (n * (n ** 2 - (l ** 2 + m ** 2) / 2) * V[p,d,sigma] +
                            sqrt(3) * n * (l ** 2 + m ** 2) * V[p,d,pi])
            E[d_z2, p_z] = -(n * (n ** 2 - (l ** 2 + m ** 2) / 2) * V[d,p,sigma] +
                             sqrt(3) * n * (l ** 2 + m ** 2) * V[d,p,pi])
        
        
            E[d_xy, d_xy] = (3 * l ** 2 * m ** 2 * V[d,d,sigma] +
                             (l ** 2 + m ** 2 - 4 * l ** 2 * m ** 2) * V[d,d,pi] +
                             (n ** 2 + l ** 2 * m ** 2) * V[d,d,delta])
        
            E[d_xy, d_yz] = E[d_yz, d_xy] = (3 * l * m ** 2 * n * V[d,d,sigma] +
                                             l * n * (1 - 4 * m ** 2) * V[d,d,pi] +
                                             l * n * (m ** 2 - 1) * V[d,d,delta])
        
            E[d_xy, d_zx] = E[d_zx, d_xy] = (3 * l ** 2 * m * n * V[d,d,sigma] +
                                             m * n * (1 - 4 * l ** 2) * V[d,d,pi] +
                                             m * n * (l ** 2 - 1) * V[d,d,delta])
        
            E[d_xy, d_x2y2] = (3 * l * m * (l **2 - m ** 2) * V[d,d,sigma] / 2 +
                               2 * l * m * (m ** 2 - l ** 2) * V[d,d,pi] +
                               l * m * (l ** 2 - m ** 2) / 2 * V[d,d,delta])
            E[d_x2y2, d_xy] = E[d_xy, d_x2y2]
        
            E[d_yz, d_x2y2] = (3 * m * n * (l **2 - m ** 2) * V[d,d,sigma] / 2 -
                               m * n * (1 + 2 * (l ** 2 - m ** 2)) * V[d,d,pi] +
                               m * n * (1 + (l ** 2 - m ** 2) / 2) * V[d,d,delta])
            E[d_x2y2, d_yz] = E[d_yz, d_x2y2]
        
            E[d_zx, d_x2y2] = (3 * n * l * (l **2 - m ** 2) * V[d,d,sigma] / 2 +
                               n * l * (1 - 2 * (l ** 2 - m ** 2)) * V[d,d,pi] -
                               n * l * (1 - (l ** 2 - m ** 2) / 2) * V[d,d,delta])
            E[d_x2y2, d_zx] = E[d_zx, d_x2y2]
        
            E[d_xy, d_z2] = sqrt(3) * ((l * m * (n ** 2 - (l ** 2 + m ** 2) / 2)) *
                                       V[d,d,sigma] -
                                       2 * l * m * n ** 2 * V[d,d,pi] +
                                       (l * m * (1 + n ** 2) / 2) * V[d,d,delta])
            E[d_z2, d_xy] = E[d_xy, d_z2]
        
            E[d_yz, d_z2] = sqrt(3) * ((m * n * (n ** 2 - (l ** 2 + m ** 2) / 2)) *
                                       V[d,d,sigma] +
                                       m * n * (l ** 2 + m ** 2 - n ** 2) * V[d,d,pi] -
                                       (m * n * (l ** 2 + m ** 2) / 2) * V[d,d,delta])
            E[d_z2, d_yz] = E[d_yz, d_z2]
        
            E[d_zx, d_z2] = sqrt(3) * ((l * n * (n ** 2 - (l ** 2 + m ** 2) / 2)) *
                                       V[d,d,sigma] +
                                       l * n * (l ** 2 + m ** 2 - n ** 2) * V[d,d,pi] -
                                       (l * n * (l ** 2 + m ** 2) / 2) * V[d,d,delta])
            E[d_z2, d_zx] = E[d_zx, d_z2]
        
            E[d_x2y2, d_x2y2] = (3 * (l ** 2 - m ** 2) ** 2 / 4 * V[d,d,sigma] +
                                 (l ** 2 + m ** 2 - (l ** 2 - m ** 2) ** 2) *
                                 V[d,d,pi] +
                                 (n ** 2 + (l ** 2 + m ** 2) ** 2 / 4) *
                                 V[d,d,delta])
        
            E[d_x2y2, d_z2] = sqrt(3) * ((l ** 2 - m ** 2) * (n ** 2 -
                                                              (l ** 2 + m ** 2) / 2) *
                                         V[d,d,sigma] / 2 +
                                         n ** 2 * (m ** 2 - l ** 2) * V[d,d,pi] +
                                         ((1 + n ** 2) * (l ** 2 - m ** 2) / 4) *
                                         V[d,d,delta])
            E[d_z2, d_x2y2] = E[d_x2y2, d_z2]
        
            E[d_z2, d_z2] = ((n ** 2 - (l ** 2 + m ** 2) / 2) ** 2 *
                             V[d,d,sigma] +
                             3 * n ** 2 * (l ** 2 + m ** 2) * V[d,d,pi] +
                             3 * (l ** 2 + m ** 2) ** 2 / 4 * V[d,d,delta])
        
        return dict(E)

    def __call__(self, vector: Vector, orbitals: tuple, labels: tuple,
                 index, equilibrium_subs: dict) -> sym.Expr:
        """Return an evaluated hopping term between two atoms.

        Parameters
        ----------
        vector : Vector
            The vector pointing between the two atoms.
        orbitals : tuple
            Ordered names of the orbitals in question on the two atoms.
        labels : tuple
            Ordered labels of the two atoms.
        index : hashable object
            Index in the table. The special index None is reserved for onsite
            energies.
        equilibrium_subs : dict
            Substitutions representing the atoms in unstrained conditions.

        Returns
        -------
        expr : sym.Expr
            A SymPy expression satisfying the given constraints.

        """
        vector_norm = abs(vector)
        normalized_vector = vector * (vector_norm ** (-1))

        equilibrium_vector = force_subs(vector, equilibrium_subs)
        equilibrium_norm = abs(equilibrium_vector)

        if vector_norm == 0 and index is None:
            if labels[0] == labels[1] and orbitals[0] == orbitals[1]:
                return self[labels[0], labels[1], index][orbitals[0][0]]
            else:
                return sym.S(0)

        elif vector_norm == 0:
            raise KeyError("vector norm symbolically zero, but index "
                           "not None.")

        if (*labels, index) not in self:
            raise KeyError("invalid key primary: {}.".format((*labels, index)))
        if (*orbitals,) not in self[labels[0], labels[1], index]:
            raise KeyError("invalid key secondary: {}.".format((*orbitals,)))

        # Raw form
        expr = self[labels[0], labels[1], index][orbitals[0], orbitals[1]]
        # With direction cosines
        expr = force_subs(
            expr, dict(zip(['l', 'm', 'n'], normalized_vector.coordinates)))
        # Substituting for displacements
        expr = force_subs(
            expr, {'r_0': equilibrium_norm, 'r': vector_norm})

        return expr

def matrix_element_neighbor_sum(vectors: VectorCollection, k: Vector,
                                table: InteratomicMatrixTable,
                                orbitals: list, labels: Iterable, index: int,
                                equilibrium_subs: dict) -> sym.Expr:
    if len(vectors) == 0:
        return sym.S(0)

    from functools import reduce
    from operator import add

    return reduce(add, (table(delta_r, orbitals, labels, index,
                              equilibrium_subs) *
                        sym.exp(sym.S("I") * (k @ delta_r))
                        for delta_r in vectors))

import sympy as sym
def translation_vector_span(vectors: VectorCollection,
                            sizes: list = []) -> sym.Matrix:
    """Return a span of translation vectors into nearby unit cells.

    The area covered by the vectors are given by:

    len(sizes)^dim(tvectors).

    """
    from itertools import product, chain, repeat
    from functools import reduce
    from operator import add
    sizes = chain(sizes, repeat(range(-1, 2), len(vectors) - len(sizes)))
    return VectorSet(reduce(add, [n[i] * r for i, r in enumerate(vectors)])
                     for n in product(*sizes))

def atomic_hopping_vectors(atom1: Vector, atom2: Vector, tvecs: sym.Matrix):
    """Return all possible hopping vectors between atoms at sites m, n.

    tvecs is assumed to be a translation vector span, generated by, for
    instance, translation_vector_span.

    """
    return VectorSet(atom2 - atom1 + tvec for tvec in tvecs)

def vector_magnitude_filter(vectors: VectorCollection, mag: sym.Expr,
                            subs: dict = {}) -> sym.Matrix:
    """Return a VectorSet of vectors """
    mag = mag.subs(subs)
    return VectorSet(vec for vec in vectors
                     if (abs(vec.subs(subs)) - mag).simplify() == 0)

def vector_magnitudes(vectors: VectorCollection, subs: dict) -> list:
    """Return a sorted list of unique vector magnitudes."""
    magnitudes = {force_subs(abs(vec), subs) for vec in vectors}
    return sorted(magnitudes, key=lambda expr: expr.subs(subs))

class UnitCell:
    def __init__(self, atoms: VectorList, translation_vectors: VectorList,
                 equilibrium_subs: dict, relative_subs: dict,
                 vector_span_sizes: list, initialize: bool = True,
                 magnitudes: list = [], **kwargs) -> None:
        """Create a unit cell.

        Parameters
        ----------

        atoms : vector list
            Atomic positions.
        translation_vectors : vector list
            Translation vectors of the unit cell.
        equilibrium_subs : dict
            Substitutions for equilibrium (unstrained) conditions.
        relative_subs : dict
            Substitutions representing the relative sizes of quantities used
            when determining the magnitudes. See notes for more information.
        vector_span_sizes : iterable
            Multiplication factors determining the span of translation vectors
            to be taken into account, when finding neighbours.
        initialize : bool
            When true (default) hopping vectors will be determined at
            instantiation. Turning off initialization may be useful when
            creating large mock unit cells not intended for computations.
        magnitudes : ordered sequence
            If an empty sequence is passed (default), magnitudes will be
            computed from the calculated hopping vectors. If non-empty,
            magnitudes for neighbour numbers will be determined solely from
            this sequence.

        Notes
        -----

        The equilibrium and relative substitutions are used when determining
        neighbours. As such, a material may be strained so as to change the
        actual ordering of the nearest and next--nearest neighbours, but this
        will not be reflected in the magnitudes or the filtered vectors
        returned by `neighbor_hopping_vectors`. The relative substitutions are
        performed after the equilibrium substitutions, hence the relative
        substitutions should contain all equilibrium constants.

        """
        self.atoms = atoms
        self.translation_vectors = translation_vectors

        self.equilibrium_subs = equilibrium_subs
        self.relative_subs = {
            key: (force_subs(value, relative_subs)
                  if hasattr(value, 'subs') else value)
            for key, value in self.equilibrium_subs.items()}

        self.vector_span_sizes = vector_span_sizes
        self.magnitudes = magnitudes

        if initialize:
            self.initialize(**kwargs)

    def initialize(self, magnitudes: list = [], **kwargs):
        from concurrent.futures import ProcessPoolExecutor
        with ProcessPoolExecutor() as e:

            tvecspan = translation_vector_span(self.translation_vectors,
                                               self.vector_span_sizes)

            futures = {(atom1, atom2): e.submit(atomic_hopping_vectors,
                                                atom1, atom2, tvecspan,
                                                **kwargs)
                     for atom1 in self.atoms
                     for atom2 in self.atoms}

        self.hvecs = {key: future.result()
                      for key, future in futures.items()}

        if len(self.magnitudes) == 0:
            from itertools import chain
            self.magnitudes = vector_magnitudes(
                VectorSet(chain(*self.hvecs.values())), self.relative_subs)

    def neighbor_hopping_vectors(self, neighnum: int, **kwargs) -> list:
        # Compatibility
        if neighnum is None:
            neighnum = 0

        subs = self.relative_subs
        magnitudes = self.magnitudes[neighnum]

        from concurrent.futures import ProcessPoolExecutor
        with ProcessPoolExecutor() as e:
            from itertools import repeat
            futures = {key: e.submit(vector_magnitude_filter,
                                     vector_collection, magnitudes, subs,
                                     **kwargs)
                       for key, vector_collection in self.hvecs.items()}

        return {key: future.result() for key, future in futures.items()}

    def grow(self, sizes: list, **kwargs) -> 'UnitCell':
        assert len(sizes) == len(self.translation_vectors)

        tvecspan = translation_vector_span(self.translation_vectors, sizes=sizes)
        atoms = VectorSet( (atom + vec).with_label(atom.label)
                           for atom in self.atoms for vec in tvecspan )
        translation_vectors = VectorSet(
            (1 + max(sizes[i]) - min(sizes[i])) * vec
            for i, vec in enumerate(self.translation_vectors) )

        init_kwargs = dict( atoms = atoms,
                            translation_vectors = translation_vectors,
                            equilibrium_subs = self.equilibrium_subs,
                            relative_subs = self.relative_subs,
                            vector_span_sizes = self.vector_span_sizes,
                            magnitudes = self.magnitudes )

        cls = self.__class__
        return cls(**{**init_kwargs, **kwargs})

    def cut(self, cut_condition, subs={}, **kwargs) -> 'UnitCell':
        subs = {**self.relative_subs, **subs}
        atoms = VectorSet( atom for atom in self.atoms
                           if not cut_condition(atom.subs(subs)) )

        init_kwargs = dict( atoms = atoms,
                            translation_vectors = self.translation_vectors,
                            equilibrium_subs = self.equilibrium_subs,
                            relative_subs = self.relative_subs,
                            vector_span_sizes = self.vector_span_sizes,
                            magnitudes = self.magnitudes )

        cls = self.__class__
        return cls(**{**init_kwargs, **kwargs})

    def rotate(self, angle: float, axis=(0, 0, 1), **kwargs) -> 'UnitCell':
        rot_mat = (angle * row_join(sym.eye(3).col(i).cross(sym.Matrix(axis))
                                    for i in range(3))).exp()

        atoms = VectorSet(
            Vector(atom.label, tuple(rot_mat * sym.Matrix(atom.coordinates)))
            for atom in self.atoms )

        translation_vectors = VectorSet(
            Vector(vec.label, tuple(rot_mat * sym.Matrix(vec.coordinates)))
            for vec in self.translation_vectors )

        init_kwargs = dict( atoms = atoms,
                            translation_vectors = translation_vectors,
                            equilibrium_subs = self.equilibrium_subs,
                            relative_subs = self.relative_subs,
                            vector_span_sizes = self.vector_span_sizes,
                            magnitudes = self.magnitudes )

        cls = self.__class__
        return cls(**{**init_kwargs, **kwargs})


class OnsiteSOCTable(dict):
    def __init__(self, labels: list, equivalences: dict = {}) -> None:
        #self.logger = logging.getLogger('slakos.analytical.OnsiteSOCTable')

        for label in labels:
            self[label] = self.generate(label, equivalences)

    def __call__(self, vector, orbitals, labels, index, subs):
        label1, label2 = labels
        assert label1 in self
        assert label2 in self

        # Compatibility
        if index is None:
            index = 0

        if (label1 != label2 or
            orbitals not in self[label1] or
            index != 0 or
            abs(vector) != 0):
            #self.logger.debug("element does not exist")
            return sym.zeros(2)

        return self[label1][orbitals]

    def generate(self, label: str, equivalences: dict) -> dict:
        label = equivalences.get(label, label)

        lambda_ = sym.symbols("lambda_{0}".format(label), real=True)
        E = ConservativeDict()

        for (i, j, k), (x, y, z) in [((1, 2, 3), ("x", "y", "z")),
                                     ((2, 3, 1), ("y", "z", "x")),
                                     ((3, 1, 2), ("z", "x", "y"))]:
            s = 's'
            p_x, p_y, p_z = 'p_{'+x+'}', 'p_{'+y+'}', 'p_{'+z+'}'
            d_xy, d_yz, d_zx = 'd_{'+x+y+'}', 'd_{'+y+z+'}', 'd_{'+z+x+'}'
            d_x2y2, d_z2 = 'd_{x^2-y^2}', 'd_{z^2}'

            from sympy.physics.matrices import msigma

            E[p_x, p_y] = -sym.I * msigma(k) * lambda_ / 2
            E[p_y, p_x] = sym.I * msigma(k) * lambda_ / 2

        return dict(E)

class Basis:
    def __init__(self, data: list):
        from itertools import chain
        self.blocks = data
        self.basis = [*chain(*[item[1] for item in data])]

    def __repr__(self):
        return "{" + ", ".join(self.basis) + "}"

    def _latex(self, printer):
        return r"\left\{" + ", ".join(self.basis) + r"\right\}"

    def __iter__(self):
        return iter(self.basis)

    @classmethod
    def from_unit_cell(cls, unit_cell: UnitCell, orbitals: dict) -> 'Basis':
        from itertools import chain, repeat
        return cls([(atom.idlabel, ["{}^{{{}}}".format(orbital, atom.idlabel)
                                  for orbital in orbitals[atom.label]])
                    for atom in unit_cell.atoms.sorted()])

    def numof(self, *names):
        try:
            from numpy import array
            from itertools import chain
            return array([*chain(*(
                (self.basis.index(name),) if spin is None else
                (self.basis.index(name) * 2,) if spin == 'up' else
                (self.basis.index(name) * 2 + 1,) if spin == 'down' else
                (self.basis.index(name) * 2,
                 self.basis.index(name) * 2 + 1) if spin == 'both' else
                None
                for name_spin in names
                for name, spin in ((name_spin,) if isinstance(name_spin, tuple)
                                   else ((name_spin, None),)) ))])
        except NameError:
            return None

    def spin(self, spin):
        from numpy import array
        from itertools import chain
        return array([*chain(*(
            (i * 2,) if spin == 'up' else
            (i * 2 + 1,) if spin == 'down' else
            (i * 2, i * 2 + 1) if spin == 'both' else
            None
            for i in range(len(self.basis)) ))])

    def with_prefix(self, prefix):
        """Return a copy of the basis prefix prepended to all elements."""
        return type(self)([(block[0], [''.join((prefix, item))
                                       for item in block[1]])
                           for block in self.blocks])

ANALYSIS_UNIQUE = 1
ANALYSIS_IDENTITY = 2
ANALYSIS_CONJUGATE = 4
ANALYSIS_TRANSPOSE = 8
ANALYSIS_ADJOINT = ANALYSIS_CONJUGATE | ANALYSIS_TRANSPOSE

def analyse_backward(row, col, matrix: sym.Matrix,
                     simplifier: Callable = identity,
                     variables: dict = {}, hermitian=False) -> dict:
    result = dict(flags = 0,
                  entry = (None, None),
                  variables = [])

    block = matrix[row, col]
    if isinstance(block, sym.Matrix):
        zero = sym.zeros(block.shape)
    else:
        zero = 0

    if hermitian and row > col:
        rows_and_cols = ((col, row),)
    else:
        rows_and_cols = ((row, col) for row in range(matrix.rows)
                                    for col in range(matrix.cols))

    for row_, col_ in rows_and_cols:
        if row_ == row and col == col_:
            result['flags'] = ANALYSIS_UNIQUE
            break

        block_ = matrix[row_, col_]

        if block_ == block or (block.shape == block_.shape and
                               simplifier(block - block_) == zero):
            result['flags'] = ANALYSIS_IDENTITY
            result['entry'] = (row_, col_)
            break

        elif (block_ == sym.conjugate(block) or
              (block.shape == block_.shape and
               simplifier(block_ - sym.conjugate(block)) == zero)):
            result['flags'] = ANALYSIS_CONJUGATE
            result['entry'] = (row_, col_)
            break

        elif (block_ == sym.transpose(block) or
              (block.shape == sym.transpose(block_).shape and
               simplifier(block_ - sym.transpose(block)) == zero)):
            result['flags'] = ANALYSIS_TRANSPOSE
            result['entry'] = (row_, col_)
            break

        elif (block_ == sym.adjoint(block) or
              (block.shape == sym.adjoint(block_).shape and
               simplifier(block_ - sym.adjoint(block)) == zero)):
            result['flags'] = ANALYSIS_ADJOINT
            result['entry'] = (row_, col_)
            break
    else:
        if hermitian and row > col:
            result['flags'] = ANALYSIS_ADJOINT
            result['entry'] = (col, row)
        else:
            result['flags'] = ANALYSIS_UNIQUE

    result['variables'] = [variable for variable in variables
                           if variable in block.atoms(sym.Symbol)]

    return result

def analyse_matrix(matrix: sym.Matrix, block_basis: list = None,
                   uname: str = None,
                   simplifier: Callable = identity,
                   variables: dict = {},
                   hermitian=False) -> sym.Matrix:
    symbols = {}
    unique = {}

    from concurrent.futures import ProcessPoolExecutor
    with ProcessPoolExecutor() as e:
        from numpy import array
        from itertools import product, repeat

        # prod = [*product(*map(range, matrix.shape))]

        # simils = array([*e.map(analyse_backward,
        #                        (a[0] for a in prod),
        #                        (a[1] for a in prod),
        #                        repeat(matrix))]).reshape(*matrix.shape)

        futures = { (row, col): e.submit(analyse_backward, row, col, matrix,
                                         simplifier=simplifier,
                                         variables=variables,
                                         hermitian=hermitian)
                    for row in range(matrix.rows)
                    for col in range(matrix.cols) }
        simils = {key: value.result() for key, value in futures.items()}

    for row, col in product(*map(range, matrix.shape)):
        if (row, col) in symbols:
            continue

        if not (simils[row, col]['flags'] ^ ANALYSIS_IDENTITY):
            symbols[row, col] = symbols[simils[row, col]['entry']]
            continue
        elif not (simils[row, col]['flags'] ^ ANALYSIS_CONJUGATE):
            symbols[row, col] = sym.conjugate(symbols[simils[row, col]['entry']])
            continue
        elif not (simils[row, col]['flags'] ^ ANALYSIS_TRANSPOSE):
            symbols[row, col] = sym.Transpose(symbols[simils[row, col]['entry']])
            continue
        elif not (simils[row, col]['flags'] ^ ANALYSIS_ADJOINT):
            symbols[row, col] = sym.Adjoint(symbols[simils[row, col]['entry']])
            continue

        # If none of the above, we are dealing with a unique element.
        block = simplifier(matrix[row, col])

        if isinstance(block, sym.MatrixBase):
            if not any(block):
                symbols[row, col] = sym.S(0)
                symbols[col, row] = sym.S(0)
                continue
            if uname is None:
                uname = 'h'
        elif isinstance(block, sym.Expr):
            if not bool(block):
                symbols[row, col] = sym.S(0)
                symbols[col, row] = sym.S(0)
                continue
            if uname is None:
                uname = 't'
        else:
            raise TypeError("{} is not a matrix or expression".format(block))

        if block_basis is not None:
            name = "{}_{}{}".format(uname, block_basis[row], block_basis[col])
        else:
            name = "{}_{}{}".format(uname, row + 1, col + 1)

        symbol = sym.Symbol(name)

        if len(simils[row, col]['variables']) > 0:
            symbols[row, col] = symbol(*simils[row, col]['variables'])
        else:
            symbols[row, col] = symbol

        unique.update({symbols[row, col]: block})

    return (sym.Matrix(*matrix.shape, lambda row, col: symbols[row, col]), unique)

class Slab:
    def __init__(self, unitcell: UnitCell, tvectors: sym.Matrix) -> None:
        self.__dict__.update(locals())
