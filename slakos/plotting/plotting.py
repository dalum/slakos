import logging

import numpy as np

from typing import Iterable, Mapping, Callable

import matplotlib.pyplot as plt
import matplotlib.widgets as wdg

# def set_aspect_equal_3d(ax):
#     """Fix equal aspect bug for 3D plots."""

#     xlim = ax.get_xlim3d()
#     ylim = ax.get_ylim3d()
#     zlim = ax.get_zlim3d()

#     from numpy import mean
#     xmean = mean(xlim)
#     ymean = mean(ylim)
#     zmean = mean(zlim)

#     plot_radius = max([abs(lim - mean_)
#                        for lims, mean_ in ((xlim, xmean),
#                                            (ylim, ymean),
#                                            (zlim, zmean))
#                        for lim in lims])

#     ax.set_xlim3d([xmean - plot_radius, xmean + plot_radius])
#     ax.set_ylim3d([ymean - plot_radius, ymean + plot_radius])
#     ax.set_zlim3d([zmean - plot_radius, zmean + plot_radius])

# def plot_unit_cell(unit_cell: 'UnitCell', parameters: 'Parameters',
#                    sizes={}, colors={},
#                    xaxis=np.array([1, 0, 0]),
#                    yaxis=np.array([0, 1, 0]),
#                    ax=None, fig=None,
#                    default_size=10, default_color='k') -> tuple:
#     if ax is None:
#         fig = plt.figure()
#         from mpl_toolkits.mplot3d import Axes3D
#         ax = fig.add_subplot(111, aspect='equal', projection='3d')
#         #ax.set_axis_off()
#         ax.set_xlabel('a')
#         ax.set_ylabel('b')
#         ax.set_zlabel('c')

#     atoms = force_subs(unit_cell.atoms.evalf(), parameters.evalb().value_dict())

#     coordinates = []
#     sizes_ = []
#     colors_ = []

#     for atom in sorted(atoms, key=lambda a: a.coordinates[2]):
#         coordinates.append(atom.coordinates)
#         sizes_.append(sizes.get(atom.label, default_size) ** 2)
#         colors_.append(colors.get(atom.label, default_color))
#     coordinates = np.array(coordinates).astype(float)

#     ax.scatter(coordinates[:, 0], coordinates[:, 1], coordinates[:, 2],
#                s=sizes_, c=colors_)
#     set_aspect_equal_3d(ax)

#     return ax

def plot_lines(xspace, datasets, *, fmt=('k-',), ax=None, fig=None):
    if ax is None:
        fig = plt.figure()
        ax = fig.add_subplot(111)

    from itertools import cycle
    fmt = cycle(fmt)
    return (ax,
            [ax.plot(xspace, dataset, next(fmt))[0]
             for dataset in datasets.swapaxes(0, 1)],
            fig)

def plot_scatter(xspace, datasets, sizes, *, col='k', ax=None, fig=None):
    if ax is None:
        fig = plt.figure()
        ax = fig.add_subplot(111)

    from itertools import cycle
    return (ax,
            [ax.scatter(xspace, dataset, size, alpha=1, facecolors='none', edgecolors=col)
             for dataset, size in zip(datasets.swapaxes(0, 1), sizes)],
            fig)

from typing import Iterable, Callable
class BandStructure:
    def __init__(self, hamiltonian: 'Hamiltonian', path: 'Path') -> None:
        """Make an object for plotting band structures."""
        self.__dict__.update(locals())
        self.logger = logging.getLogger("slakos.plotting.BandStructure")

    def _eval_path(self, steps):
        self.points = self.path(steps)

    def _eval_eig(self):
        self.eig = self.hamiltonian.find('eig', self.points)
        self.eigvals = np.array(self.points.mapm(
            {coord: val[0] for coord, val in self.eig.items()}))
        self.eigvecs = np.array(self.points.mapm(
            {coord: val[1] for coord, val in self.eig.items()}))
        self.direct_gap = self.hamiltonian.direct_gap
        self.indirect_gap = self.hamiltonian.indirect_gap

    def _eval(self, steps):
        self._eval_path(steps)
        self._eval_eig()

    def _proj_amp(self, projection, scale=50):
        projection = projection# / np.sqrt(sum(projection))
        absqr = np.array([[
            abs(sum(complex(projection[k]) * self.eigvecs[i, k, j]
                    for k in range(len(projection)))) ** 2
            for j in range(self.eigvecs.shape[2])]
                          for i in range(self.eigvecs.shape[0])])
        #absqr = np.abs(np.tensordot(projection.conj(), self.eigvecs[:, :, :], axes=(0, 1))) ** 2
        absqr = scale * absqr.swapaxes(0, 1)
        return absqr

    def plot(self, add_datasets: Iterable = (), add_fmts: Iterable = (), *,
             steps: int = 100,
             fmt: Iterable = ('k-',), xlim=(0, 1), ylim=(-5, 5),
             ylabel="eV", vlinecol='k', hlinecol='green', show_gaps=True,
             projections: Iterable = None, projection_scale: int = 50,
             adjustable: list = [],
             ax=None, fig=None, sliders=None,
             **kwargs) -> tuple:
        """Plot the band structure.

        If additional datasets are provided, these are plotted below the
        original, using the iterable of iterables, add_fmts, for styling the
        plot.

        """
        # We put our original datasets at the end so they will be plotted on
        # top. This also ensures that the last lines list that we will pass to
        # the interactive plot function are the right ones.
        self._eval(steps)

        for datasets, fmt in zip([*add_datasets, (self.eigvals, self.eigvecs)],
                                 [*add_fmts, fmt]):
            eigvals, eigvecs = (datasets if isinstance(datasets, tuple) else
                                (datasets, None))
            if len(eigvals.shape) == 2:
                path_space = self.path.space
            elif len(eigvals.shape) == 3:
                path_space = eigvals[:, 0, 0]
                eigvals = eigvals[:, :, 1]
            else:
                raise ValueError("shape of dataset ({}) not supported"
                                 "".format(eigvals.shape))

            if projections is None or eigvecs is None:
                ax, lines, fig = plot_lines(path_space, eigvals,
                                            fmt=fmt, ax=ax, fig=fig)
            else:
                for projection, col in projections:
                    ax, lines, fig = plot_scatter(
                        path_space, eigvals,
                        self._proj_amp(projection, projection_scale),
                        col=col, ax=ax, fig=fig)

        ax.set_xlim(0, 1)
        ax.set_ylim(*ylim)
        ax.set_ylabel(ylabel)

        if show_gaps:
            from itertools import chain
            hlines = [ax.axhline(val, color=hlinecol, linestyle='dashed')
                      for val in chain(self.direct_gap, self.indirect_gap)
                      if val is not None]
        else:
            hlines = []

        vlines = [ax.axvline(point, color=vlinecol)
                  for point in self.path.norm_points]
        ax.set_xticks(self.path.norm_points)
        ax.set_xticklabels(self.path.labels)

        sliders = self._setup_sliders(ax, sliders, adjustable, **kwargs)

        def update_parameters(val):
            nonlocal sliders, projections, steps

            for symbol, slider in sliders.items():
                self.hamiltonian.parameters[symbol].set_value(slider.val)

            self._eval_path(steps)
            self._eval_eig()

        def update_plot(val):
            nonlocal ax, sliders, projections, vlines, hlines

            ax.set_xticks(self.path.norm_points)
            for vline, point in zip(vlines, self.path.norm_points):
                vline.set_xdata([point] * 2)
                ax.draw_artist(vline)

            for line, dataset in zip(lines, self.eigvals.swapaxes(0, 1)):
                if hasattr(line, 'set_ydata'):
                    line.set_ydata(dataset)
                elif hasattr(line, 'set_offsets'):
                    offsets = line.get_offsets()
                    offsets[:, 1] = dataset
                    line.set_offsets(offsets)
                ax.draw_artist(line)

            for hline, val in zip(hlines, chain(self.hamiltonian.direct_gap,
                                                self.hamiltonian.indirect_gap)):
                hline.set_ydata([val] * 2)
                ax.draw_artist(hline)

        for slider in sliders.values():
            slider.on_changed(update_parameters)
            slider.on_changed(update_plot)

        def sync_fn(eigvals, bands, draw_line=None):
            nonlocal sliders, projections, steps

            self._eval_path(steps)
            self.eigvals = np.array(eigvals)
            eigvals = self.eigvals.swapaxes(0, 1)
            # #self.eigvals = eigvals
            # for symbol, slider in sliders.items():
            #     slider.set_val(
            #         self.hamiltonian.parameters.evalb().named_value_dict()[symbol])
            #     slider.ax.figure.canvas.blit(ax.bbox)

            for i, line in enumerate(lines):
                if i in bands:
                    line.set_color('blue')
            if draw_line:
                ax.axhline(draw_line, color='blue', linestyle='solid')
            update_plot(None)
            ax.figure.canvas.blit(ax.bbox)
            plt.pause(0.01)
            #plt.pause(0.5)

        return ax, lines, sliders, vlines, sync_fn

    def projection(self, projections: Iterable, bands: Iterable, *,
                   steps: int = 100,
                   ylabel="probability amplitude", vlinecol='k',
                   adjustable: Iterable = [],
                   ax=None, fig=None, sliders=None,
                   **kwargs) -> tuple:

        self._eval(steps)

        for projection, fmt in projections:
            ax, lines, fig = plot_lines(
                self.path.space,
                self._proj_amp(projection, 1)[bands, :].swapaxes(0, 1),
                fmt=fmt, ax=ax, fig=fig)

        ax.set_xlim(0, 1)
        ax.set_ylim(0, 1)
        ax.set_ylabel(ylabel)

        vlines = [ax.axvline(point, color=vlinecol)
                  for point in self.path.norm_points]
        ax.set_xticks(self.path.norm_points)
        ax.set_xticklabels(self.path.labels)

        sliders = self._setup_sliders(ax, sliders, adjustable, **kwargs)

        def update_parameters(val):
            nonlocal sliders, projections, steps

            for symbol, slider in sliders.items():
                self.hamiltonian.parameters[symbol] = slider.val

            self._eval_path(steps)
            self._eval_eig()

        def update_plot(val):
            nonlocal ax, sliders, projections, vlines

            ax.set_xticks(self.path.norm_points)
            for vline, point in zip(vlines, self.path.norm_points):
                vline.set_xdata([point] * 2)

            for projection, _ in projections:
                for line, amps in zip(
                        lines, self._proj_amp(projection, 1)[bands, :]):
                    line.set_ydata(amps)

        for slider in sliders.values():
            slider.on_changed(update_parameters)
            slider.on_changed(update_plot)

        return fig, ax, lines, vlines

    def _get_slider_args(self, symbol, view, **kwargs):
        parameter = self.hamiltonian.parameters.evalb()[symbol]
        value = parameter.value
        lower, upper = parameter.bounds

        if lower is None and upper is None:
            lower = value - view / 2
            upper = value + view / 2
        elif lower is None:
            lower = min(upper - view, value - view / 2)
            upper = min(upper, value + view / 2)
        elif upper is None:
            lower = max(lower, value - view / 2)
            upper = max(lower + view, value + view / 2)

        return {'label': str(symbol), 'valmin': lower, 'valmax': upper,
                'valinit': value}

    def _setup_sliders(self, ax, sliders, adjustable, view=2, **kwargs):
        # Add adjustable sliders for interacting with the plot
        if sliders is None:
            sliders = {}

        # Populate sliders
        for i, symbol in enumerate(adjustable):
            if symbol not in sliders:
                sliders[symbol] = wdg.Slider(
                    ax.figure.add_axes([0.15 + (0.45 * (i % 2)),
                              0.01 + (0.025 * (i // 2)),
                              0.30,
                              0.02]),
                    **self._get_slider_args(symbol, view, **kwargs))
        return sliders

    def _setup_live_fit_button(self, ax, fit_function):
        fit_button = wdg.Button(ax.figure.add_axes([0.85, 0.85, 0.1, 0.025]),
                                'Start fit')

        def start_fit(event):
            fit_button.color = 'blue'
            fit_function()
            fit_button.color = 'green'
            ax.figure.canvas.draw()

        fit_button.on_clicked(start_fit)

        return fit_button
