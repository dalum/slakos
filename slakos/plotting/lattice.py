import numpy as np
import sympy as sym

import matplotlib.pyplot as plt
import matplotlib.widgets as wdg

from slakos import fuzzy_subs
from slakos.modelling.hopping import translation_vector_span
from slakos.representations.lattices import Lattice

def plot_lattice_from_axis(lattice: Lattice,
                           subs: dict,
                           sizes: dict = None,
                           colors: dict = None,
                           *,
                           xaxis=[1, 0, 0],
                           yaxis=[0, 1, 0],
                           xlabel=None, ylabel=None,
                           ax=None, fig=None,
                           default_size: int = 10,
                           default_color: str = 'k',
                           unit_cell_style: str = 'r--',
                           unit_cell_bounding_vectors: set = None,
                           hopping_nums: list = None,
                           hopping_styles: list = None) -> tuple:

    hopping_nums = hopping_nums or []

    sizes = sizes or {}
    colors = colors or {}
    unit_cell_bounding_vectors = unit_cell_bounding_vectors or set()

    from itertools import cycle
    hopping_styles = cycle(hopping_styles or [('k-', 2), ('b-', 1), ('g--', 1)])

    if ax is None:
        fig = plt.figure()
        ax = fig.add_subplot(111, aspect='equal')
        #ax.set_axis_off()
        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)

    xaxis = np.array(xaxis) / np.sqrt(sum(x ** 2 for x in xaxis))
    yaxis = np.array(yaxis) / np.sqrt(sum(x ** 2 for x in yaxis))
    zaxis = np.cross(xaxis, yaxis)

    coordinates = []
    sizes_ = []
    colors_ = []

    def float_sub(expr):
        return float(fuzzy_subs(sym.S(expr), subs))
    def subarr(expr):
        return np.array(fuzzy_subs(expr, subs)).astype(float)

    elems = sorted(lattice.unit_cell, key=lambda a: subarr(a.point) @ zaxis)

    for neighnum in hopping_nums:
        hvecs = lattice.neighbor_hopping_vectors(neighnum)
        style, lw = next(hopping_styles)

        for elem in elems:
            for elem2 in elems:
                for hvec in hvecs[elem, elem2]:
                    xs = [subarr(elem.point) @ xaxis,
                          subarr(elem.point + hvec) @ xaxis]
                    ys = [subarr(elem.point) @ yaxis,
                          subarr(elem.point + hvec) @ yaxis]
                    ax.plot(xs, ys, style, lw=lw)

    for elem in elems:
        for tvec in translation_vector_span(lattice.translation_vectors,
                                            lattice.vector_span_sizes):
            point = subarr(elem.point + tvec)

            if (point == subarr(elem.point)).all():
                alpha_ = 1
            else:
                alpha_ = 0.5# * (1 / (1 + np.exp(-point @ zaxis)))
            ax.scatter([point @ xaxis], [point @ yaxis],
                       sizes.get(elem.symbol, default_size) ** 2,
                       c=colors.get(elem.symbol, default_color),
                       alpha=alpha_, linewidth=int(alpha_))

    tvecs = list(map(subarr, (lattice.translation_vectors |
                              unit_cell_bounding_vectors)))
    from itertools import permutations
    for combs in permutations(tvecs):
        start = np.array([0, 0])
        for tvec in combs:
            stop = start + np.array([tvec @ xaxis, tvec @ yaxis])
            ax.plot([start[0], stop[0]], [start[1], stop[1]], unit_cell_style)
            start = stop

    return ax

def set_aspect_equal_3d(ax):
    """Fix equal aspect bug for 3D plots."""

    xlim = ax.get_xlim3d()
    ylim = ax.get_ylim3d()
    zlim = ax.get_zlim3d()

    from numpy import mean
    xmean = mean(xlim)
    ymean = mean(ylim)
    zmean = mean(zlim)

    plot_radius = max([abs(lim - mean_)
                       for lims, mean_ in ((xlim, xmean),
                                           (ylim, ymean),
                                           (zlim, zmean))
                       for lim in lims])

    ax.set_xlim3d([xmean - plot_radius, xmean + plot_radius])
    ax.set_ylim3d([ymean - plot_radius, ymean + plot_radius])
    ax.set_zlim3d([zmean - plot_radius, zmean + plot_radius])

def plot_lattice_3d(lattice: Lattice, subs: dict,
                    sizes={}, colors={},
                    xaxis=np.array([1, 0, 0]),
                    yaxis=np.array([0, 1, 0]),
                    ax=None, fig=None,
                    default_size=10, default_color='k') -> tuple:
    if ax is None:
        fig = plt.figure()
        from mpl_toolkits.mplot3d import Axes3D
        ax = fig.add_subplot(111, aspect='equal', projection='3d')
        #ax.set_axis_off()
        ax.set_xlabel('a')
        ax.set_ylabel('b')
        ax.set_zlabel('c')

    coordinates = []
    sizes_ = []
    colors_ = []

    for elem in sorted(lattice.unit_cell, key=lambda a: a.point[2]):
        coordinates.append(fuzzy_subs(elem.point, subs))
        sizes_.append(sizes.get(elem.symbol, default_size) ** 2)
        colors_.append(colors.get(elem.symbol, default_color))
    coordinates = np.array(coordinates).astype(float)

    ax.scatter(coordinates[:, 0], coordinates[:, 1], coordinates[:, 2],
               s=sizes_, c=colors_)
    set_aspect_equal_3d(ax)

    return ax
