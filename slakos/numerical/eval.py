import sympy as sym

from .parameters import Parameters

def evalb(expr, parameters: Parameters):
    return float(sym.sympify(expr, locals=parameters.evalf().named_value_dict()))
