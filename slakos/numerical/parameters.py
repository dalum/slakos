import numpy as np
import sympy as sym

import logging

from typing import Iterable, Mapping, Callable

from collections import UserDict

from slakos.core import (unify, unifys, unify_map, unifys_map, qualified_name,
                         fuzzy_subs, latex)

class Parameter:
    def __init__(self, symbol="unnamed parameter",
                 symbol_value: sym.Symbol = sym.S(0),
                 symbol_bounds: tuple = (None, None),
                 unit: str = "", *,
                 tension=1000, namespace: dict = {}) -> None:
        self.__dict__.update(locals())

    def copy(self):
        cls = self.__class__
        return cls(symbol=self.symbol, symbol_value=self.symbol_value,
                   symbol_bounds=self.symbol_bounds, unit=self.unit,
                   tension=self.tension, namespace=self.namespace)

    def evalf(self, *args, **kwargs):
        def evalf_(x):
            if hasattr(x, 'evalf'):
                xevalf = x.evalf
            elif x is None:
                xevalf = lambda *args, **kwargs: None
            else:
                from numpy import around
                xevalf = lambda *args, **kwargs: around(x, *args, **kwargs)
            return xevalf(*args, **kwargs)

        cls = self.__class__
        return cls(self.symbol, evalf_(self.value),
                   (*map(evalf_, self.bounds),),
                   self.unit, tension=self.tension,
                   namespace=self.namespace)

    def evalb(self):
        def evalb_(x):
            try:
                if x is None:
                    return None
                elif not getattr(x, 'is_real', True):
                    return complex(x)
                else:
                    return float(x)
            except TypeError as e:
                raise TypeError("cannot convert {} of type {} to base type"
                                "".format(x, type(x))) from e

        cls = self.__class__
        return cls(symbol=self.symbol, symbol_value=evalb_(self.value),
                   symbol_bounds=(*map(evalb_, self.bounds),),
                   unit=self.unit, tension=self.tension,
                   namespace=self.namespace)

    def __getstate__(self) -> dict:
        return dict(symbol=sym.srepr(self.symbol),
                    symbol_value=sym.srepr(self.symbol_value),
                    symbol_bounds=(*map(sym.srepr, self.symbol_bounds),),
                    unit=self.unit, tension=self.tension)

    def __setstate__(self, state) -> None:
        self.__init__()

        self.symbol = sym.sympify(state['symbol'])
        self.symbol_value = sym.sympify(state['symbol_value'])
        self.symbol_bounds = (*map(sym.sympify, state['symbol_bounds']),)
        self.unit = state['unit']
        self.tension = state['tension']

    def _transform(self, x, max_recursion=10):
        if hasattr(x, 'subs') and hasattr(x, 'is_constant'):
            for _ in range(max_recursion):
                if x.is_constant():
                    break
                x = fuzzy_subs(x, {key: value.symbol_value
                                   for key, value in self.namespace.items()})
        return x

    def __repr__(self):
        return repr(self.value)

    def _unify(self, **kwargs):
        self.symbol = unify(self.symbol, **kwargs)
        self.symbol_value = unify(self.symbol_value, **kwargs)
        self.symbol_bounds = (*unify_map(self.symbol_bounds, **kwargs),)
        return self

    def _latex(self, printer):
        out = sym.latex(str(self.value))
        if self.unit:
           out += '\\text{{ {}}}'.format(self.unit)
        return out

    @property
    def bounds(self):
        return (*map(self._transform, self.symbol_bounds),)
    def set_bounds(self, *bounds):
        self.symbol_bounds = tuple(map(sym.S, bounds))

    @property
    def value(self):
        return self._transform(self.symbol_value)
    def set_value(self, value):
        self.symbol_value = value

    @property
    def stress(self):
        lower, upper = self.bounds
        if lower is not None and self.value < lower:
            return self.tension * ((self.value - lower) / lower) ** 2
        elif upper is not None and self.value > upper:
            return self.tension * ((self.value - upper) / upper) ** 2
        else:
            return 0

    def set_tension(self, tension):
        self.tension = tension

class ParameterTuple(tuple):
    def __repr__(self):
        names_values = ', '.join("{} = {}".format(param.symbol,
                                                  param.value)
                                 for param in self)
        return '(' + names_values + ')'

    def _latex(self, printer) -> str:
        format_string = "{} = {}"
        return ', '.join([format_string.format(latex(param.symbol),
                                               latex(param))
                           for param in self])

    def _latex_inline(self, printer) -> str:
        format_string = "${} = {}$"
        return ', '.join([format_string.format(latex(param.symbol),
                                               latex(param))
                           for param in self])

    def _latex_table(self, printer) -> str:
        columns = printer._settings['columns']

        from itertools import cycle, chain
        connectors = cycle(chain([' & '] * (columns - 1), [' \\\\ ']))

        format_string = "${}$ & ${}$"

        column = 0
        param = self[0]
        formatted_table = format_string.format(latex(sym.S(param.symbol)),
                                               latex(param))
        for param in self[1:]:
            column = (column + 1) % columns
            formatted_table += next(connectors)
            formatted_table += format_string.format(latex(sym.S(param.symbol)),
                                                    latex(param))
        for _ in range(columns - column - 1):
            formatted_table += next(connectors) * 2

        return formatted_table

    def set_bounds(self, *bounds):
        for parameter in self:
            parameter.set_bounds(*bounds)

    def set_value(self, value):
        for parameter in self:
            parameter.set_value(value)

    def set_unit(self, unit):
        for parameter in self:
            parameter.unit = unit

    def __getattr__(self, name):
        return tuple(getattr(parameter, name) for parameter in self)

class Parameters(UserDict):
    def __init__(self, *args, **kwargs) -> None:
        """Make a parameter list.

        Parameters
        ----------
        values : mapping(symbol : Parameter or float or symbol or str)
            Numerical values associated with the symbol.
        special_syms : sequence
            Parameters which will be excluded from certain procedures, such as
            fitting. They are considered either constant or variables such as a
            k coordinates.
        first_class : sequence
            Parameters which should be pushed to the beginning of the list.
        discard : sequence
            Parameters which should be ignored completely.
        expr : sympy expression  (keyword only)
            Expression from which parameters are extracted. All atoms A for
            which `bool(A.is_Symbol) == True` will be included in sorted order.
        default_value : any  (keyword only)
            Default value to assign to parameters if not specified in `values`.

        Properties
        ----------
        all_symbols : list
            List of all symbols including those in special_syms.
        free_parameters : list
            Sorted list of all symbols except those listed in special_syms.

        """
        super().__init__()

        self.logger = logging.getLogger("slakos.numerical.Parameters")

        self._expr_syms = set()
        self._special_syms = []
        self._first_class = []
        self._discard = set()

        self.update(*args, **kwargs)

    def update(self, values: dict = {}, special_syms: Iterable = (),
               first_class: Iterable = (), discard: Iterable = (), *,
               expr: sym.Expr = None, default_value=0) -> None:
        if expr is not None:
            self._expr_syms |= {*map(str, expr.atoms(sym.Symbol))}

        self._special_syms += [str(symbol) for symbol in special_syms
                               if str(symbol) not in self._special_syms]
        self._first_class += [str(symbol) for symbol in first_class
                              if str(symbol) not in self._first_class]

        self._discard |= {*map(str, discard)}

        if not isinstance(default_value, Parameter):
            default_value = Parameter(symbol=default_value)
        else:
            default_value.namespace = self

        for key in {*values.keys(), *self.free_parameters}:
            if key in values:
                self[key] = values[key]
            elif key in self:
                continue
            else:
                self[key] = default_value.copy()

            self[key].symbol = key
            self[key].namespace = self

    def update_values(self, new_parameters: dict) -> None:
        for key in new_parameters.keys():
            if key in self:
                self[key].set_value(new_parameters[key].value)
            else:
                self[key] = new_parameters[key]
            self[key].namespace = self

    @property
    def all_symbols(self):
        return [*self._special_syms] + self.free_parameters

    @property
    def all_qualified_names(self):
        return [*map(lambda expr: qualified_name(str(expr)), self.all_symbols)]

    @property
    def free_parameters(self):
        # Poor man's sorting.
        def _key(A):
            A = qualified_name(str(A).lower())
            import re
            m = re.search("\_\_([0-9]+)", A)
            return A if not m else ''.join(("zzz", m.group(), A))

        syms = self._expr_syms | {*self.keys()}
        syms -= ({*self._special_syms} | {*self._first_class} | {*self._discard})

        return [*self._first_class] + sorted(syms, key=_key)

    def find(self, pattern):
        import re
        return [param for param in self.free_parameters
                if re.match(pattern, str(param))]


    def value_dict(self):
        return {key: value.value for key, value in self.items()}

    def named_value_dict(self):
        return {qualified_name(str(key)): value.value
                for key, value in self.items()}

    def evalf(self, *args, **kwargs):
        cls = self.__class__
        return cls(**{'special_syms': self._special_syms,
                      'first_class': self._first_class,
                      'values': {key: param.evalf(*args, **kwargs)
                                 for key, param in self.items()}})

    def evalb(self):
        cls = self.__class__
        return cls(**{'special_syms': self._special_syms,
                      'first_class': self._first_class,
                      'values': {key: param.evalb()
                                 for key, param in self.items()}})

    def __getstate__(self) -> dict:
        from pickle import dumps

        return dict(
            _expr_syms = {*map(sym.srepr, self._expr_syms)},
            _special_syms = [*map(sym.srepr, self._special_syms)],
            _first_class = [*map(sym.srepr, self._first_class)],
            _discard = {*map(sym.srepr, self._discard)},
            data = {sym.srepr(key): dumps(value)
                    for key, value in self.items()})

    def __setstate__(self, state) -> None:
        from pickle import loads

        self.__init__()

        self.__dict__.update(dict(
            _expr_syms = {*map(sym.sympify, state['_expr_syms'])},
            _special_syms = [*map(sym.sympify, state['_special_syms'])],
            _first_class = [*map(sym.sympify, state['_first_class'])],
            _discard = {*map(sym.sympify, state['_discard'])},
            data = {sym.sympify(key): loads(value)
                    for key, value in state['data'].items()}))

        for key, parameter in self.items():
            parameter.namespace = self

    def hash(self):
        from hashlib import sha1
        from pickle import dumps
        return sha1(dumps(self)).hexdigest()

    def _unify(self, **kwargs):
        self.data = {unify(key, **kwargs): unify(value, **kwargs)
                     for key, value in self.items()}
        return self

    def _latex(self, printer) -> str:
        return ', '.join(["{} = {}".format(latex(key),
                                           latex(self[key]))
                          for key in self.free_parameters])

    def __repr__(self):
        return ''.join(('{', ', '.join("{}: {}".format(key, self[key])
                                       for key in self.free_parameters), '}'))

    def __getitem__(self, key) -> Parameter:
        """Return a parameter or tuple of parameters.

        Usage
        -----

        If `key` is a sympy symbol, return the Parameter associated with that
        symbol. If `key` is an iterable, return a ParameterTuple of parameters
        matching the symbols in `key`. If `key` is a string, it is interpreted
        as a regex pattern, and a ParameterTuple of symbols whose names match
        the given pattern are returned.

        """
        if isinstance(key, str) and key.startswith('/'):
            import re
            pattern = re.compile(key[1:])
            key = [item for item in self.free_parameters
                   if re.match(pattern, item)]
        if isinstance(key, (tuple, list)):
            return ParameterTuple(self[item] for item in key)
        else:
            key = str(key)

        return super().__getitem__(key)

    def __setitem__(self, key, value) -> None:
        if not isinstance(key, str):
            key = str(key)

        if not isinstance(value, Parameter):
            if key in self:
                self[key].set_value(sym.sympify(value))
                return
            else:
                value = Parameter(key, sym.sympify(value), namespace=self)

        super().__setitem__(key, value)

    def partial(self, fn: Callable) -> Callable:
        from inspect import signature
        fn_args = [*signature(fn).parameters.keys()]

        named_value_dict = self.evalb().named_value_dict()
        default_kwargs = {key: value for key, value in named_value_dict.items()
                          if key in fn_args}

        self.logger.debug("Creating partial function with parameters: "
                          "{}".format(default_kwargs, fn_args))

        missing = {*fn_args, *self.all_qualified_names} - {*default_kwargs}
        missing = [name for name in self.all_qualified_names
                   if name in missing]

        args_str = ', '.join(missing)

        kwargs_str = ',\n        '.join(
            ( '{} = {}'.format(name, default_kwargs[name])
              for name in self.all_qualified_names
              if name in default_kwargs ))

        definition = r'''def partial_function({args}):
        """Partially applied function.

        Parameters
        ----------
        {args}

        Compiled with arguments
        -----------------------
        {kwargs}

        """
        kwargs.update({{ key: value for key, value in locals().items()
                         if key in fn_args }})
        return fn(**kwargs)

        '''.format(args=args_str, kwargs=kwargs_str)

        partial_function_code = compile(
            definition, "<partial function source>", "exec")

        partial_globals = {}
        eval(partial_function_code, {'kwargs': default_kwargs, 'fn': fn,
                                     'fn_args': fn_args},
             partial_globals)

        partial_function = partial_globals['partial_function']
        partial_function.missing = missing
        partial_function.original = fn

        return partial_function
