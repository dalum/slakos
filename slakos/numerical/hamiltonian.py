# This file is part of Slakos.
# 
# Slakos is free software: you can redistribute it and/or modify it under the terms
# of the GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
# 
# Slakos is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE.  See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with
# Slakos.  If not, see <http://www.gnu.org/licenses/>.

import logging

import numpy as np
import sympy as sym

from typing import Callable, Iterable

from slakos.core.unify import fuzzy_subs, qualified_name
from slakos.core import slambdify

from slakos.simplify import identity, powfact

from slakos.collections import Sequence, PointCollection, Mesh, Path

from .parameters import Parameters

import cloudpickle as pickle

def guarded(exception_class: Exception, fn: Callable, args: Iterable = (),
            default=None):
    try:
        return fn(*args)
    except exception_class:
        return default

def run_pickle_encoded(pickled_func: bytes, *args):
    func = pickle.loads(pickled_func)
    return func(*args)

def pickle_map(e: 'PoolExecutor', func: Callable, *args) -> 'PoolExecutorMap':
    from itertools import repeat
    return e.map(run_pickle_encoded, repeat(pickle.dumps(func)), *args)


def spin_expectation_value(qstate, index):
    from sympy.physics import msigma
    n = len(qstate) / 2
    s = np.array(msigma(index)).astype(complex)
    S = np.kron(np.eye(n), s)
    return qstate.conj() @ S @ qstate

class Hamiltonian:
    def __init__(self, expr: sym.Expr, parameters: Parameters,
                 subs: dict = {}, derivs: dict = None) -> None:
        """Make a Hamiltonian.

        Parameters
        ----------
        expr : Expr
            A SymPy expression representing the Hamiltonian.
        parameters : Parameters
            Exhaustive list of parameters for the expression. Symbols that are
            in `expr` but not in `parameters` will be substituted by zeros. To
            override this default behaviour, an optional `subs` parameter can
            be passed.
        subs : dict
            Substitution dictionary used on the expression. See parameters
            above.
        derivs : dict
            Derivatives of the Hamiltonian in the direction of the keys of
            `derivs`

        Attributes
        ----------
        expr : Expr
            Same as input parameter with substitutions as described.
        parameters : Parameters
            Same as input.
        subs : dict
            Substitutions used on `expr`. The keys of `subs` represent all
            symbols not in parameters, and the values their substitutions in
            `expr`.

        """
        self.logger = logging.getLogger("slakos.numerical.Hamiltonian")

        self.parameters = parameters
        self.parameters_hash = self.parameters.hash()
        self.cache = {}

        unassigned = ({*map(lambda symbol: qualified_name(str(symbol)),
                           expr.atoms(sym.Symbol))} -
                      {*self.parameters.all_qualified_names})

        self.subs = {**{symbol: 0 for symbol in unassigned}, **subs}
        self.expr = fuzzy_subs(expr, self.subs)

        if derivs is not None:
            self.derivs = {key: expr.subs(self.subs)
                           for key, expr in derivs.items()}
        else:
            self.derivs = {}

        # We hardcode the usage of numpy for stability.
        self.expr_symbols = self.expr.atoms(sym.Symbol)
        self._fn = slambdify(self.expr_symbols, self.expr)

        self._deriv_fns = {key: slambdify(self.expr_symbols, expr)
                           for key, expr in self.derivs.items()}

        # Maybe not that useful, but let's keep the following for debugging.
        from sympy.utilities.lambdify import (lambdastr,
                                              _get_namespace as get_namespace)
        from sympy.printing.lambdarepr import NumPyPrinter
        self._code = lambdastr(self.parameters.all_symbols, self.expr,
                               printer=NumPyPrinter)
        self._code_namespace = get_namespace('numpy')

    @property
    def _gamma_eigvals(self):
        gamma_point = tuple(0 for _ in self.fn.missing)
        gamma_eigs = self.find('eig', (gamma_point,))
        return gamma_eigs[gamma_point][0]

    @property
    def vband(self):
        return max(i for i, eigval in enumerate(self._gamma_eigvals)
                   if eigval <= 0)

    @property
    def cband(self):
        return min(i for i, eigval in enumerate(self._gamma_eigvals)
                   if eigval >= 0)

    @property
    def fn(self):
        return self.parameters.partial(self._fn)

    def deriv(self, key):
        return self.parameters.partial(self._deriv_fns[key])

    @property
    def eig(self):
        fn = self.fn
        def eig_fn(*k) -> np.array:
            return np.linalg.eigh(fn(*k))
        return eig_fn

    @property
    def eigvals(self):
        fn = self.fn
        def eigvals_fn(*k) -> np.array:
            return np.linalg.eigvalsh(fn(*k))
        return eigvals_fn

    def _check_hash(self):
        self.logger.debug("Checking parameters hash ...")
        if self.parameters.hash() != self.parameters_hash:
            self.logger.info("Parameters hash has changed! "
                             "Updating and clearing cache ...")
            self.parameters_hash = self.parameters.hash()
            self.cache = {}
            self.logger.info("Done.")

    @staticmethod
    def round_point(point, precision):
        if precision is None:
            return point
        return (*map(lambda x: round(x, precision), point),)

    def find(self, name, points: Iterable, params: tuple = (),
             precision: int = None, use_cache: bool = True, **kwargs) -> list:
        """Find and return sorted values for `name` at `points`.

        Parameters
        ----------
        name : str
            Desired quantity to be computed. Must be a property of Hamiltonian.
        points : iterable
            Iterable returning points at which to compute the quantity.
        precision : int or None
            Convert `points` to decimal `precision` before computations and
            lookups. If None (default) full precision is used.
        use_cache : bool
            When true (default) the internal cache will be used to retrieve missing
            values.

        Notes
        -----
        This method uses a cache to improve speed. If memory is an issue, you may
        consider disabling the cache.

        """
        if use_cache:
            self._check_hash()
        self.logger.debug("Finding values for: {}".format(name))

        points = [(*point,) for point in points]
        rounded_points = {self.round_point(point, precision): point
                          for point in points}

        if use_cache:
            if name not in self.cache:
                self.cache[name] = {}

            chart = {self.round_point(point, precision): value
                     for (point, params), value in self.cache[name].items()}
            uncharted_points = [rounded_points[point]
                                for point in ({*rounded_points} - {*chart})]
        else:
            chart = {}
            uncharted_points = points

        data = self._chart_points(name, getattr(self, name), uncharted_points,
                                  params=params, use_cache=use_cache)
        chart.update({self.round_point(point, precision): value
                      for (point, params), value in data.items()})

        return {point: chart[self.round_point(point, precision)] for point in points}

    def _chart_points(self, name: str, fn: Callable,
                      points: Iterable[tuple], params: tuple = (),
                      use_cache=True) -> dict:
        if len(points) > 0:
            self.logger.info("Charting {} new points ...".format(len(points)))
            from concurrent.futures import ProcessPoolExecutor
            with ProcessPoolExecutor() as e:
                k_points = np.array([[*map(float, k)] for k in points])
                k_points = k_points.swapaxes(0, 1)

                data = dict(zip(((point, params) for point in points),
                                [*pickle_map(e, fn, *k_points, *params)]))

            if use_cache:
                self.logger.debug("Updating cache ...")
                self.cache[name].update(data)
                self.logger.info("Done.")
        else:
            data = dict()

        return data

    @staticmethod
    def _berry_curvature_point(n: int, ms: Iterable, eigvecs:  np.array,
                               eigvals: np.array, d1: np.array, d2: np.array,
                               eps: float) -> float:
        return -np.imag(sum([((eigvecs[:, n].conj() @ (d1 @ eigvecs[:, m])) *
                              (eigvecs[:, m].conj() @ (d2 @ eigvecs[:, n])) -
                              (eigvecs[:, n].conj() @ (d2 @ eigvecs[:, m])) *
                              (eigvecs[:, m].conj() @ (d1 @ eigvecs[:, n]))) /
                             ((eigvals[n] - eigvals[m]) ** 2 - 1j * eps)
                             for m in ms]))

    @property
    def direct_gap(self):
        self._check_hash()

        eigvals_values = [*self.cache.get(
            'eigvals',
            {key: eigvals for key, (eigvals, eigvecs) in
             self.cache.get('eig', {}).items()}).values()]
        if len(eigvals_values) == 0:
            return (None, None)
        else:
            from itertools import chain
            gaps = {*chain(
                (guarded((ValueError, TypeError),
                         max, (val for val in eigvals if val <= 0)),
                 guarded((ValueError, TypeError),
                         min, (val for val in eigvals if val >= 0)))
                for eigvals in eigvals_values)}

            return sorted(gaps, key=lambda gap: guarded(
                TypeError, lambda gap: gap[1] - gap[0], (gap,), 0))[0]

    @property
    def indirect_gap(self):
        self._check_hash()

        eigvals_values = [*self.cache.get(
            'eigvals',
            {key: eigvals for key, (eigvals, eigvecs) in
             self.cache.get('eig', {}).items()}).values()]
        if len(eigvals_values) == 0:
            return (None, None)
        else:
            from itertools import chain
            eigvals = {*chain.from_iterable(eigvals_values)}
            return (guarded((ValueError, TypeError), max, (val for val in eigvals if val <= 0)),
                    guarded((ValueError, TypeError), min, (val for val in eigvals if val >= 0)))

    def low_energy_points(self, points: Iterable, direct=False) -> tuple:
        """Return the VBM and CBM points from a collection of points.

        Parameters
        ----------
        points : iterable
            Collection of points at which eigenvalues will be computed
            to determine the VBM and CBM.
        direct : bool
            If False (default), find maximum and minimum points which
            are individually closest to the Fermi energy, used for
            calculating the indirect gap.  If True, find the point with
            the smallest difference between the valence and conduction
            bands, used for calculating the direct gap.

        Returns
        -------
        VBM : tuple
            The point of the valence band maximum (VBM).
        CBM : tuple
            The point of the conduction band minimum (CBM).  If
            `direct=True`, this is the same as VBM.

        """
        eigvals = self.find('eigvals', points)
        vband = self.vband
        cband = self.cband
        lower = ((point, vals[vband]) for point, vals in eigvals.items())
        upper = ((point, vals[cband]) for point, vals in eigvals.items())
        if direct is False:
            vbm = max(lower, key=lambda i: i[1])[0]
            cbm = min(upper, key=lambda i: i[1])[0]
        else:
            diff = ((point, upper[point] - lower[point]) for point in points)
            vbm = min(diff, key=lambda i: i[1])[0]
            cbm = vbm

        return vbm, cbm

    def gradient_approach(self, point: tuple, band: int,
                          method: str = 'max', eps: float = 0.1,
                          maxfev: int = 500, threshold: float = 1e-4) -> tuple:
        assert method in ('min', 'max')

        ndim = len(point)
        def fn(point_):
            return abs(float(self.find('eigvals', [point_])[tuple(point_)][band]))

        from scipy.optimize import minimize
        opt_point = minimize(fn, [point])
        return tuple(opt_point.x)

    def berry_curvature(self, points: PointCollection,
                        n: int, ms: Iterable, x: str, y: str,
                        eps: float = 1e-8) -> float:

        futures = {}
        deriv_x = self.deriv(x)
        deriv_y = self.deriv(y)

        from concurrent.futures import ThreadPoolExecutor
        with ThreadPoolExecutor() as e:
            for point, eig in self.find('eig', points).items():
                eigvals, eigvecs = eig
                d1 = deriv_x(*point)
                d2 = deriv_y(*point)

                futures[point] = e.submit(self._berry_curvature_point,
                                          n, ms, eigvecs, eigvals, d1, d2, eps)

        curvature = {key: value.result() for key, value in futures.items()}

        return curvature

def effective_mass(hamiltonian, point, band, width: float = 0.1,
                   nsamples: int = 100) -> tuple:
    ndim = len(point)
    ms = []
    out = []
    res = []
    spc = []
    for i in range(ndim):
        start, stop = -width / 2, width / 2
        spaces = [np.linspace(point[j] + start * (i == j),
                              point[j] +  stop * (i == j),
                              nsamples)
                  for j in range(ndim)]
        points = Sequence(zip(*spaces))

        from scipy.optimize import leastsq

        def poly2(c, b, f, x):
            return (f * x ** 2 - 2 * b * x + c)

        eigvals = np.array(points.mapm(hamiltonian.find('eigvals', points)))
        diff = lambda a: (eigvals[:, band] - poly2(a[0], a[1], a[2], spaces[i]))
        c, b, f = leastsq(diff, [0, point[i], 0])[0]

        from scipy.constants import m_e, hbar, physical_constants
        electron_volt = physical_constants['electron volt'][0]

        m = (hbar ** 2) / (2 * f * m_e) * (1e20) / electron_volt
        ms.append(m)
        out.append(eigvals)
        res.append(poly2(c, b, f, spaces[i]))
        spc.append(spaces[i])

    return ms, out, res, spc

def effective_mass_tensor(hamiltonian: Hamiltonian, point: tuple,
                          band: int, width: float = 0.1,
                          nsamples: int = 10) -> tuple:
    ndim = len(point)
    ms = []
    out = []
    res = []
    spc = []

    start, stop = -width / 2, width / 2
    mesh = Mesh(*[np.linspace(point[i] + start, point[i] + stop, nsamples)
                 for i in range(ndim)])
    eigvals = hamiltonian.find('eigvals', mesh)

    from itertools import product, combinations
    from scipy.optimize import leastsq

    def fun(E_0, M_1, k):
        k = np.array(k) - np.array(point)
        return (E_0 + sum(M_1[min((i, j)), max((i, j))] * k[i] * k[j]
                          for i, j in product(range(ndim), repeat=2)))

    def diff(params):
        E_0, *M_ = params
        M_1 = np.array(M_).reshape(ndim, ndim)
        return [eigvals[tuple(k)][band] - fun(E_0, M_1, k) for k in mesh]

    E_0, *M_ = leastsq(diff, [0] + [0] * ndim ** 2)[0]

    from scipy.constants import m_e, hbar, physical_constants
    electron_volt = physical_constants['electron volt'][0]

    M_1 = np.array(M_).reshape(ndim, ndim)

    # Copy upper triangle to lower triangle
    for i, j in combinations(range(ndim), 2):
        M_1[j, i] = M_1[i, j]

    M = np.linalg.inv(2 * M_1 / hbar ** 2) / (m_e) / electron_volt * (1e20) # angstrom

    return E_0, M

class LeastSquareFitter:
    def __init__(self, hamiltonian: Hamiltonian, path: Path) -> None:
        self.__dict__.update(locals())
        self.logger = logging.getLogger('slakos.numerical.LeastSquareFitter')

    @staticmethod
    def default_measure(data, model_data, bands, **kwargs) -> list:
        measure = (data[:, bands] - model_data[:, bands]) ** 2
        return measure.flatten().tolist()

    @staticmethod
    def distance_measure(data, model_data, bands, decay_constant: float = 1.,
                         cutoff: float = 3, **kwargs) -> list:
        diffs = (data[:, bands] - model_data[:, bands]) ** 2
        measure = (diffs * np.exp(-abs(data[:, bands]) / decay_constant) *
                   (data[:, bands] <= cutoff))
        return measure.flatten().tolist()

    @staticmethod
    def smooth_measure(data, model_data, bands,
                       smoothness_weight: float = 100.,
                       cutoff: float = 3, **kwargs) -> list:
        data_derivs = data[1:, bands] - data[:-1, bands]
        model_data_derivs = model_data[1:, bands] - model_data[:-1, bands]
        measure = ((smoothness_weight * (data_derivs - model_data_derivs) ** 2) *
                   (data[1:, bands] <= cutoff))
        return measure.flatten().tolist()

    @staticmethod
    def smooth_distance_measure(data, model_data, bands, **kwargs):
        return (
            LeastSquareFitter.distance_measure(
                data, model_data, bands, **kwargs) +
            LeastSquareFitter.smooth_measure(
                data, model_data, bands, **kwargs))

    def fit_eigvals(self, data: np.array, fit_params: list, *,
                    bands: Iterable = None, weights: dict = None,
                    measure: str = None, maxiter: int = 100,
                    ext_sync: Iterable = (), sync_interval=None,
                    **kwargs) -> Callable:
        if bands is None:
            bands = slice(None)
        if measure is None:
            measure = self.default_measure
        elif isinstance(measure, str):
            measure = getattr(self.__class__, measure)
        if sync_interval is None:
            sync_interval = len(fit_params)

        ncalls = 0
        def fit_function(vals):
            nonlocal ncalls
            ncalls += 1
            self.logger.debug('{} calls made to fit function (maxiter = {}).'
                             ''.format(ncalls, maxiter))

            stress = []
            for symbol, value in zip(fit_params, vals):
                self.hamiltonian.parameters[symbol].set_value(value)
                stress.append(self.hamiltonian.parameters[symbol].stress)

            path_points = self.path(data.shape[0])
            eigvals = path_points.mapm(self.hamiltonian.find('eigvals', path_points,
                                                             use_cache=False))
            if ncalls % sync_interval == 0:
                self.logger.debug('Making external function sync calls.')
                for ext_fn in ext_sync:
                    ext_fn(eigvals, bands)

            model_data = np.array(eigvals)

            return sum(stress + measure(data, model_data, bands,
                                        weights=weights, **kwargs))

        from scipy.optimize import minimize

        def start_fit_fn(sequentially=False):
            self.logger.info("Starting fitting procedure ...")
            params = self.hamiltonian.parameters.evalb().value_dict()

            if sequentially:
                for symbol in fit_params:
                    minimize(fit_function, [params[symbol]], options={'maxiter': maxiter})
            else:
                minimize(fit_function, [params[symbol] for symbol in fit_params],
                         options={'maxiter': maxiter})
            self.logger.info("Done.")

        return start_fit_fn
