import sympy as sym

from .abstractvector import AbstractVectorSource

def spanning(*span, **kwargs):
    return sum(sym.Symbol('_'.join(v.name.split('.')), **kwargs) * v for v in span)

class ParameterSource(AbstractVectorSource):
    pass
