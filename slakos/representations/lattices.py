import sympy as sym

from typing import Callable

from collections import OrderedDict

from slakos import fuzzy_subs
from slakos.modelling.hopping import (translation_vector_span,
                                      atomic_hopping_vectors,
                                      vector_magnitudes,
                                      vector_magnitude_filter)

from .elements import ElementCollection

class Lattice:
    def __init__(self,
                 unit_cell: list,
                 symmetries: set,
                 equilibrium_subs: dict = None,
                 relative_subs: dict = None,
                 vector_span_sizes: list = None,
                 *,
                 initialize: bool = True,
                 magnitudes: list = [],
                 **kwargs) -> None:
        """Create a lattice.

        Parameters
        ----------
        unit_cell : list
            Collection of elements representing the unit cell.
        symmetries : set
            Set of transformations which leave the lattice invariant.
        equilibrium_subs : dict
            Substitutions for equilibrium conditions.
        relative_subs : dict
            Substitutions representing the relative sizes of quantities used
            when determining the magnitudes. See notes for more information.
        vector_span_sizes : iterable
            Multiplication factors determining the span of translation vectors
            to be taken into account, when finding neighbours.
        initialize : bool
            When true (default) hopping vectors will be determined at
            instantiation. Turning off initialization may be useful when
            creating large mock unit cells not intended for computations.
        magnitudes : ordered sequence
            If an empty sequence is passed (default), magnitudes will be
            computed from the calculated hopping vectors. If non-empty,
            magnitudes for neighbour numbers will be determined solely from
            this sequence.

        Notes
        -----
        The equilibrium and relative substitutions are used when determining
        neighbours.  As such, a lattice may be strained so as to change the
        actual ordering of the nearest and next--nearest neighbours, but this
        will not be reflected in the magnitudes or the filtered vectors
        returned by `neighbor_hopping_vectors`.  The relative substitutions are
        performed after the equilibrium substitutions, hence the relative
        substitutions should contain all equilibrium constants.

        """
        self.unit_cell = ElementCollection(unit_cell)
        self.symmetries = symmetries
        self.equilibrium_subs = equilibrium_subs or {}
        self.relative_subs = relative_subs or {}

        self.relative_subs = {
            key: (fuzzy_subs(value, relative_subs)
                  if hasattr(value, 'subs') else value)
            for key, value in self.equilibrium_subs.items()}

        self.vector_span_sizes = vector_span_sizes
        self.magnitudes = magnitudes

        if initialize:
            self.initialize(**kwargs)

    def initialize(self, magnitudes: list = [], **kwargs):
        from concurrent.futures import ProcessPoolExecutor
        with ProcessPoolExecutor() as e:
            tvecspan = translation_vector_span(self.translation_vectors,
                                               self.vector_span_sizes)

            futures = {(elem1, elem2): e.submit(atomic_hopping_vectors,
                                                elem1.point, elem2.point,
                                                tvecspan, **kwargs)
                       for elem1 in self.unit_cell
                       for elem2 in self.unit_cell}

        self.hvecs = {key: future.result()
                      for key, future in futures.items()}

        if len(self.magnitudes) == 0:
            from itertools import chain
            self.magnitudes = vector_magnitudes(
                set(chain(*self.hvecs.values())), self.relative_subs)

    def neighbor_hopping_vectors(self, neighnum: int, **kwargs) -> list:
        subs = self.relative_subs
        magnitudes = self.magnitudes[neighnum]

        from concurrent.futures import ProcessPoolExecutor
        with ProcessPoolExecutor() as e:
            from itertools import repeat
            futures = {key: e.submit(vector_magnitude_filter,
                                     vector_collection, magnitudes, subs,
                                     **kwargs)
                       for key, vector_collection in self.hvecs.items()}

        return {key: future.result() for key, future in futures.items()}

    # def grow(self, sizes: list, **kwargs) -> 'UnitCell':
    #     assert len(sizes) == len(self.translation_vectors)

    #     tvecspan = translation_vector_span(self.translation_vectors, sizes=sizes)
    #     atoms = VectorSet( (atom + vec).with_label(atom.label)
    #                        for atom in self.atoms for vec in tvecspan )
    #     translation_vectors = VectorSet(
    #         (1 + max(sizes[i]) - min(sizes[i])) * vec
    #         for i, vec in enumerate(self.translation_vectors) )

    #     init_kwargs = dict( atoms = atoms,
    #                         translation_vectors = translation_vectors,
    #                         equilibrium_subs = self.equilibrium_subs,
    #                         relative_subs = self.relative_subs,
    #                         vector_span_sizes = self.vector_span_sizes,
    #                         magnitudes = self.magnitudes )

    #     cls = self.__class__
    #     return cls(**{**init_kwargs, **kwargs})

    # def cut(self, cut_condition, subs={}, **kwargs) -> 'UnitCell':
    #     subs = {**self.relative_subs, **subs}
    #     atoms = VectorSet( atom for atom in self.atoms
    #                        if not cut_condition(atom.subs(subs)) )

    #     init_kwargs = dict( atoms = atoms,
    #                         translation_vectors = self.translation_vectors,
    #                         equilibrium_subs = self.equilibrium_subs,
    #                         relative_subs = self.relative_subs,
    #                         vector_span_sizes = self.vector_span_sizes,
    #                         magnitudes = self.magnitudes )

    #     cls = self.__class__
    #     return cls(**{**init_kwargs, **kwargs})

    # def rotate(self, angle: float, axis=(0, 0, 1), **kwargs) -> 'UnitCell':
    #     rot_mat = (angle * row_join(sym.eye(3).col(i).cross(sym.Matrix(axis))
    #                                 for i in range(3))).exp()

    #     atoms = VectorSet(
    #         Vector(atom.label, tuple(rot_mat * sym.Matrix(atom.coordinates)))
    #         for atom in self.atoms )

    #     translation_vectors = VectorSet(
    #         Vector(vec.label, tuple(rot_mat * sym.Matrix(vec.coordinates)))
    #         for vec in self.translation_vectors )

    #     init_kwargs = dict( atoms = atoms,
    #                         translation_vectors = translation_vectors,
    #                         equilibrium_subs = self.equilibrium_subs,
    #                         relative_subs = self.relative_subs,
    #                         vector_span_sizes = self.vector_span_sizes,
    #                         magnitudes = self.magnitudes )

    #     cls = self.__class__
    #     return cls(**{**init_kwargs, **kwargs})

from operator import add

def transform(elements: ElementCollection, point: sym.Point,
              operator: Callable = add) -> ElementCollection:
    new_elements = []

    for element in elements:
        cls = element.__class__
        new_element = cls(*map(lambda a: operator(a[0], a[1]),
                               zip(point, element.point)))
        new_elements.append(new_element)

    return new_elements
