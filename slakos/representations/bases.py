import numpy as np
import sympy as sym

from typing import Iterable
from collections import OrderedDict, namedtuple

from slakos import fuzzy_subs

from .elements import ElementCollection

class BasisElement:
    def __init__(self, elem: 'Element', num: int, orbital: 'Orbital',
                 parent_basis: 'Basis'):
        self.elem = elem
        self.num = num
        self.orbital = orbital
        self.parent_basis = parent_basis

    def __iter__(self):
        return iter((self.elem, self.num, self.orbital))

    def __eq__(self, other):
        return all((self.elem == other.elem,
                    self.num == other.num,
                    self.orbital == other.orbital))

    def as_vector(self, parent_basis=None, spin=None):
        """Return a normalized vector representing the basis element."""
        parent_basis = parent_basis or self.parent_basis
        vector = np.array([int(self == elem) for elem in parent_basis.basis])
        if spin is not None:
            vector = np.kron(vector, spin)
        return vector / np.sqrt((np.abs(vector) ** 2).sum())

class Basis:
    def __init__(self, blocks: OrderedDict,
                 parent_basis: 'Basis' = None) -> None:
        from itertools import chain
        self.parent_basis = parent_basis or self
        self._blocks = blocks
        self.basis = list(chain([BasisElement(elem, num, orbital, self)
                                 for (elem, num), orbitals in blocks.items()
                                 for orbital in orbitals]))
        if len(blocks) > 1:
            self.blocks = OrderedDict()
            for (elem, num), orbitals in blocks.items():
                self.blocks[elem, num] = Basis(OrderedDict([((elem, num),
                                                             orbitals)]))
        else:
            elem, num = list(blocks.keys())[0]
            self.blocks = OrderedDict([((elem, num), self)])

    def __repr__(self):
        return "{" + ", ".join("{}^{{{}{}}}".format(orbital, elem.symbol, num)
                               for elem, num, orbital in self.basis) + "}"

    def _latex(self, printer):
        return (r"\left\{" +
                ", ".join("{}^{{{}{}}}".format(orbital, elem.symbol, num)
                            for elem, num, orbital in self.basis) +
                r"\right\}")

    def __iter__(self):
        return iter(self.basis)

    def __len__(self):
        return len(self.basis)

    def index(self, elem=None, num=None, orbital=None):
        indices = []
        for index, (elem_, num_, orbital_) in enumerate(self.basis):
            if elem is not None and elem != elem_.symbol:
                continue
            if num is not None and num != num_:
                continue
            if orbital is not None and orbital != orbital_.label:
                continue
            indices.append(index)

        return indices

    def as_array(self, parent_basis=None):
        parent_basis = parent_basis or self.parent_basis
        return np.diag([int(elem in self.basis) for elem in parent_basis.basis])

    def complement_array(self, parent_basis=None):
        parent_basis = parent_basis or self.parent_basis
        return np.eye(len(parent_basis.basis)) - self.as_array(parent_basis)

    def vector(self, indices: Iterable, spin=None, parent_basis=None) -> np.ndarray:
        """Return a normalized vector from an iterable of indices."""
        parent_basis = parent_basis or self.parent_basis

        vec = sym.Matrix([0] * len(self.parent_basis))
        for index in indices:
            if isinstance(index, tuple):
                index, weight = index
            else:
                weight = 1
            vec += sym.Matrix([weight * (self.basis[index] == elem)
                             for elem in parent_basis.basis])
        if spin is not None:
            from sympy.physics.quantum import TensorProduct
            vec = TensorProduct(vec, sym.Matrix(spin))

        return vec.normalized()# / sym.sqrt((np.abs(vec) ** 2).sum())

    def transformation_array(self, weights: Iterable[Iterable],
                             elems=None, nums=None, orbitals=None, spins=None):
        elems = elems or [None]
        nums = nums or [None]
        orbitals = orbitals or [None]
        spins = spins or [None]

        indices = []
        from itertools import product
        for (elem, num, orbital) in product(elems, nums, orbitals):
            indices.append(self.index(elem, num, orbital))

        from functools import reduce
        return reduce(lambda a, b: a.row_join(b),
                      (self.vector(zip(index, weight), spin)
                       for weight in weights
                       for index in zip(*indices)
                       for spin in spins))

    @classmethod
    def from_elements(cls, elems: list, orbital_ordering: list = None,
                      excluding: list = None,
                      spinful: bool = False) -> 'Basis':
        orbital_ordering = orbital_ordering or []
        excluding = excluding or []

        elements, counter = ElementCollection(elems).enumerated
        blocks = OrderedDict()

        def orbital_keyfn(orbital):
            if orbital.label in orbital_ordering:
                return orbital_ordering.index(orbital.label)
            else:
                return len(orbital_ordering)

        for elem, num in elements:
            if counter[elem.symbol] <= 1:
                num = ''
            orbitals = {orbital if spinful else orbital.spinless
                        for orbital in elem.orbitals
                        if orbital.label not in excluding}
            blocks[elem, num] = sorted(orbitals, key=orbital_keyfn)

        return cls(blocks)

    def numof(self, *names):
        try:
            from numpy import array
            from itertools import chain
            return array([*chain(*(
                (self.basis.index(name),) if spin is None else
                (self.basis.index(name) * 2,) if spin == 'up' else
                (self.basis.index(name) * 2 + 1,) if spin == 'down' else
                (self.basis.index(name) * 2,
                 self.basis.index(name) * 2 + 1) if spin == 'both' else
                None
                for name_spin in names
                for name, spin in ((name_spin,) if isinstance(name_spin, tuple)
                                   else ((name_spin, None),)) ))])
        except NameError:
            return None

    def spin(self, spin):
        from numpy import array
        from itertools import chain
        return array([*chain(*(
            (i * 2,) if spin == 'up' else
            (i * 2 + 1,) if spin == 'down' else
            (i * 2, i * 2 + 1) if spin == 'both' else
            None
            for i in range(len(self.basis)) ))])

    def prefixed(self, prefix):
        """Return a copy of the basis prefix prepended to all elements."""
        return type(self)([(block[0], [''.join((prefix, item))
                                       for item in block[1]])
                           for block in self._blocks])

    def __getitem__(self, key):
        if isinstance(key, slice):
            length = len(self.basis)
            start = 0 if key.start is None else (length + key.start) % length
            stop = length if key.stop is None else (length + key.stop) % length
            step = 1 if key.step is None else key.step
            key = range(start, stop, step)

        if not isinstance(key, Iterable):
            key = (key,)

        cls = self.__class__
        blocks = OrderedDict()

        for (elem, num), orbitals in self._blocks.items():
            filtered_orbitals = [
                orbital for orbital in orbitals
                if any(self.basis[i] == BasisElement(elem, num, orbital, self)
                       for i in key)]
            if filtered_orbitals:
                blocks[elem, num] = filtered_orbitals

        return cls(blocks, parent_basis=self.parent_basis)

def spin_indices(indices):
    from itertools import chain
    return [*chain.from_iterable((2 * index, 2 * index + 1) for index in indices)]
