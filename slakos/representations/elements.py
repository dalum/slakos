from typing import Callable, Mapping, Iterable

from slakos.collections import SortedDict

from .orbitals import (L_CHEMISTRY_TO_PHYSICS, L_PHYSICS_TO_CHEMISTRY,
                       LM_REAL_REPR, RealAtomicOrbital)

class ElectronConfiguration(SortedDict):
    def __init__(self, *args,
                 convert=L_CHEMISTRY_TO_PHYSICS,
                 **kwargs) -> None:
        def key_function(key):
            n, l = key
            return (int(n), convert[l])

        SortedDict.__init__(self, *args, key_function=key_function, **kwargs)

    def _latex(self, printer):
        return "{configuration}".format(
            configuration=''.join(
                "{n}{l}^{{{count}}}".format(n=n, l=l, count=count)
                for (n, l), count in self.items()))


class ElementCollection:
    def __init__(self, elements: list):
        self.elements = elements

    def __iter__(self):
        return iter(self.elements)

    @property
    def enumerated(self):
        from collections import Counter
        counter = Counter()
        enumerated_elements = []

        for elem in self.elements:
            counter.update((elem.symbol,))
            enumerated_elements.append((elem, counter[elem.symbol]))

        return enumerated_elements, counter


def parse_electron_configuration(element,
                                 convert_l: dict = L_CHEMISTRY_TO_PHYSICS,
                                 convert_lm: dict = LM_REAL_REPR,
                                 orbital_class: type = RealAtomicOrbital):
    element.orbitals.update(orbital_class(n, l, m, s, label=convert_lm[l][m])
                            for n_, l_ in element.electron_configuration
                            for n in (int(n_),)
                            for l in (convert_l[l_],)
                            for m in range(-l, l + 1)
                            for s in (-1/2, 1/2))


global_elements = {}

def element(name: str, symbol: str,
            electron_configuration: ElectronConfiguration,
            parser: Callable) -> type:
    def __init__(self, *coords, symbol=None) -> None:
        from sympy import Point
        self.point = Point(*coords)
        if symbol is not None:
            self.symbol = symbol

    def __repr__(self) -> str:
        return "{cls}({point})".format(cls=self.name, point=self.point)

    def __hash__(self):
        return hash(self.point) + hash(self.name)

    def __eq__(self, other):
        return self.point == other.point and self.name == other.name

    cls = type(name, (), locals().copy())
    cls.orbitals = set()
    parser(cls)
    global_elements[name] = cls

    return cls


Carbon = element(
    "Carbon", "C",
    ElectronConfiguration({'2s': 2, '2p': 2}),
    parse_electron_configuration)

Selenium = element(
    "Selenium", "Se",
    ElectronConfiguration({'4s': 2, '4p': 4}),
    parse_electron_configuration)

Stannum = Tin = element(
    "Stannum", "Sn",
    ElectronConfiguration({'5s': 2, '5p': 2}),
    parse_electron_configuration)
