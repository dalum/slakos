import sympy as sym

from typing import Iterable
from .abstractvector import normalize

sympify = sym.sympify

ONE = sym.S(1)
I = sym.I
NaN = sym.nan
sqrt = sym.sqrt
cos, sin, tan = sym.cos, sym.sin, sym.tan
acos, asin, atan2 = sym.acos, sym.asin, sym.atan2

from functools import reduce
from operator import add, mul, matmul, eq
def prod(iterable, *, reduce=reduce, mul=mul):
    return reduce(mul, iterable, 1)

def diag(span: Iterable):
    return sum(v/v.c for v in span)

def angular_momentum(spin_span: Iterable, real_span: Iterable):
    span = [*spin_span]
    l = sympify(len(span) - 1) / 2

    L_z = sum(span[i]/span[i].c * m
             for i in range(len(span))
             for m in (l - i,))

    L_plus = sum(span[i-1]/span[i].c * sqrt(l * (l + 1) - m * (m + 1))
                 for i in range(1, len(span))
                 for m in (l - i,))

    L_minus = sum(span[i+1]/span[i].c * sqrt(l * (l + 1) - m * (m - 1))
                  for i in range(len(span)-1)
                  for m in (l - i,))

    L_x = ONE / 2 * (L_plus + L_minus)
    L_y = -I / 2 * (L_plus - L_minus)

    return sum(L / r for L, r in zip((L_x, L_y, L_z), real_span))

def gram_schmidt(basis_in, init=()):
    basis_out = [normalize(v) for v in init]
    for v in basis_in:
        basis_out.append(normalize(v - sum((v_.c @ v) * v_ for v_ in basis_out)))
    return basis_out

def translation(v, aux, span: Iterable = ()):
    """Return a rank-2 tensor representing a translation.

    The translation is defined by the vector `v` and embedded into a
    higher-order dimensional space, requiring at least one auxiliary dimension,
    in `span`.

    """
    return ( sum(v/v.c for v in span) + v/aux.c )

def rotation_plane(v1, v2, angle, span: Iterable = ()):
    """Return a rank-2 tensor representing a rotation in a plane.

    The plane is defined by the vectors `v1` and `v2`, and `angle` represents
    the rotation angle.  An optional iterable, `space`, can be passed to add
    unity diagonal terms for the vectors, `v` in `space`, not in the set `{v1,
    v2}`.

    """
    return ( ( v1/v1.c + v2/v2.c ) * (cos(angle) - ONE) + # We add it again below
             (-v1/v2.c + v2/v1.c ) * sin(angle) +
             sum(v/v.c for v in gram_schmidt(span, (v1, v2))) )

def spin_half_rotation(axis, angle, spin_span, real_span):
    return ( cos(angle / 2) * diag(spin_span) +
             2 * I * sin(angle / 2) *
             (angular_momentum(spin_span, real_span) @ axis) )

def spherical_decomposition(v, span):
    """Return vector components of a vector in spherical coordinates.

    This function takes a vector, `v`, and a 3-tuple, `space` of 3 vectors in
    which the spherical transformation is to be made.

    The returned parameters are: r, theta, phi.

    """
    x, y, z = span#, *_ = space
    r = sym.Symbol('r', real=True, positive=True)
    theta = acos((z @ v) / r)
    phi = atan2((y @ v), (x @ v))

    # Fix angle to 0 when the vector is parallel to the third axis.
    if phi is NaN:
        phi = 0

    return r, theta, phi, {r: sqrt(v @ v)}

def slater_koster(degree, theta, phi, space, parameter_space):
    S = space
    C = parameter_space

    R = prod( rotation_plane(S.z, S.x, -theta,
                             (S._, S.x, S.y, S.z)) @
              rotation_plane(S.x, S.y, -phi,
                             (S._, S.x, S.y, S.z))
              for _ in range(degree) )

    E = prod( C.sss / S._ / S._ + C.spp / S.z / S._ +
              C.spm / S._ / S.z + C.pp0 / S.z / S.z +
              C.pp1 / (S.x / S.x + S.y / S.y)
              for _ in range(degree) )

    return R, E

if __name__ == '__live_coding__' or __name__ == '__main__':
    from abstractvector import AbstractVectorSource, represent, span_in
    from parameters import ParameterSource, spanning

    A = AbstractVectorSource('A')
    B = AbstractVectorSource('B')

    S = AbstractVectorSource('S')
    V = ParameterSource('V')

    a, b = sym.symbols('a, b', real=True, positive=True)
    #b = tan(b)
    #a = sym.pi / 3

    basis = (A.w, A._, A.x, A.y, A.z)
    T = translation(b * A.z, A.w, basis)
    P = rotation_plane(A.x, A.y, a, basis)
    O = P @ T

    atom_0 = A.x + A.w
    v = O @ atom_0 - atom_0
    r, theta, phi, subs = spherical_decomposition(v, basis[-3:])
    phi = 0

    R, E = slater_koster(1, theta, phi, A, V)

    I_S = S.up/S.up + S.down/S.down
    I_E = A.s/A.s + A.x/A.x + A.y/A.y + A.z/A.z

    V_ = spanning(V['lambda'], V.pp0, V.pp1, real=True)

    A[0] = A.z
    A[1] = normalize(A.x + I * A.y)
    A[-1] = normalize(A.x - I * A.y)

    angmom = angular_momentum((A[1], A[0], A[-1]), basis[-3:])
    sigma = angular_momentum((S.up, S.down), basis[-3:])
    SOC = V['lambda'] / (angmom % sigma)

    H_1 = ((I_S * R)).dag @ (I_S * E)[..., V_] @ ((I_S * O) @ (I_S * R))
    H_1_ = ((I_S * O) @ (I_S * R)).dag @ (I_S * E)[..., V_] @ ((I_S * R))
    xi, kappa, alpha = sym.symbols('xi, kappa, alpha', real=True)
    subs_ = {sym.sqrt(r ** 2 - b ** 2).conjugate(): sym.sqrt(r ** 2 - b ** 2)}
    H_1 = H_1 * sym.exp(I * xi) + H_1_ * sym.exp(-I * xi)
    #expr = (H_1 + H_1.dag)
    # orbital1 = A.z * S.up
    # orbital2 = A.z * S.up
    # k = (orbital1 @ expr @ orbital2).simplify().subs(subs_).simplify()
    # k = (orbital2 @ expr @ orbital1).simplify().subs(subs_).simplify()
    H = (H_1).simplify().subs(subs_).simplify().subs(subs_).simplify() + SOC[..., V_]
    # # orbital1 = A.z * S.up
    # # orbital2 = A.z * S.up
    # # expr = (orbital1 @ H @ orbital2).simplify()
    # # orbital1 = A.z * S.down
    # # orbital2 = A.z * S.down
    # # expr = sym.simplify(orbital1 @ H_ @ orbital2).subs({sym.sqrt(r ** 2 - b ** 2).conjugate(): sym.sqrt(r ** 2 - b ** 2)}).simplify()

    # #mat = sym.Matrix(represent(H, orbitals))

    P = diag(span_in((A.z) * (S.up + S.down)))
    Q = diag(span_in((A.x + A.y) * (S.up + S.down)))

    omega = sym.Symbol('omega')

    full_span = (A.x + A.y + A.z) * (S.up + S.down)
    H_eff = P @ H @ P + P @ (H @ (Q @ (diag(full_span) / omega + (H / (omega ** 2)) @ (Q @ (H @ P)))))

    # model = sym.Matrix(represent(H_eff, span_in((A.z) * (S.up + S.down))))#.subs(subs)
    model = sym.Matrix(represent(H, span_in((A.x + A.y + A.z) * (S.up + S.down))))#.subs(subs)
    symbols = sorted(model.free_symbols, key=lambda a: a.name)
    import numpy as np
    fn = sym.lambdify(symbols, model, 'numpy')
    params = [-2, 2, 1, 1, 0.5, np.pi]
    a_, b_ = np.pi / 3, 0.5
    r_ = np.sqrt(b_**2 + (np.cos(a_) - 1)**2 + np.sin(a_)**2)
    params = lambda kappa, omega: [0.03, -2, 2, a_, b_, omega, r_, kappa]
    params = lambda kappa, omega: [0.03, -2, 2, a_, b_, r_, kappa]
    from numpy.linalg import eigvalsh
    import matplotlib.pyplot as plt
    #params = [0.03, -2, 2, a_, b_, r_, np.pi]
    #sym.pprint(mat)

    # orbital1 = A.y * S.down
    # orbital2 = A.x * S.up

    # for orbital in orbitals:
    #     for orbital_ in orbitals:
    #         expr = (((I_S * R) @ orbital2).c @ (I_S * E + SOC) @ (R_spin * (R_orbit @ R) @ orbital1))[..., V_]
    #         expr2 = sym.simplify(expr)
    #         expr3 = ((I_S * R).dag @ ((I_S * E + SOC) @ (R_spin * (R_orbit @ R)))).transpose(1, 2)[..., V_]
    #         expr4 = sym.simplify((orbital2.c @ expr3 @ orbital1))
    #         expr5 = expr2 - expr4
    #         print(orbital, orbital_, expr5)

    # subs, expr = sym.cse(V_ @ expr)
    # expr = expr
    # expr = ((I_S * R) @ orbital2).c @ (SOC) @ (R_spin * (R_orbit @ R) @ orbital1)
    # subs, expr = sym.cse(V_ @ expr)
    # expr = expr
    # expr = ((I_S * R) @ orbital2).c @ (I_S * E + SOC) @ (R_spin * (R_orbit @ R) @ orbital1)
    # subs, expr = sym.cse(V_ @ expr)
    # expr = expr
