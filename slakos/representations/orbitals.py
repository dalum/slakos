from typing import Callable, Mapping, Iterable

L_CHEMISTRY_TO_PHYSICS = {
    's': 0,
    'p': 1,
    'd': 2,
    'f': 3,
    'g': 4,
    'h': 5,
    'i': 6,
    'k': 7,
    'l': 8}

L_PHYSICS_TO_CHEMISTRY = {
    value: key
    for key, value in L_CHEMISTRY_TO_PHYSICS.items()}

LM_REAL_REPR = {
    0: {0: 's'},
    1: {-1: 'p_{y}', 0: 'p_{z}', 1: 'p_{x}'},
    2: {-2: ..., -1: ..., 0: ..., 1: ..., 2: ...}
}

M_CHEMISTRY_TO_PHYSICS = {
    'sigma': 0,
    'pi': 1,
    'delta': 2,
    'phi': 3}

M_PHYSICS_TO_CHEMISTRY = {
    value: key
    for key, value in M_CHEMISTRY_TO_PHYSICS.items()}


class AtomicOrbital:
    def __init__(self, n: int, l: int, m: int, s: float = None,
                 label: str = None) -> None:
        self.__dict__.update(locals())

    @property
    def quantum_numbers(self):
        return self.n, self.l, self.m, self.s

    @property
    def spinless(self):
        cls = self.__class__
        return cls(self.n, self.l, self.m, ..., label=self.label)

    def __repr__(self):
        if self.label is not None:
            return str(self.label)
        return '{cls}({params})'.format(
            cls=self.__class__.__name__,
            params=', '.join(map(repr, (self.n, self.l, self.m, self.s))))

    def __hash__(self):
        return hash((self.__class__.__name__, self.n, self.l, self.m, self.s))

    @staticmethod
    def _compare(self, other):
        return self == other or self is Ellipsis or other is Ellipsis

    def __eq__(self, other):
        return (self.__class__.__name__ is other.__class__.__name__ and
                self._compare(self.n, other.n) and
                self._compare(self.l, other.l) and
                self._compare(self.m, other.m) and
                self._compare(self.s, other.s))

    def __setattr__(self, name, value):
        raise AttributeError("{cls} does not support attribute assignment"
                             "".format(cls=self.__class__))

class RealAtomicOrbital(AtomicOrbital):
    pass

class ComplexAtomicOrbital(AtomicOrbital):
    pass


s = RealAtomicOrbital(..., 0, 0, ..., label='s')
p_x = RealAtomicOrbital(..., 1, 1, ..., label='p_x')
p_y = RealAtomicOrbital(..., 1, -1, ..., label='p_y')
p_z = RealAtomicOrbital(..., 1, 0, ..., label='p_z')
