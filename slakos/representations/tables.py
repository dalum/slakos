import sympy as sym

from functools import lru_cache

from slakos.core.unify import fuzzy_subs
from slakos.collections.mappings import MultiDimensionalDict
from slakos.simplify import identity, powfact

from .orbitals import (L_CHEMISTRY_TO_PHYSICS, L_PHYSICS_TO_CHEMISTRY,
                       M_CHEMISTRY_TO_PHYSICS, M_PHYSICS_TO_CHEMISTRY,
                       LM_REAL_REPR)

class MatrixElementTable(MultiDimensionalDict):
    names=['V']

    def __init__(self, elements: set, indices: list, **options) -> None:
        MultiDimensionalDict.__init__(self)

        for option, value in options.items():
            setattr(self, option, value)

        for unique in indices:
            self.generate(elements, unique=unique)

    def symbol(self, index: tuple, orbitals: tuple, m: int,
               convert_l: dict = L_PHYSICS_TO_CHEMISTRY,
               convert_m: dict = M_PHYSICS_TO_CHEMISTRY) -> sym.Expr:
        unique, *elems = index
        orbital_elems = sorted(zip(orbitals, elems),
                                key=lambda a: (a[0].l, a[0].n, a[1].symbol))

        symbol_min, symbol_max = (elem.symbol for orbital, elem in orbital_elems)
        l_min, l_max = (convert_l[orbital.l]
                        for orbital, elem in orbital_elems)
        bond = convert_m[m]

        _default = "_{l_min}{l_max}_{bond}".format(l_min=l_min,
                                                   l_max=l_max,
                                                   bond=bond)
        _default_sup = []

        if symbol_min is not None:
            _default_sup.append(symbol_min)
        if symbol_max is not None:
            _default_sup.append(symbol_max)
        if unique is not None:
            _default_sup.append(str(unique))

        if _default_sup:
            _default += "__" + "".join(_default_sup)

        r, r_0 = sym.symbols('r, r_0', real=True, positive=True)

        expr = sum(sym.Symbol(name + _default, real=True) *
                   ((r - r_0) / r_0) ** i
                   for i, name in enumerate(self.names))

        return expr

    def generate(self, elements: set, unique: int = None) -> None:
        from itertools import product
        for elem1, elem2 in product(elements, repeat=2):
            for orbitals in product(elem1.orbitals, elem2.orbitals):
                index = (unique, elem1, elem2)
                self._generate_table_entry(index, orbitals)

    def _generate_table_entry(self, index: tuple, orbitals: tuple):
        pass

    def __call__(self, vector: sym.Point, index: tuple,
                 orbitals: tuple, equilibrium_subs: dict = None) -> sym.Expr:
        """Return an evaluated hopping term between two atoms.

        Parameters
        ----------
        vector : Point
            The vector pointing between the two atoms.
        index : tuple
            Index in the table of the form (unique, elem1, elem2)
        orbitals : tuple
            Orbitals on the two atoms.
        equilibrium_subs : dict
            Substitutions for unstrained lattice conditions.

        Returns
        -------
        expr : sym.Expr
            A SymPy expression satisfying the given constraints.

        """
        equilibrium_subs = equilibrium_subs or {}

        vector_norm = abs(vector)
        if vector_norm != 0:
            normalized_vector = vector * vector_norm ** (-1)
        else:
            normalized_vector = vector

        equilibrium_vector = fuzzy_subs(vector, equilibrium_subs)
        equilibrium_norm = abs(equilibrium_vector)

        # if (index, *orbitals) not in self:
        #     raise KeyError("invalid key: {}.".format((index, *orbitals)))

        key = (index, *orbitals[0].quantum_numbers,
                      *orbitals[1].quantum_numbers)

        # Raw form
        expr = self._process(self[key], vector)
        # With direction cosines
        expr = fuzzy_subs(expr, dict(zip(['L', 'M', 'N'], normalized_vector)))
        # Substituting for displacements
        expr = fuzzy_subs(expr, {'r_0': equilibrium_norm, 'r': vector_norm})

        return expr

    def _process(self, expr, vector):
        return expr

class OnsiteEnergyTable(MatrixElementTable):
    names = ['E']
    convert = L_PHYSICS_TO_CHEMISTRY

    def symbol(self, index, orbitals: tuple) -> sym.Expr:
        unique, *elems = index

        if orbitals[0] != orbitals[1] or elems[0] != elems[1] or unique != 0:
            return sym.S(0)

        orbital = orbitals[0]
        l = self.convert[orbital.l]
        m = orbital.m
        label = l[m] if isinstance(l, dict) else l
        symbol = elems[0].symbol

        _default = "_{label}".format(label=label)
        _default_sup = []

        if symbol is not None:
            _default_sup.append(symbol)

        if _default_sup:
            _default += "__" + "__".join(_default_sup)

        r, r_0 = sym.symbols('r, r_0', real=True, positive=True)

        expr = sum(sym.Symbol(name + _default, real=True) *
                   ((r - r_0) / r_0) ** i
                   for i, name in enumerate(self.names))

        return expr

    def _generate_table_entry(self, index: int, orbitals: tuple) -> None:
        key = (index, *orbitals[0].spinless.quantum_numbers,
                      *orbitals[1].spinless.quantum_numbers)
        if key in self:
            return

        expr = self.symbol(index, orbitals)

        self[key] = expr

    def _process(self, expr, vector):
        if abs(vector) != 0:
            return sym.S(0)
        return expr

class SlaterKosterTable(MatrixElementTable):
    def _generate_table_entry(self, index, orbitals: tuple) -> None:
        key = (index, *orbitals[0].spinless.quantum_numbers,
                      *orbitals[1].spinless.quantum_numbers)
        if key in self:
            return

        n_1, l_1, m_1, s_1 = orbitals[0].quantum_numbers
        n_2, l_2, m_2, s_2 = orbitals[1].quantum_numbers

        from sympy import sqrt

        def V(m):
            return self.symbol(index, orbitals, m)

        L, M, N = sym.symbols('L, M, N', real=True)

        if (l_1, l_2) == (0, 0):
            expr = V(0)

        elif (l_1, l_2) == (0, 1):
            for L_, m_i in [(L, 1), (M, -1), (N, 0)]:
                if m_2 == m_i:
                    expr = L_ * V(0)

        elif (l_1, l_2) == (1, 0):
            for L_, m_i in [(L, 1), (M, -1), (N, 0)]:
                if m_1 == m_i:
                    expr = -L_ * V(0)

        elif (l_1, l_2) == (1, 1):
            from itertools import product
            for (L_, m_i), (M_, m_j) in product([(L, 1), (M, -1), (N, 0)],
                                                repeat=2):
                if (m_1, m_2) == (m_i, m_i):
                    expr = (L_ ** 2) * V(0) + (1 - L_ ** 2) * V(1)
                elif (m_1, m_2) == (m_i, m_j):
                    expr = L_ * M_ * V(0) - M_ * L_ * V(1)

        self[key] = expr

    def _process(self, expr, vector):
        if abs(vector) == 0:
            raise ValueError("SlaterKosterTable only accepts non-zero "
                             "symbolic vectors")
        return expr


from sympy.physics.matrices import msigma

class OnsiteSOCTable(OnsiteEnergyTable):
    names = ['lambda']
    sigma = {0: sym.eye(2), 1: msigma(1), 2: msigma(2), 3: msigma(3)}
    convert = L_PHYSICS_TO_CHEMISTRY

    def symbol(self, index, orbitals: tuple) -> sym.Expr:
        unique, *elems = index

        if elems[0] != elems[1] or unique != 0:
            return sym.S(0)

        orbital = orbitals[0]
        l = self.convert[orbital.l]
        symbol = elems[0].symbol

        _default = "_{l}".format(l=l)
        _default_sup = []
        if symbol is not None:
            _default_sup.append(symbol)
        if _default_sup:
            _default += "__" + "__".join(_default_sup)

        r, r_0 = sym.symbols('r, r_0', real=True, positive=True)

        expr = sum(sym.Symbol(name + _default, real=True) *
                   ((r - r_0) / r_0) ** i
                   for i, name in enumerate(self.names))

        return expr

    def _generate_table_entry(self, index, orbitals: tuple) -> None:
        key = (index, *orbitals[0].spinless.quantum_numbers,
                      *orbitals[1].spinless.quantum_numbers)
        if key in self:
            return

        n_1, l_1, m_1, s_1 = orbitals[0].quantum_numbers
        n_2, l_2, m_2, s_2 = orbitals[1].quantum_numbers

        lmbd = self.symbol(index, orbitals)

        L, M, N = sym.symbols('L, M, N', real=True)

        if (l_1, l_2) == (1, 1):
            for k, m_i, m_j in [(3,   1, -1),
                                (1,  -1,  0),
                                (2,   0,  1)]:
                if (m_1, m_2) == (m_i, m_j):
                    expr = -sym.I * self.sigma[k] * lmbd / 2
                    break
                elif (m_1, m_2) == (m_j, m_i):
                    expr = sym.I * self.sigma[k] * lmbd / 2
                    break
            else:
                expr = sym.S(0)
        else:
            expr = sym.S(0)

        self[key] = expr

    def _process(self, expr, vector):
        if abs(vector) != 0:
            raise ValueError("OnsiteSOCTable only accepts zero-length vectors")
        return expr


class PodolskiyVoglTable(MatrixElementTable):
    def _generate_table_entry(self, orbital1, orbital2,
                 simplifier=powfact) -> None:
        if (orbital1.spinless, orbital2.spinless) in self:
            return

        n_1, l_1, m_1, s_1 = orbital1.quantum_numbers
        n_2, l_2, m_2, s_2 = orbital2.quantum_numbers

        l_lt = min(l_1, l_2)
        l_gt = max(l_1, l_2)

        def V(l1, l2, m):
            from sympy import Symbol
            return Symbol("V_{}_{}_{}".format(l1, l2, m))

        from sympy import sqrt, cos, sin, acos, asin

        L, M, N = sym.symbols('L, M, N', real=True)

        gamma = sym.Piecewise((0, abs(N) >= 1),
                              (acos(-L / (sqrt(1 - N ** 2))), True))

        def primed_sum(iterable, fn) -> float:
            total_sum = 0
            for t in iterable:
                try:
                    total_sum += fn(t)
                except ValueError:
                    pass
            return total_sum

        def heaviside(expr):
            return 1 if expr >= 0 else 0

        def d(l, m, m_) -> float:
            from sympy import factorial as fac

            term1 = ((1 + N) / 2) ** l * ((1 - N) / (1 + N)) ** ((m - m_) / 2)
            term2 = sqrt(fac(l + m_) * fac(l - m_) * fac(l + m) * fac(l - m))
            term3 = primed_sum(range(0, 2 * l + 1 + 1),
                lambda t: ((-1) ** t / (fac(l + m_ - t) * fac(l - m - t) *
                                        fac(t) * fac(t + m - m_)) *
                           ((1 - N) / (1 + N)) ** t))

            return sym.Piecewise( (1 if m == m_ else 0, abs(N) >= 1),
                                  (simplifier(term1 * term2 * term3), True) )

        def A(m):
            if m == 0:
                return sqrt(2) / 2
            return (-1) ** (abs(m)) * (heaviside(m) * cos(abs(m) * gamma) -
                                       heaviside(-m) * sin(abs(m) * gamma))

        def B(m):
            if m == 0:
                return 0
            return (-1) ** (abs(m)) * (heaviside(m) * sin(abs(m) * gamma) +
                                       heaviside(-m) * cos(abs(m) * gamma))

        def S(l, m, m_) -> float:
            return A(m) * ((-1) ** (abs(m_)) * d(l, abs(m), abs(m_)) +
                             d(l, abs(m), -abs(m_)))

        def T(l, m, m_) -> float:
            return B(m) * ((-1) ** (abs(m_)) * d(l, abs(m), abs(m_)) -
                             d(l, abs(m), -abs(m_)))

        prefactor = (-1) ** ((l_1 - l_2 + abs(l_1 - l_2)) / 2)
        term1 = sum( V(l_lt, l_gt, abs(m_)) *
                     simplifier(S(l_1, m_1, abs(m_)) *
                                S(l_2, m_2, abs(m_)) +
                                T(l_1, m_1, abs(m_)) *
                                T(l_2, m_2, abs(m_)))
                     for m_ in range(1, l_lt + 1))
        term2 = V(l_lt, l_gt, 0) * simplifier(2 * A(m_1) * A(m_2) *
                                              d(l_1, abs(m_1), 0) *
                                              d(l_2, abs(m_2), 0))

        expr = prefactor * (term1 + term2)

        #return prefactor * (term1 + term2)
        def call(vec, symbol):
            L_, M_, N_ = vec / abs(vec)
            return expr.subs({L: L_, M: M_, N: N_})

        self[orbital1.spinless, orbital2.spinless] = call
