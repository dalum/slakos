import sympy as sym

# Put the square root in global scope so we can re-assign it at will
sqrt = sym.sqrt

ZERO = sym.S(0)
ONE = sym.S(1)
IMAG = sym.I

from collections.abc import Mapping
from typing import Iterable
from itertools import product
from collections import Counter

## Utilities, should probably be put elsewhere

from functools import reduce
from operator import add, mul, matmul, eq

def sum(iterable, *, reduce=reduce, add=add):
    return reduce(add, iterable, ZERO)

def prod(iterable, *, reduce=reduce, mul=mul):
    return reduce(mul, iterable, ONE)

## Functions

def dagger(obj):
    if order(obj) > 1:
        return obj.dagger().conjugate()
    return obj.conjugate()

def inner_product(obj1, obj2, **kwargs):
    if order(obj1) > 0 and order(obj2) > 0:
        return obj1.inner_product(obj2, **kwargs)
    return obj1 * obj2

def normalize(vector):
    """Return a normalized vector expression.

    Example:

    >>> V = AbstractVectorSpace('V')
    >>> v = V.a + V.b
    >>> v @ v
    2
    >>> v = normalize(v)
    >>> v @ v
    1

    """
    N = sqrt(vector.c @ vector)
    return vector / N

def outer_product(obj1, obj2, **kwargs):
    if order(obj1) > 0 and order(obj2) > 0:
        return obj1.outer_product(obj2, **kwargs)
    return obj1 * obj2

def order(obj):
    """Return the order of an object.

    This function checks if the object has the attribute `_order` and returns
    it, otherwise it returns zero.

    """
    return getattr(obj, '_order', 0)

def represent(obj, basis: Iterable, *, dtype=list):
    """Return a representation of the object in a given basis.

    If the object has non-zero order (see the order function in this module),
    the representation is given by nested lists or optionally any other
    data-type which accepts an iterable, specified using the keyword-only
    argument `dtype`.  If a zero-order object is passed, the object is returned
    unchanged.

    The second argument, `basis`, is an ordered iterable of basis vectors to be
    used in the representation.  The basis need not span the entire space, nor
    be orthonormal for `represent` to work.

    """
    basis = [*basis]

    if order(obj):
        return dtype(represent(inner_product(vec, obj), basis)
                     for vec in basis)
    return obj

def sanitize(_dict: dict):
    return {key: value for key, value in _dict.items() if value}

def span_in(obj) -> list:
    """Return a list of all non-zero inputs to `obj`.

    Example:

    >>> V = AbstractVectorSpace('V')
    >>> span_in(V.x + V.y + V.z)
    [V.x:, V.y:, V.z:]

    """
    return sorted(obj, key=lambda a: a.name)

def tensor_product(obj1, obj2, **kwargs):
    if order(obj1) > 0 and order(obj2) > 0:
        return obj1.tensor_product(obj2, **kwargs)
    return obj1 * obj2


class AbstractVectorSource:
    """A source for abstract vectors.

    AbstractVectorSource instances have overloaded `__getattr__`,
    `__getitem__`, `__setattr__`, `__setitem__` methods to automatically
    initialize a new abstract vector in their space.  Example:

    >>> V = AbstractVectorSource('V')
    >>> V.c = V.a + V['b']
    >>> V.c
    V.c:
    >>> V['c'] @ V.b
    1

    """

    _formats = {'latex': lambda a: sym.Symbol('{}hat'.format(a))}

    def __init__(self, name: str, *, default_norm=1):
        super().__setattr__('_dict', {})
        super().__setattr__('_name', name)
        super().__setattr__('_default_norm', default_norm)
        super().__setattr__('_', AbstractZeroVector(name='_', source=self))

        super().__init__()

    ## Private methods

    def _post_process_source(self, vector):
        """Intended for subclasses to post-process sourced vectors."""
        return vector

    ## Public methods

    def take(self, iterable):
        """Return a generator of vectors with names taken from `iterable`.

        """
        return (self[item] for item in iterable)

    ## Magic methods

    def _format_latex(self, name):
        return self._formats['latex'](name)

    def __getattr__(self, attr):
        return self.__getitem__(attr)

    def __getitem__(self, item):
        if isinstance(item, slice):
            if item.stop is None:
                raise ValueError('must specify end of range in slice')

            return self.take(range(item.start or 0,
                                   item.stop,
                                   item.step or 1))

        if item not in self._dict:
            self._dict[item] = self._post_process_source(
                AbstractVectorBase(name=item, source=self))
        return self._dict[item]

    def __repr__(self):
        return '{}'.format(self._name)

    def __setattr__(self, attr, value):
        self.__setitem__(attr, value)

    def __setitem__(self, item, value):
        if item not in self._dict:
            _dict = {key: val for key, val in value.items()}
            self._dict[item] = self._post_process_source(
                SourcedAbstractVectorExpression(
                    _dict,
                    order = order(value),
                    prefactor = value.prefactor,
                    name = item,
                    source = self))
        else:
            raise ValueError('{} already defined'.format(item))


class AbstractVector:
    @property
    def c(self):
        """Shorthand for the complex conjugate."""
        return self.conjugate()

    @property
    def dag(self):
        return self.dagger()

    @property
    def is_singular(self):
        return len(self) == 1 and all(getattr(value, 'is_singular', True)
                                      for key, value in self.items())

    @property
    def is_null(self):
        return bool(self.prefactor == ZERO or
                    (len(self) == 0 and order(self) > 0))

    @property
    def T(self):
        try:
            return self.transpose(0, 1)
        except IndexError:
            return self

    ## Public methods

    def conjugate(self):
        """Return a new vector expression by complex conjugating each entry."""
        return AbstractVectorExpression(
            { key: self.coeff(key).conjugate()
              for key in self.components },
            order = order(self),
            prefactor = self.prefactor.conjugate())

    def dagger(self):
        return self.hermitian_conjugate()

    def hermitian_conjugate(self):
        raise NotImplementedError

    def inner_product(self, other, *, post_function=outer_product):
        """Compute the inner product.

        In bra-ket notation, this is equivalent to `<self|other>`.  The `@`
        (`__matmul__`) operator has been overloaded to support the notation
        `self @ other`.  `__getitem__` has also been overloaded such that
        `self[other]` is equivalent to `other @ self`.

        """
        components = {*self.components, *other.components}
        return (self.prefactor * other.prefactor *
                sum(post_function(self.coeff(key), other.coeff(key))
                    for key in components))

    def outer_product(self, other):
        """Compute the outer product.

        In bra-ket notation, this is equivalent to `|self><other|` The `/`
        (`__truediv__`) operator has been overloaded to support the equivalent
        notation `self/other`.

        """
        return AbstractVectorExpression(
            sanitize({ key: outer_product(self.with_prefactor(ONE),
                                          other.coeff(key))
                       for key in other.components }),
            order = order(self) + order(other),
            prefactor = other.prefactor * self.prefactor)

    def simplify(self):
        return AbstractVectorExpression(
            sanitize({ key: value.simplify()
                       for key, value in self.items() }),
            order = order(self),
            prefactor = self.prefactor.simplify())

    def subs(self, *args, **kwargs):
        return AbstractVectorExpression(
            sanitize({ key: value.subs(*args, **kwargs)
                       for key, value in self.items() }),
            order = order(self),
            prefactor = self.prefactor.subs(*args, **kwargs))

    def tensor_product(self, other):
        """Compute the tensor product.

        In bra-ket notation, this is equivalent to `|self>|other>` The `*`
        (`__mul__`) operator has been overloaded to support the equivalent
        notation `self*other`.

        """
        return AbstractVectorExpression(
            { key*key_: self.coeff(key) * other.coeff(key_)
              for key in self for key_ in other },
            order = max(order(self), order(other)),
            prefactor = self.prefactor * other.prefactor )

    def transpose(self, axis1, axis2):
        raise NotImplementedError

    def with_prefactor(self, prefactor):
        return AbstractVectorExpression(
            self._dict,
            order = order(self),
            prefactor = prefactor)

    ## Magic methods

    def __add__(self, other):
        if not other:
            return self
        if not self:
            return other

        if order(self) != order(other):
            raise ValueError('cannot add objects of different orders: '
                             '{}, {}'.format(order(self), order(other)))

        if order(self) > 0:
            components = {*self.components, *other.components}

            if self.prefactor == other.prefactor:
                prefactor = self.prefactor
                _dict = sanitize({ key: self.coeff(key) + other.coeff(key)
                                   for key in components })
            else:
                prefactor = ONE
                _dict = sanitize({ key: self.prefactor * self.coeff(key) +
                                   other.prefactor * other.coeff(key)
                                   for key in components })
        else:
            return self.prefactor + getattr(other, 'prefactor', other)

        return AbstractVectorExpression(_dict,
                                        order = order(self),
                                        prefactor = prefactor)

    def __eq__(self, other):
        try:
            return self @ self == other @ self
        except AttributeError:
            return False

    def __matmul__(self, other):
        return inner_product(self, other.T)

    def __mod__(self, other):
        return inner_product(self, other, post_function=tensor_product)

    def __mul__(self, other):
        if isinstance(other, AbstractVector):
            return self.tensor_product(other)

        tmp = other.__rmul__(self)
        if tmp is NotImplemented:
            return self.mul(other)
        return tmp

    def __ne__(self, other):
        return not self.__eq__(other)

    def __neg__(self):
        return AbstractVectorExpression(
            { key: self.coeff(key) for key in self.components },
            order = order(self),
            prefactor = -self.prefactor)

    __radd__ = __add__

    def __rmatmul__(self, other):
        return inner_product(other, self.T)

    def __rmod__(self, other):
        return inner_product(other, self, post_function=tensor_product)

    def __rmul__(self, other):
        if isinstance(other, AbstractVector):
            return other.tensor_product(self)

        tmp = other.__mul__(self)
        if tmp is NotImplemented:
            return self.mul(other)
        return tmp

    def __rsub__(self, other):
        return (-self).__add__(other)

    def __sub__(self, other):
        return self.__add__(-other)

    def __truediv__(self, other):
        if isinstance(other, AbstractVector):
            return self.outer_product(other)
        return self.__mul__(1 / other)


class AbstractVectorExpression(AbstractVector):
    ## Constructors

    def __init__(self, _dict, order, prefactor):
        self._dict = _dict if prefactor else {}
        self._order = order
        self._prefactor = prefactor if order and self._dict else ZERO

    @classmethod
    def from_sequence(cls, sequence, prefactor):
        if len(sequence) < 2:
            raise ValueError("length of sequence must be at least 2")

        if len(sequence) == 2:
            return cls({sequence[0]: sequence[1]},
                       order = 1,
                       prefactor = prefactor)

        return cls({sequence[0]: cls.from_sequence(sequence[1:], ONE)},
                   order = len(sequence) - 1,
                   prefactor = prefactor)

    ## Properties

    @property
    def components(self):
        return iter(self._dict.keys())

    @property
    def entries(self):
        if self.is_singular or self.is_null:
            return [self]
        return [AbstractVectorExpression({key: a_value},
                                         order = order(self),
                                         prefactor = self.prefactor)
                for key, value in self.items()
                for a_value in getattr(value, 'entries', (value,))]

    @property
    def prefactor(self):
        return self._prefactor

    ## Private methods

    @staticmethod
    def _singular_sequence(obj):
        is_singular = getattr(obj, 'is_singular', None)

        if is_singular is None:
            return [obj]
        elif is_singular is True:
            (key, value), = obj.items()
            return [key] + [*obj._singular_sequence(value * obj.prefactor)]

        raise ValueError('_singular_sequence: expression is not singular')

    ## Public methods

    def coeff(self, item):
        """Return the coefficient explicitly written in front of `item`."""
        null_tensor = AbstractVectorExpression(
            {}, order=order(self)-ONE, prefactor=ZERO)
        return self.get(item, null_tensor)

    def get(self, item, default=None):
        return self._dict.get(item, default)

    def hermitian_conjugate(self):
        return self.conjugate().transpose(0, 1)

    def items(self):
        return sorted(self._dict.items(), key=lambda a: a[0].name)

    def keys(self):
        return self._dict.keys()

    def mul(self, other):
        if self.is_singular:
            return AbstractVectorExpression(
                sanitize({key: value * other for key, value in self.items()}),
                order = order(self),
                prefactor = self.prefactor)
        return AbstractVectorExpression(
            self._dict,
            order = order(self),
            prefactor = self.prefactor * other)

    def rotate(self, pivot=1):
        if self.is_null:
            return self

        if self.is_singular:
            *sequence, tail = self._singular_sequence(self)
            sequence = sequence[pivot:] + sequence[:pivot] + [tail]
            return AbstractVectorExpression.from_sequence(
                sequence,
                prefactor = ONE)
        return sum(elem.rotate(pivot) for elem in self.entries)

    def transpose(self, axis1, axis2):
        if self.is_null:
            return self

        if self.is_singular:
            *sequence, tail = self._singular_sequence(self)
            sequence[axis1], sequence[axis2] = sequence[axis2], sequence[axis1]

            return AbstractVectorExpression.from_sequence(
                sequence + [tail],
                prefactor = ONE)
        return sum(elem.transpose(axis1, axis2) for elem in self.entries)

    def values(self):
        return self._dict.values()

    ## Magic methods

    def __bool__(self):
        return not self.is_null

    def __contains__(self, item):
        return item in self._dict

    def __eq__(self, other):
        if order(self) != order(other):
            return False
        if order(self) == 0:
            return self.prefactor == other
        return self @ other == self @ self

    def __getitem__(self, items):
        items = items if isinstance(items, tuple) else (items,)

        if items[0] is Ellipsis:
            items = (slice(None),) * int(1 + order(self) - len(items)) + items[1:]

        if len(items) > order(self):
            raise ValueError("__getitem__: length of items ({}) exceeds order "
                             "of expression ({})".format(len(items),
                                                         order(self)))

        v = self
        i = 0
        for item in items:
            if item == slice(None):
                v = v.rotate()
                i += 1
            else:
                v = v.inner_product(item)

        if order(v) > 1:
            v = v.rotate(-i)

        return v

    def __hash__(self):
        return hash(self.__repr__())

    def __iter__(self):
        return iter(self._dict)

    def __len__(self):
        return len(self._dict)

    def __repr__(self):
        repr_ = ''

        if self.prefactor != ONE:
            repr_ += '{}*('.format(self.prefactor)

        if self.prefactor == ZERO:
            return str(ZERO)

        repr_ += ' + '.join('{}({})'.format(key, value)
                            for key, value in self.items())

        if self.prefactor != ONE:
            repr_ += ')'

        return repr_


class AbstractVectorBase(AbstractVector):
    """An abstract basis vector.

    Abstract basis vectors form the basis of abstract vector expressions.
    Abstract basis vectors are orthogonal and normalized.  Abstract vectors
    should be instantiated by an AbstractVectorSource.  Example:

    >>> V = AbstractVectorSource('V')
    >>> V.a
    V.a:

    """

    _order = ONE

    def __init__(self, name: str, source: AbstractVectorSource):
        self._name = name
        self._source = source
        self._dict = {self: ONE}

    ## Properties

    @property
    def bases(self):
        return [self]

    @property
    def components(self):
        return (self,)

    @property
    def elements(self):
        return (self,)

    @property
    def name(self):
        return '{}.{}'.format(self._source, self._name)

    @property
    def prefactor(self):
        return ONE

    @property
    def source(self):
        return self._source

    ## Private methods

    def _sympy_eq(self):
        return self._source._format_latex(self._name)

    ## Magic methods

    def coeff(self, item):
        return self.get(item)

    def conjugate(self):
        return self

    def get(self, item, default=ZERO):
        return ONE if item == self else default

    def hermitian_conjugate(self):
        return self

    def inner_product(self, other, *, post_function=outer_product):
        """Compute the inner product.

        In bra-ket notation, this is equivalent to `<self|other>`.  The `@`
        (`__matmul__`) operator has been overloaded to support the notation
        `self @ other`.  `__getitem__` has also been overloaded such that
        `self[other]` is equivalent to `other @ self`.

        """
        if isinstance(other, AbstractVectorBase):
            return self.get(other)

        return (other.prefactor *
                sum(post_function(self.coeff(key), other.coeff(key))
                    for key in other.components))

    def items(self):
        return ((self, ONE),)

    def keys(self):
        return (self,)

    def mul(self, other):
        return AbstractVectorExpression(
            { self: ONE },
            order = order(self),
            prefactor = other)

    def tensor_product(self, other):
        if isinstance(other, AbstractVectorBase):
            if not self.elements:
                return other
            if not other.elements:
                return self
            return AbstractVectorProduct(*self.elements, *other.elements)

        return sum(other.coeff(key) * self.tensor_product(key)
                   for key in other)

    def transpose(self, axis1, axis2):
        return self

    def values(self):
        return (ONE,)

    ## Magic methods

    def __contains__(self, item):
        return item == self

    def __eq__(self, other):
        return self is other

    def __getitem__(self, items):
        items = items if isinstance(items, tuple) else (items,)
        if len(items) > 2:
            raise ValueError("__getitem__: items must be either a single item "
                             "or Ellipsis (`...`) followed by a single item")

        if items[0] is Ellipsis:
            item = items[1]
        else:
            item = items[0]

        return item.inner_product(self)

    def __hash__(self):
        return hash(self.name)

    def __iter__(self):
        return iter((self,))

    def __len__(self):
        return 1

    def __repr__(self):
        return '{}:'.format(self.name)


class SourcedAbstractVectorExpression(AbstractVectorExpression):
    def __init__(self, _dict, order, prefactor,
                 name: str, source: AbstractVectorSource):
        super().__init__(_dict, order, prefactor)
        self._name = name
        self._source = source

    ## Properties

    name = AbstractVectorBase.name

    ## Magic methods

    __hash__ = AbstractVectorBase.__hash__
    __repr__ = AbstractVectorBase.__repr__


class AbstractZeroVector(AbstractVectorBase):
    @property
    def bases(self):
        return []

    @property
    def elements(self):
        return ()


class AbstractVectorProduct(AbstractVectorBase):
    def __init__(self, *bases):
        self._bases = Counter(base for base in bases)
        self._elements = sorted((base for base in self._bases.elements()),
                                key = lambda a: a.name)
        self._dict = {self: ONE}

    ## Properties

    @property
    def bases(self):
        return [base for base in self
                if not isinstance(base, AbstractZeroVector)]

    @property
    def elements(self):
        return self._elements

    @property
    def name(self):
        return '*'.join(base.name for base in self) or '_'

    ## Public methods

    ## Magic methods

    def __eq__(self, other):
        return self.bases == getattr(other, 'bases', None)

    __hash__ = AbstractVectorBase.__hash__

    def __iter__(self):
        return iter(self._elements)

    def __repr__(self):
        return '{}:'.format(self.name)

if __name__ == '__main__':
    import doctest
    doctest.testmod()

if __name__ == '__live_coding__':
    A = AbstractVectorSource('A')
    #i = represent(A.x / A.y, (A.x, A.y, A.z))
    a, b = sym.symbols('a, b')
    #i = a * (b * A.x + A.y) / A.y + A.x / A.z
    #j = (A.x).inner_product(i)
    #j = A.x @ i
    #i = i.conjugate()
    #j = i.inner_product(A.x)
    #j = A.x.inner_product(i)
    #i = (A.x/A.y + A.y/A.y)[..., A.y]
