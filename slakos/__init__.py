from .core import *
from .collections import Path
from .representations import *
from .io import *
from .analysis import *

#from .analytical import (InteratomicMatrixTable, OnsiteSOCTable,
#                         UnitCell, Database, Basis,
#                         analyse_matrix)

from .numerical import (Parameter, Parameters,
                        Hamiltonian,
                        LeastSquareFitter)
from .plotting import (BandStructure)

import logging

logger = logging.getLogger('slakos')
logger.setLevel(logging.WARNING)

_logging_formatter = logging.Formatter("%(levelname)s:%(asctime)s:%(name)s :: "
                                       "%(message)s")

_debug_handler = logging.FileHandler("slakos-debug.log")
_debug_handler.setLevel(logging.DEBUG)
_debug_handler.setFormatter(_logging_formatter)

_info_handler = logging.FileHandler("slakos.log")
_info_handler.setLevel(logging.INFO)
_info_handler.setFormatter(_logging_formatter)

logger.addHandler(_debug_handler)
