class SortedDict(dict):
    def __init__(self, *args, key_function=lambda key: key, **kwargs):
        super().__init__(*args, **kwargs)
        self.key_function = key_function

    def __repr__(self):
        return '{{{}}}'.format(', '.join('{}: {}'.format(key, item)
                                         for key, item in self.items()))

    def __iter__(self):
        return iter(sorted(self.keys(), key=self.key_function))

    def items(self):
        return ((key, self[key]) for key in self)


class ConservativeDict(dict):
    def __setitem__(self, key, value):
        if key not in self:
            super().__setitem__(key, value)


class MultiDimensionalDict:
    def __init__(self):
        self.d = {}

    def __setitem__(self, key, value):
        d = self.d
        if not isinstance(key, tuple):
            key = (key,)

        for k in key[:-1]:
            d[k] = d.get(k, {})
            d = d[k]
        d[key[-1]] = value

    def __getitem__(self, key):
        d = self.d
        if not isinstance(key, tuple):
            key = (key,)
        for k in key:
            if k is Ellipsis:
                d = d.get(Ellipsis, list(d.values()).pop())
            else:
                try:
                    d = d[k]
                except KeyError as e:
                    try:
                        d = d[Ellipsis]
                    except KeyError:
                        raise e
        return d

    def __contains__(self, key):
        d = self.d
        if isinstance(key, tuple):
            for k in key:
                try:
                    d = d[k]
                except KeyError:
                    return False
            return True
        else:
            return key in d
