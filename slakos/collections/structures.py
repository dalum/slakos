import numpy as np

from itertools import product

class StructureMixin:
    def map(self, fn):
        pass

    def mapm(self, mapping):
        pass

class Sequence(StructureMixin, list):
    def map(self, fn) -> list:
        return [fn(coord) for coord in self]

    def mapm(self, mapping) -> list:
        return [mapping[coord] for coord in self]

class Mesh(StructureMixin, set):
    """Collection of points in an ordered structure."""
    def __init__(self, *coordinates):
        self.shape = tuple(len(xs) for xs in coordinates)
        self.array = np.empty(self.shape, dtype=tuple)
        for xs in product(*(enumerate(xs) for xs in coordinates)):
            self.array[tuple(x[0] for x in xs)] = tuple(x[1] for x in xs)
        self.update(self.array.flatten())

    def map(self, fn) -> np.ndarray:
        arr = np.empty(self.shape, dtype=object)
        for i, j in product(*map(range, self.array.shape)):
            arr[i, j] = fn(self.array[i, j])
        return arr

    def mapm(self, mapping) -> np.ndarray:
        arr = np.empty(self.shape, dtype=object)
        for i, j in product(*map(range, self.array.shape)):
            arr[i, j] = mapping[self.array[i, j]]
        return arr

class Chart(dict):
    def __getitem__(self, key):
        if key not in self:
            key = sorted(self, key=lambda item: sum((key[i] - item[i]) ** 2
                                                    for i in range(len(key))))[0]
        return super().__getitem__(key)
