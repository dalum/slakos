import numpy as np
import sympy as sym

from typing import Iterable, Mapping, Callable

from slakos.core import fuzzy_subs

from .structures import Sequence

class PointCollection:
    def atoms(self, *args, **kwargs):
        return {atom for point in self
                for atom in point.atoms(*args, **kwargs)}

    def subs(self, *args, **kwargs):
        cls = self.__class__
        return cls(point.subs(*args, **kwargs) for point in self)

    def evalf(self, *args, **kwargs):
        cls = self.__class__
        return cls(point.evalf(*args, **kwargs) for point in self)

    @classmethod
    def from_sequences(cls, coordinates: Iterable[Iterable], axis: int = 0):
        """Create a PointCollection from a sequence of coordinates.

        Parameters
        ----------
        coordinates : two-dimensional sequence
            Sequence of coordinates to be assigned to the points.
        axis : int, optional
            Axis along which to separate points and dimensions. If `axis` is 0
            (default), the coordinates are assumed to be aligned along axis 1
            and vice versa. See notes for more information.

        Usage Notes
        -----------
        If coordinates are arranged in columns, specify `axis=1`.

        """
        coordinates = np.array(coordinates)
        if axis == 1:
            coordinates = coordinates.swapaxes(0, 1)

        self = cls(sym.Point(*coordinate) for coordinate in coordinates)

        return self

class PointSet(PointCollection, set):
    pass

class PointList(PointCollection, list):
    pass

class Path:
    def __init__(self, labels: list, points: PointList,
                 parameters: 'Parameters') -> None:
        """Make a path.

        Parameters
        ----------
        points : vector list
            VectorList of points between which the path is interpolated.
        parameters : Parameters
            Parameters used to resolve symbolic points.

        Notes
        ----------
        The path is normalised to a length of 1 with 0 being the first point and 1
        being the last. The norm_points are the positions of the points along the
        path.

        """
        self.__dict__.update(locals())

    def _unify(self, **kwargs):
        cls = self.__class__
        points = unify(self.points, **kwargs)
        return cls(points, self.parameters)

    @classmethod
    def from_sequences(cls, labels: Iterable, coordinates: Iterable[Iterable],
                       parameters: 'Parameters', axis: int = 0) -> 'Path':
        """Create a Path from sequences of labels and coordinates.

        See `PointCollection.from_sequences` for more details.

        """
        points = PointList.from_sequences(coordinates, axis=axis)

        self = cls(labels=labels, points=points, parameters=parameters)

        return self

    def __call__(self, steps, subs: dict = {}, start=0, stop=1) -> list:
        from numpy import linspace
        self.space = np.linspace(start, stop, steps)

        subs_ = {key: float(value) for key, value in
                self.parameters.named_value_dict().items()}
        subs_.update(subs)

        points = fuzzy_subs(self.points, subs_).evalf()

        dist = np.array([ float(abs(points[i + 1] - points[i]))
                          for i in range(len(points) - 1) ])
        dist /= sum(dist)

        from itertools import accumulate
        self.norm_points = (0, *accumulate(dist))

        coordinates = np.array(points).swapaxes(0, 1)

        from scipy.interpolate import interp1d
        path_fn = interp1d(self.norm_points, coordinates, bounds_error=False,
                           fill_value='extrapolate')

        return Sequence(tuple(point) for point in
                             path_fn(self.space).swapaxes(0, 1))

