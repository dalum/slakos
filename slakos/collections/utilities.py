# This file is part of Slakos.
#
# Slakos is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# Slakos is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# Slakos.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import sympy as sym

from typing import Callable, Mapping, Iterable

from .structures import Mesh

def matrix_from_sparse_mapping(shape: tuple, mapping: Mapping = None,
                               mesh: Mesh = None, default=0) -> sym.Matrix:
    """Return a SymPy Matrix from a sparsely indexed mapping."""
    mapping = mapping or {}
    if mesh is None:
        def _fn(i, j):
            return mapping.get((i, j), default)
    else:
        def _fn(i, j):
            return mapping.get(mesh.mapping[i, j], default)

    return sym.Matrix(*shape, _fn)

def array_from_sparse_mapping(shape: tuple, mapping: Mapping = None,
                              mesh: Mesh = None, default=0) -> np.ndarray:
    """Return an array from a sparsely indexed mapping."""
    mapping = mapping or {}
    if mesh is None:
        def _fn(i, j):
            return mapping.get((i, j), default)
    else:
        def _fn(i, j):
            return mapping.get(mesh.mapping[i, j], default)

    arr = np.empty(shape)
    from itertools import product
    for key in product(*map(range, shape)):
        arr[key] = _fn(*key)
    return arr
