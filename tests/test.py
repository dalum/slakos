import sympy as sym
import sys
sys.path.append('..')
from slakos.representations.abstractvector import *
from slakos.representations.transformations import *

V = AbstractVectorSpace('V')

def Rz(V, a):
    return ((V.x/V.x + V.y/V.y) * sym.cos(a) +
            (-V.x/V.y + V.y/V.x) * sym.sin(a) +
            V.z/V.z + V.w/V.w)

def Rx(V, a):
    P = V.x/V.z + V.y/V.x + V.z/V.y + V.w/V.w
    P_ = P @ P
    return P @ Rz(V, a) @ P_

def Ry(V, a):
    P = V.x/V.z + V.y/V.x + V.z/V.y + V.w/V.w
    P_ = P @ P
    return P_ @ Rz(V, a) @ P

x1, y1, z1 = sym.symbols('x1, y1, z1', real=True)
x2, y2, z2 = sym.symbols('x2, y2, z2', real=True)
l, m, n = sym.symbols('l, m, n', real=True)

xi = V.x + V.y + V.z

a, b, c, d = sym.symbols('a, b, c, d', real=True)

R = Rz(V, b) @ Ry(V, a)

#V.z_, V.x_, V.y_ = gram_schmidt((xi, V.x, V.y), normalize=normalize)

V.x_ = R @ V.x
V.y_ = R @ V.y
V.z_ = R @ V.z

E = c * (V.x_/V.x_ + V.y_/V.y_) + d * (V.z_/V.z_)
