import sys
import os

sys.path.append(os.path.join('..', '..'))

import unittest

import slakos.representations.abstractvector as av

class TestAbstractVector(unittest.TestCase):

    def test_vec_equal_properties(self):
        O = av.AbstractVectorSpace('O')
        O.a = O.b
        self.assertEqual(O.a, O.b)
        self.assertIsNot(O.a, O.b)

    def test_add_associativity(self):
        O = av.AbstractVectorSpace('O')
        self.assertEqual(O.a + O.b, O.b + O.a)

    def test_add_null(self):
        O = av.AbstractVectorSpace('O')
        self.assertEqual(O.a + 0, O.a)
        self.assertEqual(O.a + (-O.a), av.AbstractVectorExpression({}, order=1))
        self.assertNotEqual(O.a + (-O.a), av.AbstractVectorExpression({}, order=0))

    def test_sub_null(self):
        O = av.AbstractVectorSpace('O')
        self.assertEqual(O.a - 0, O.a)
        self.assertEqual(O.a - O.a, av.AbstractVectorExpression({}, order=1))
        self.assertNotEqual(O.a - O.a, av.AbstractVectorExpression({}, order=0))

    def test_mul_associativity(self):
        O = av.AbstractVectorSpace('O')
        self.assertEqual(4 * O.a, O.a * 4)

    def test_mul_div_similarity(self):
        O = av.AbstractVectorSpace('O')
        self.assertEqual(0.5 * O.a, O.a / 2)

    def test_mul_null(self):
        O = av.AbstractVectorSpace('O')
        self.assertEqual(0 * O.a, av.AbstractVectorExpression({}, order=1))
        self.assertNotEqual(0 * O.a, av.AbstractVectorExpression({}, order=2))

    def test_inner_orthogonal(self):
        O = av.AbstractVectorSpace('O')
        self.assertEqual(O.a @ O.b, 0)
        self.assertEqual(O.a @ O.a, 1)

    def test_inner_complex_conj(self):
        O = av.AbstractVectorSpace('O')
        O.a[O.b] = 1j
        self.assertEqual(O.b @ O.a, 1j)
        self.assertEqual(O.a @ O.b, -1j)

    def test_tensor_associativity(self):
        O = av.AbstractVectorSpace('O')
        S = av.AbstractVectorSpace('S')
        self.assertEqual(O.a*S.m, S.m*O.a)

    def test_tensor_notsame(self):
        O = av.AbstractVectorSpace('O')
        with self.assertRaises(ValueError) as cm:
            _ = O.a*O.b

    def test_tensor_space_preserving(self):
        O = av.AbstractVectorSpace('O')
        S = av.AbstractVectorSpace('S')
        self.assertEqual((S.m*O.a) @ (O.a*S.m), 1)
        self.assertEqual((S.n*O.a) @ (O.a*S.m), 0)
        self.assertEqual((S.n*O.a) @ (O.a), S.n)
        self.assertEqual(O.a @ (S.n*O.a), S.n)

    def test_outer_mul_null(self):
        O = av.AbstractVectorSpace('O')
        self.assertEqual(0 * (O.a / O.a),
                         av.AbstractVectorExpression({}, order=2))
        self.assertEqual(0 * (O.a / O.a / O.b),
                         av.AbstractVectorExpression({}, order=3))
        self.assertNotEqual(0 * (O.a / O.a),
                            av.AbstractVectorExpression({}, order=1))

    def test_outer_as_operator(self):
        O = av.AbstractVectorSpace('O')
        op = ((O.a / O.a) + 2 * (O.b / O.b)
              + 1j * (O.b / O.a - O.a / O.b))

        self.assertEqual(op @ O.a, O.a - 1j * O.b)
        self.assertEqual(op @ O.b, 2 * O.b + 1j * O.a)

        self.assertEqual(O.a @ op, O.a - 1j * O.b)
        self.assertEqual(O.b @ op, 2 * O.b + 1j * O.a)

    def test_outer_non_hermitian(self):
        O = av.AbstractVectorSpace('O')
        op = 1j * (O.a/O.a)

        self.assertEqual(op @ O.a, -1j * O.a)
        self.assertEqual(O.a @ op, 1j * O.a)
        self.assertEqual(O.a @ op @ O.a, -1j)
        self.assertEqual(O.a @ (op @ O.a), -1j)
        self.assertEqual((O.a @ op) @ O.a, -1j)

    def test_vector_overlap(self):
        O = av.AbstractVectorSpace('O')
        O.a[O.b] = 1 / 4
        O.m = (O.a + O.b)
        self.assertEqual(O.m @ O.m, 2.5)
        self.assertEqual(O.m @ O.m, O.m.self_overlap)

    def test_represent_vector(self):
        O = av.AbstractVectorSpace('O')
        vec = av.represent(sum(n * O[n] for n in range(3)), O[:3])
        self.assertEqual(vec, [*range(3)])

    def test_represent_matrix(self):
        O = av.AbstractVectorSpace('O')
        vec = sum(1j * n * O[n] for n in range(3))
        mat = av.represent(vec / vec, O[:3])
        self.assertEqual(mat, [[0,  0,  0],
                               [0, -1, -2],
                               [0, -2, -4]])

if __name__ == '__main__':
    unittest.main()
